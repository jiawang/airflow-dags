# Airflow DAGs

Collection of DAGs and common libraries to be executed by the WMF Airflow instances.

## Instance directories

Data Engineering maintains several airflow instances, each usually associated with a
specific engineering team or function. To collaborate together, we this single
airflow-dags repository deployed to each instance, with instance specific dags directories
and config.

Instance directories contain a dags/ folder, as well as a config/ folder.
Airflow DAGs for an instance should be placed in the `<instance>/dags` folder.

## Artifact syncing

[data-engineering/workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils) is used to abstract away details of where job artifacts live from
DAG code.

Artifacts are declared in an instance's `<instance>/config/artifacts.yaml` file,
and are automatically synced from defined sources to defined caches during
scap deployment of this repository to an WMF Airflow instance.

The cached artifact URLs (usually in HDFS) are then automatically looked up
when using the `artifact()` function defined in an instance's config/dag_config.py.
Example:

```python
from my_instance_name.config.dag_config import artifact

with DAG(...) as dag:
    t1 = SparkSubmitOperator(archives=artifact('my_artifact-0.0.1.tgz'))
```

`artifact` will look up the actual URL to use for the given artifact name.

## wmf_airflow_common

Common Airflow classes and configs used for WMF Airflow instances.

Operators and hooks are generally kept non WMF specific. However,
wmf_airflow_common uses WMF conventions to automate configs for our
airflow instances.

### Spark Operators

wmf_airflow_common comes with 2 custom Spark Operators: `SparkSubmitOperator` and
`SparkSqlOperator`. These are compatible with Airflow's built in Spark operators,
but add additional features like:

- not having to use airflow connections
- Launching Spark via skein
- Overriding the java_class used for SparkSql.

#### SparkSubmitOperator for conda dist envs

Our `SparkSubmitOperator` has a factory method help construct `SparkSubmitOperator`s
for use with conda dist env archives:

```python
t1 = SparkSubmitOperator.for_virtualenv(
    virtualenv_archive = artifact('my-conda-dist-env-0.1.0.tgz'),
    entry_point = 'bin/my_spark_job.py',
    launcher = 'skein',
    # ... Other SparkSubmitOperator constructor keyword args here.
)
```

This is meant to work with conda dist env archives without having them
locally available to the Airflow Scheduler, so setting `launcher='skein'`
and using a `virtualenv_archive` in HDFS is probably what you want to do.

There is experimental support for using your own pyspark dependency
from the conda virtualenv. If you have pyspark installed in your conda
virtualenv, setting `use_virtualenv_spark=True` will cause `SparkSubmitOperator`
to set `spark_binary` to the path to bin/spark-submit in your virtualenv archive.
NOTE: While this works, there are extra configurations that need to be set
to work with [Hadoop](https://spark.apache.org/docs/latest/hadoop-provided.html#apache-hadoop)
and [Hive](https://spark.apache.org/docs/latest/sql-data-sources-hive-tables.html#hive-tables).

### Skein Operators

The `SkeinOperator` and `SimpleSkeinOperator` can be used to launch generic
applications in YARN via Skein.

## Dependencies

To build anything, you first need Kerberos and other basic dependencies:

* On Ubuntu: `sudo apt install libkrb5-dev curl gcc g++ gpg git make ca-certificates conda openjdk-11-jdk libkrb5-dev libsasl2-dev`
* On Mac: see [Developer guide § Developing on macOS](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide#Developing_on_macOS)

## Running Tests

### Linux

Make sure you have `conda` in your path, and the environment dependencies. Then set up an environment:

```bash
conda env create --name airflow-dags -f conda-environment.lock.yml
conda activate airflow-dags
pip install ".[dev,test,lint]"
```

You can then check your changes against style, formatting, typechecking and the testing suite with the following commands:

```bash
# In your conda environment from above:
export PYTHONPATH=.:./wmf_airflow_common/plugins
flake8
mypy
black --check .
isort --check .
pytest
```

If you wish to regenerate the test-fixtures for skein jobs automatically
generated, use:

```bash
find tests -name \*.expected -exec rm '{}' \;
REBUILD_FIXTURES=yes pytest
```

### macOS via Docker

Running test and linters in Docker, like in Gitlab-CI. First, set up the container:

#### Linters

##### Setup

```bash
DOCKER_BUILDKIT=1 \
  docker build \
  --platform linux/x86_64 \
  -m 8g -c 5 \
  -f docker/blubber.yml \
  --target build-lint-image -t conda_env_lint .
```

##### Running

```bash
docker run \
  --platform linux/x86_64 \
  --user 0:0 \
  --memory="8g" \
  --cpus="5.0" \
  --rm -it \
  --volume $(pwd):/opt/airflow-dags \
  --workdir /opt/airflow-dags \
  --entrypoint docker/entrypoint.sh \
  conda_env_lint \
  bash -c "source /srv/app/miniconda/bin/activate && conda activate airflow && flake8 && mypy && black --check . && isort --check ."
```

#### Tests

##### Setup

```bash
DOCKER_BUILDKIT=1 \
  docker build \
  --platform linux/x86_64 \
  -m 8g -c 5 \
  -f docker/blubber.yml \
  --target build-test-image -t conda_env_test .
```

##### Running

```bash
docker run \
  --platform linux/x86_64 \
  --user 0:0 \
  --memory="8g" \
  --cpus="5.0" \
  --rm -it \
  --volume $(pwd):/opt/airflow-dags \
  --workdir /opt/airflow-dags \
  --env PYTHONPATH=".:./wmf_airflow_common/plugins" \
  --entrypoint docker/entrypoint.sh \
  conda_env_test \
  bash -c "source /srv/app/miniconda/bin/activate && conda activate airflow && pytest"
```

If you want to rebuild the fixtures, prepend the `pytest` call with `REBUILD_FIXTURES=yes`.

## Autoformatting

Python files could be standardized locally with:

```bash
# For complying with PEP 8
black path/to/my/file/to/format.py
# For import statements
isort path/to/my/file/to/format.py
```

## Developing

A guide to testing your code while you develop can be found [here](https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide)

## Dependencies management

How to update the dependencies and recreate the conda-environment.lock.yml file:

```bash
./generate_conda_environment_lock_yml.sh
```

Then check the changes in your new env file, and test it:

```bash
git diff conda-environment.lock.yml
# Make sure you don't git add regressions (like removing gitlab urls).

# Perform conda environment creation check
./check_conda_environment_lock_yml.sh

# Run the unit tests
conda activate airflow-dags
pytest
```

When you are satisfied with your lock file, prepare the deb containing the Airflow conda env,
which is going to be deployed on our cluster. A framework of the deploy could be:

* Add a line in debian/changelog
* Bump the version in .gitlab-ci.yml & Dockerfile
* Create a Gitlab release & tag
* Manually launch the CI pipeline to launch the creation of the archive
* Deploy the new version of your deb to the cluster
  * send the package to apt.wikimedia.org
  * `apt update airflow` on target servers (first on test cluster)
  * check result here: https://debmonitor.wikimedia.org/packages/airflow

## DagProperties variables

One of the most commonly misunderstood areas of WMF Airflow DAG development
is the use of the DagProperties objects. Our implementation of DagProperties
is sourced from wmf_airflow_common/config/dag_properties.py and it provides
us with an easy way of setting and configuring the DAG properties in
Airflow UI, from within Python source code.

However, since Airflow UI properties are meant to **override** the settings
found in the source code, this means that (for DAGs already instantiated
in the Airflow UI) any changes to the DagProperties variables in the source code
itself **will not** be reflected in the Airflow UI properties for that DAG
upon deployment of the changes.

Note that this is by design, as it requires the developer to be mindful of
any overrides to the properties made in the UI!

The solution to this problem is to manually inspect the state of the DAG
properties in the UI, and then either:

* Manually make changes to specific properties in the UI and
bring parity between the source code and the UI properties
* Or, more drastically, simply delete the UI properties altogether and let
Airflow re-create them from the source code deployment
