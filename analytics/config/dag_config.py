from os import environ, path

from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.dataset import DatasetRegistry
from wmf_airflow_common.easy_dag import EasyDAGFactory
from wmf_airflow_common.pool import PoolRegistry
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = "hdfs://analytics-hadoop"
refinery_directory = f"{hadoop_name_node}/wmf/refinery"
hql_directory = f"{refinery_directory}/current/hql"

artifacts_directory = f"{refinery_directory}/current/artifacts"
hdfs_temp_directory = "/wmf/tmp/analytics"
archive_directory = f"{hadoop_name_node}/wmf/data/archive"
published_directory = f"{hadoop_name_node}/wmf/data/published"

# Emails.
alerts_email = "data-engineering-alerts@wikimedia.org"

# default_args for this Airflow Instance.
instance_default_args = {
    "owner": "analytics",
    "email": alerts_email,
    "hadoop_name_node": hadoop_name_node,
    "metastore_conn_id": "analytics-hive",
    "datahub_conn_id": "datahub_kafka_jumbo",
    "datahub_environment": "PROD",
    "custom_ca_cert_bundle_location": "/etc/ssl/certs/ca-certificates.crt",
}

# config for jobs that require read or write to the AQS Cassandra cluster.
cassandra_default_conf = dag_default_args.cassandra_default_conf

# Config for jobs that ingest to the Druid cluster.
druid_default_conf = dag_default_args.druid_default_conf

# Config for jobs that want to use Spark 3.3.2 instead of prod default of Spark 3.1.2
spark_3_3_2_conf = dag_default_args.spark_3_3_2_conf

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args["queue"] = "production"

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance("analytics")
artifact = artifact_registry.artifact_url

# Dataset registry for easier sensor and maintenance configuration.
# Provides a syntactic sugar to use when creating Sensors in DAGs.
dataset_file_paths = [path.join(path.dirname(__file__), "datasets.yaml")]
dataset_registry = DatasetRegistry(dataset_file_paths)
dataset = dataset_registry.get_dataset

# Pool registry that creates pools against Airflow DB automatically based on
# the pools declared on Yaml config files.
pool_file_paths = [path.join(path.dirname(__file__), "pools.yaml")]
pool_registry = PoolRegistry(pool_file_paths)
pool = pool_registry.get_or_create

_extra_default_args = {
    **instance_default_args,
    # Arguments for operators that have artifact dependencies
    "hdfs_tools_shaded_jar_path": artifact("hdfs-tools-0.0.6-shaded.jar"),
    "spark_sql_driver_jar_path": artifact("wmf-sparksqlclidriver-1.0.0.jar"),
}

if not is_wmf_airflow_instance():
    user = environ.get("USER")
    if user:
        _extra_default_args["owner"] = user

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(_extra_default_args)

# Helper for creating DAGs with a nicer interface.
# Passes all the defaults and shortcuts to the DAG factory.
create_easy_dag = EasyDAGFactory(
    factory_default_args={
        **default_args,
        **druid_default_conf,
    },
    factory_default_args_shortcuts=["email", "sla"],
    factory_user_defined_filters=filters,
).create_easy_dag


# Helper for creating cassandra loding DAGs with a nicer interface.
# Passes usual and cassandra defaults and shortcuts to the DAG factory.
create_easy_cassandra_loading_dag = EasyDAGFactory(
    factory_default_args={
        **default_args,
        "conf": {
            **cassandra_default_conf,
            # Hack: as we override `conf` with cassandra settings we need to make
            # sure we have the default values as well
            **(default_args["conf"]),
        },
        "jars": (
            # This jar is needed for spark to interact with cassandra
            artifact("spark-cassandra-connector-assembly-3.2.0-WMF-1.jar")
            + ","
            # This jar is needed to provide the org.json.simple library to HQL
            + artifact("refinery-job-0.2.17-shaded.jar")
        ),
    },
    factory_default_args_shortcuts=["email", "sla"],
    factory_user_defined_filters=filters,
).create_easy_dag
