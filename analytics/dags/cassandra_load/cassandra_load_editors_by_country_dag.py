"""
Loads Hive editors by country into Cassandra.

The loading is done with 1 monthly dag
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data source and destination.
    hive_geoeditors_public_monthly_table="wmf.geoeditors_public_monthly",
    cassandra_editors_by_country_table="aqs.local_group_default_T_editors_bycountry.data",
    # DAG start date
    monthly_dag_start_date=datetime(2023, 7, 1),
    # HQL query path
    monthly_dag_hql=f"{hql_directory}/cassandra/monthly/load_cassandra_editors_bycountry_monthly.hql",
    # SLA and alerts email.
    monthly_dag_sla=timedelta(days=5),
    alerts_email=alerts_email,
)


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_editors_by_country_monthly",
    doc_md="Loads Hive editors by country data in cassandra monthly",
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=["monthly", "from_hive", "to_cassandra", "uses_hql", "requires_wmf_geoeditors_public_monthly"],
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    sensor = dataset("hive_wmf_geoeditors_public_monthly").get_sensor_for(monthly_dag)

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.monthly_dag_hql,
        query_parameters={
            "source_table": props.hive_geoeditors_public_monthly_table,
            "destination_table": props.cassandra_editors_by_country_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            # Setting coalesce_partitions to 6 is important
            # as it defines how many parallel loaders we use for cassandra.
            # We currently use 6 as there are 6 cassandra hosts
            "coalesce_partitions": 6,
        },
        # The default spark resources configuration is enough for this job
        # as it processes relatively small data
    )

    sensor >> etl
