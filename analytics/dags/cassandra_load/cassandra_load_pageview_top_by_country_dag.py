"""
Loads Hive pageview top by country into Cassandra.
This is the top countries by number of pageviews.

The loading is done with 1 monthly dag
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_cassandra_loading_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # Data source and destination.
    hive_projectview_table="wmf.projectview_hourly",
    hive_countries_table="canonical_data.countries",
    cassandra_pageview_top_by_country_table="aqs.local_group_default_T_top_bycountry.data",
    # DAG start date
    monthly_dag_start_date=datetime(2023, 7, 1),
    # HQL query path
    monthly_dag_hql=f"{hql_directory}/cassandra/monthly/load_cassandra_pageview_top_bycountry_monthly.hql",
    # SLA and alerts email.
    monthly_dag_sla=timedelta(days=5),
    alerts_email=alerts_email,
)


with create_easy_cassandra_loading_dag(
    dag_id="cassandra_load_pageview_top_by_country_monthly",
    doc_md=(
        "Loads Hive pageviewtop by country data in cassandra monthly."
        " This is the top countries by number of pageviews."
    ),
    start_date=props.monthly_dag_start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_cassandra",
        "uses_hql",
        "requires_wmf_canonical_data_countries",
        "requires_wmf_projectview_hourly",
    ],
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    sensor = dataset("hive_wmf_projectview_hourly").get_sensor_for(monthly_dag)

    etl = SparkSqlOperator(
        task_id="load_cassandra",
        sql=props.monthly_dag_hql,
        query_parameters={
            "source_table": props.hive_projectview_table,
            "country_deny_list_table": props.hive_countries_table,
            "destination_table": props.cassandra_pageview_top_by_country_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            # Setting coalesce_partitions to 6 is important
            # as it defines how many parallel loaders we use for cassandra.
            # We currently use 6 as there are 6 cassandra hosts
            "coalesce_partitions": 6,
        },
        # Spark resources configuration needs to be tweaked for this job
        # as it processes relatively big data
        executor_memory="8G",
        executor_cores=2,
        driver_memory="8G",
        driver_cores=2,
        conf={
            # Hack: We don't want to override default_args["conf"] already set,
            # we just want to add to them, so we reset them in our override.
            **monthly_dag.default_args["conf"],
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.executor.memoryOverhead": 2048,
        },
    )

    sensor >> etl
