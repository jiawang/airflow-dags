"""
### Creates the clickstream monthly dataset for various projects.
The job runs every month and its TSV results are synchronised to the public.

The Airflow dag launches a spark action that runs the ClickstreamBuilder scala job in
analytics-refinery-source/refinery-job and saves the results in a temporary folder.

Then the files from the temporary folder are archived to an archive location, with nice names.

* Temporary result location example:
  /wmf/tmp/analytics/clickstream_monthly__create_clickstream_file__20220301/

* Archive example:
  /wmf/data/archive/clickstream/2022-03/clickstream-enwiki-2022-03.tsv.gz

#### Sources:
- project_namespace_table: wmf_raw.mediawiki_project_namespace_map
- page_table: wmf_raw.mediawiki_page
- redirect_table: wmf_raw.mediawiki_redirect
- pagelinks_table: wmf_raw.mediawiki_pagelinks
- pageview_actor_table: wmf.pageview_actor

#### Variables stored in clickstream_monthly_config with their default values
* clickstream_archive_base_path: '/wmf/data/archive/clickstream'
* default_args:
    poke_interval: 2 hours
* wiki_list: ["enwiki", "ruwiki", "dewiki", "eswiki", "jawiki", "frwiki", "zhwiki", "itwiki",
    "plwiki", "ptwiki", "fawiki"]
* start_date: datetime(2022, 3, 31)),
* refinery_job_jar: artifact('refinery-job-0.1.27-shaded')
* clickstream_minimum_links: 10
"""

from datetime import datetime, timedelta

from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hdfs_temp_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import PrePartitions, add_post_partitions

# List of mediawiki to create an archive for.
wiki_list_default = "enwiki ruwiki dewiki eswiki jawiki frwiki zhwiki itwiki plwiki ptwiki fawiki".split()

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    dag_start_date=datetime(2024, 4, 1),
    # SLA and alerts
    dag_sla=timedelta(days=10),
    alerts_email=alerts_email,
    # Job JAR
    refinery_job_jar=artifact("refinery-job-0.2.41-shaded.jar"),
    # Paths
    tmp_base_path=f"{hdfs_temp_directory}",
    clickstream_archive_base_path="/wmf/data/archive/clickstream",
    # Job parameters
    wiki_list=wiki_list_default,
    minimun_link_count=10,
    spark_partitions=1024,
    # Spark conf
    spark_executor_cores=4,
    spark_executor_memory="8G",
    spark_max_executors=128,
)

with create_easy_dag(
    dag_id="clickstream_monthly",
    doc_md=__doc__,
    start_date=props.dag_start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "from_hive",
        "to_hdfs",
        "uses_archiver",
        "uses_spark",
        "requires_wmf_mediawiki_project_namespace_map",
        "requires_wmf_pageview_actor",
        "requires_wmf_raw_mediawiki_page",
        "requires_wmf_raw_mediawiki_pagelinks",
        "requires_wmf_raw_mediawiki_redirect",
    ],
    sla=props.dag_sla,
    email=props.alerts_email,
    default_args={"poke_interval": timedelta(hours=2).total_seconds()},
) as dag:
    sensors = []
    sensors.append(dataset("hive_wmf_pageview_actor").get_sensor_for(dag))
    sensors.append(dataset("hive_wmf_raw_mediawiki_project_namespace_map").get_sensor_for(dag))

    # don't use dataset-sensors for complex partition lists
    snapshot = "{{data_interval_start | to_ds_month}}"
    wiki_db_partitions = PrePartitions([[f"wiki_db={db}" for db in props.wiki_list]])
    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_page_snapshots",
            partition_names=add_post_partitions([f"wmf_raw.mediawiki_page/snapshot={snapshot}"], wiki_db_partitions),
        )
    )

    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_pagelinks_snapshots",
            partition_names=add_post_partitions(
                [f"wmf_raw.mediawiki_pagelinks/snapshot={snapshot}"], wiki_db_partitions
            ),
        )
    )

    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_redirect_snapshots",
            partition_names=add_post_partitions(
                [f"wmf_raw.mediawiki_redirect/snapshot={snapshot}"], wiki_db_partitions
            ),
        )
    )

    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_private_linktarget_snapshots",
            partition_names=add_post_partitions(
                [f"wmf_raw.mediawiki_private_linktarget/snapshot={snapshot}"], wiki_db_partitions
            ),
        )
    )

    tmp_base_path = f"{props.tmp_base_path}/" + "{{dag.dag_id}}_{{data_interval_start | to_ds_nodash}}"

    etl = SparkSubmitOperator(
        task_id="clickstream_builder",
        driver_memory="8G",
        driver_cores=2,
        executor_memory=props.spark_executor_memory,
        executor_cores=props.spark_executor_cores,
        conf={"spark.dynamicAllocation.maxExecutors": props.spark_max_executors},
        # Too many logs are generated, it makes log collection crashed.
        skein_app_log_collection_enabled=False,
        # Spark application
        application=props.refinery_job_jar,
        java_class="org.wikimedia.analytics.refinery.job.ClickstreamBuilder",
        application_args={
            "--output_base_path": tmp_base_path,
            "--wiki_list": ",".join(props.wiki_list),
            "--year": "{{data_interval_start.year}}",
            "--month": "{{data_interval_start.month}}",
            "--minimun_link_count": props.minimun_link_count,
            "--spark_partitions": props.spark_partitions,
        },
    )

    archivers = [
        HDFSArchiveOperator(
            source_directory=f"{tmp_base_path}/wikiDb={wiki}",
            archive_file=f"{props.clickstream_archive_base_path}/{snapshot}/clickstream-{wiki}-{snapshot}.tsv.gz",
            task_id=f"archive_{wiki}",
        )
        for wiki in props.wiki_list
    ]

    sensors >> etl >> archivers
