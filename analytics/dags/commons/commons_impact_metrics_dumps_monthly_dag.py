"""
Generates the dumps for the Commons Impact Metrics data product monthly.
They will be stored as bzipped TSV files under /wmf/data/archive/commons/.
"""

from datetime import datetime, timedelta

from airflow.sensors.external_task import ExternalTaskSensor
from airflow.utils.task_group import TaskGroup

from analytics.config.dag_config import (
    alerts_email,
    archive_directory,
    create_easy_dag,
    hdfs_temp_directory,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# Folder in the refinery repository where the queries live.
query_folder = f"{hql_directory}/commons_impact_metrics"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted after re-deploying this script.
props = DagProperties(
    start_date=datetime(2023, 11, 1),
    # Configuration for each one of the datasets to dump.
    dataset_config={
        "commons_category_metrics_snapshot": {
            "sensor_external_task_id": "compute_category_metrics",
            "hql_file": f"{query_folder}/dump_commons_category_metrics_snapshot.hql",
            "source_table": "wmf_contributors.commons_category_metrics_snapshot",
            "destination_directory": f"{archive_directory}/commons/category_metrics_snapshot",
        },
        "commons_media_file_metrics_snapshot": {
            "sensor_external_task_id": "compute_media_file_metrics",
            "hql_file": f"{query_folder}/dump_commons_media_file_metrics_snapshot.hql",
            "source_table": "wmf_contributors.commons_media_file_metrics_snapshot",
            "destination_directory": f"{archive_directory}/commons/media_file_metrics_snapshot",
        },
        "commons_pageviews_per_category_monthly": {
            "sensor_external_task_id": "compute_pageviews_per_category",
            "hql_file": f"{query_folder}/dump_commons_pageviews_per_category_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_category_monthly",
            "destination_directory": f"{archive_directory}/commons/pageviews_per_category_monthly",
        },
        "commons_pageviews_per_media_file_monthly": {
            "sensor_external_task_id": "compute_pageviews_per_media_file",
            "hql_file": f"{query_folder}/dump_commons_pageviews_per_media_file_monthly.hql",
            "source_table": "wmf_contributors.commons_pageviews_per_media_file_monthly",
            "destination_directory": f"{archive_directory}/commons/pageviews_per_media_file_monthly",
        },
        "commons_edits": {
            "sensor_external_task_id": "compute_edits",
            "hql_file": f"{query_folder}/dump_commons_edits.hql",
            "source_table": "wmf_contributors.commons_edits",
            "destination_directory": f"{archive_directory}/commons/edits",
        },
    },
    # Base directory where to store the temporary dump files.
    base_temp_directory=f"{hdfs_temp_directory}/commons_impact_metrics_dumps",
    # Spark config.
    hql_spark_config={
        "executor_cores": 4,
        "executor_memory": "16G",
        "driver_cores": 2,
        "driver_memory": "4G",
        "conf": {
            "spark.dynamicAllocation.maxExecutors": 16,
            "spark.executor.memoryOverhead": "2G",
            "spark.yarn.maxAppAttempts": 1,
        },
    },
    # Alerting.
    sla=timedelta(days=10),
    alerts_email=alerts_email,
)

tags = [
    "monthly",
    "from_iceberg",
    "to_hdfs",
    "uses_hql",
    "uses_archiver",
    "requires_wmf_contributors_commons_category_metrics_snapshot",
    "requires_wmf_contributors_commons_media_file_metrics_snapshot",
    "requires_wmf_contributors_commons_pageviews_per_category_monthly",
    "requires_wmf_contributors_commons_pageviews_per_media_file_monthly",
    "requires_wmf_contributors_commons_edits",
]

with create_easy_dag(
    dag_id="commons_impact_metrics_dumps_monthly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=tags,
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    year_month = "{{data_interval_start | to_ds_month}}"

    for dataset_name, dataset_properties in props.dataset_config.items():
        # Directory paths for this dataset.
        temporary_directory = f"{props.base_temp_directory}/{year_month}/{dataset_name}"
        archive_file = f"{dataset_properties['destination_directory']}/{dataset_name}_{year_month}.tsv.bz2"

        with TaskGroup(group_id=dataset_name):
            wait_for_source_data = ExternalTaskSensor(
                task_id="wait_for_source_data",
                external_dag_id="commons_impact_metrics_monthly",
                external_task_id=dataset_properties["sensor_external_task_id"],
            )

            generate_dumps = SparkSqlOperator(
                task_id="generate_dumps",
                sql=dataset_properties["hql_file"],
                query_parameters={
                    "source_table": dataset_properties["source_table"],
                    "destination_directory": temporary_directory,
                    "year_month": year_month,
                },
                **props.hql_spark_config,
            )

            archive_dumps = HDFSArchiveOperator(
                task_id="archive_dumps",
                source_directory=temporary_directory,
                archive_file=archive_file,
                expected_filename_ending=".bz2",
                check_done=True,
            )

            wait_for_source_data >> generate_dumps >> archive_dumps
