import os
from datetime import datetime, timedelta

from airflow import DAG

from analytics.config.dag_config import artifact, default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator

dag_id = "metadata_ingest_daily"
var_props = VariableProperties(f"{dag_id}_config")

hive_dbs = [
    "event",
    "event_sanitized",
    "wmf",
    "wmf_raw",
    "wmf_product",
    "wmf_traffic",
    "wmf_readership",
    "wmf_contributors",
    "canonical_data",
    "knowledge_gaps",
    "gdi",
    "differential_privacy",
    "content_gap_metrics",
    "research",
    "sandbox",
    "sandbox_iceberg",
    "discovery",
]
# druid_clusters = ['internal', 'public']
druid_clusters = ["internal"]
kafka_brokers = ["jumbo"]

here = os.path.dirname(os.path.realpath(__file__))
config_template = os.path.join(here, "configs", "{}.yaml")
hive_jobs = [f"hive_{db}" for db in hive_dbs]
druid_jobs = [f"druid_{cluster}" for cluster in druid_clusters]
kafka_jobs = [f"kafka_{broker}" for broker in kafka_brokers]

transformers = ["add_event_streams.py"]
transformer_files = {t: os.path.join(here, "configs", t) for t in transformers}

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 6, 28)),
    schedule="@daily",
    tags=["daily", "from_druid", "from_hive", "from_kafka", "to_datahub", "uses_skein"],
    default_args=var_props.get_merged(
        "default_args",
        {
            **default_args,
            "sla": timedelta(hours=10),
        },
    ),
) as dag:
    ingestion_jobs = hive_jobs + druid_jobs + kafka_jobs

    [
        # keytab and principal set automatically by default_args from the parent DAG
        SimpleSkeinOperator(
            task_id=f"run_{job}_ingestion",
            script="REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt "
            "./environment/bin/python environment/bin/datahub ingest -c config.yaml",
            files={
                "environment": artifact("datahub-cli-0.10.4.tgz"),
                "config.yaml": config_template.format(job),
            }
            | transformer_files,
        )
        for job in ingestion_jobs
    ]
