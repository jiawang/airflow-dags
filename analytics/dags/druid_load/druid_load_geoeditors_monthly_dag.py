"""
Formats wmf.geoeditors_monthly and load it to Druid monthly.
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # DAG start date.
    start_date=datetime(2023, 4, 1),
    # Source database and table.
    geoeditors_monthly_database="wmf",
    geoeditors_monthly_table="geoeditors_monthly",
    # HQL query paths.
    druid_hql_path=f"{hql_directory}/druid_load/generate_druid_geoeditors_monthly.hql",
    # Intermediate (temporary) database and base directory.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Druid configs
    hive_to_druid_config={
        "druid_datasource": "mediawiki_geoeditors_monthly",
        "timestamp_column": "month",
        "timestamp_format": "yyyy-MM",
        "dimensions": ["wiki_db", "country_code", "users_are_anonymous", "activity_level"],
        "metrics": [
            "distinct_editors",
            "namespace_zero_distinct_editors",
        ],
        "query_granularity": "month",
        "segment_granularity": "month",
        "num_shards": 1,
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid.
        "temp_directory": None,
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLA and alerts email.
    sla=timedelta(days=10),  # Similar to geoeditors monthly
    alerts_email=alerts_email,
)


with create_easy_dag(
    dag_id="druid_load_geoeditors_monthly",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_geoeditors_monthly"],
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    intermediate_directory = (
        f"{props.intermediate_base_directory}" "/{{dag.dag_id}}/{{data_interval_start|to_ds_month_nodash}}"
    )

    sensor = dataset("hive_wmf_geoeditors_monthly").get_sensor_for(dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=props.druid_hql_path,
        query_parameters={
            "source_table": f"{props.geoeditors_monthly_database}.{props.geoeditors_monthly_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "month": "{{data_interval_start | to_ds_month}}",  # e.g. 2023-04
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
