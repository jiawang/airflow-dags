"""
Loads wmf.pageview_hourly from Hive to druid datasource pageviews_daily.

This monthly DAG waits for a month's data to be ready in Hive and
loads it all to Druid.

DAG reads from hive wmf.pageview_hourly table.
Druid datasource is pageviews_daily
"""

from datetime import datetime, timedelta

from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # input tables
    hive_pageview_table="wmf.pageview_hourly",
    wiki_map_table="canonical_data.wikis",
    namespace_map_table="wmf_raw.mediawiki_project_namespace_map",
    # Project-namespace-map snapshot.
    # Project-namespace-map is made available for month X-1 after
    # all mediawiki tables are sqooped at the beginning of month X,
    # meaning around the 2nd at mid-day (currently).
    # As this job is monthly, we only need to delay this data by 1 month.
    # For example: At the start of 2020-05, this job will calculate
    # the data for 2020-04, an use the project_namespace_map from 2020-03.
    mw_namespace_snapshot="{{data_interval_start|start_of_previous_month|to_ds_month}}",
    # temporary base directory and database
    temporary_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    temporary_database="wmf",
    # start date
    start_date=datetime(2023, 4, 1),
    # hql file location
    hql_path=f"{hql_directory}/druid_load/pageview/generate_monthly_druid_pageviews.hql",
    # druid to hive config
    hive_to_druid_config={
        "druid_datasource": "pageviews_daily",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "project",
            "language_variant",
            "project_family",
            "namespace_is_content",
            "namespace_is_talk",
            "namespace_canonical_name",
            "access_method",
            "agent_type",
            "referer_class",
            "continent",
            "country_code",
            "country",
            "ua_browser_family",
            "ua_browser_major",
            "ua_os_family",
            "ua_os_major",
            "ua_os_minor",
            "ua_wmf_app_version",
        ],
        "metrics": ["view_count"],
        "segment_granularity": "month",
        "query_granularity": "day",
        "num_shards": 4,
        "reduce_memory": "8192",
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        "temp_directory": None,  # Override just for testing.
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLA and alerts email.
    sla=timedelta(days=2),
    alerts_email=alerts_email,
)


with create_easy_dag(
    dag_id="druid_load_pageviews_daily_aggregated_monthly",
    doc_md="Loads pageview from hive to druid monthly",
    start_date=props.start_date,
    schedule="@monthly",
    tags=["monthly", "from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_pageview_hourly"],
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    monthly_temporary_directory = (
        props.temporary_base_directory + "/{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    )
    monthly_temporary_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"

    sensors = []
    sensors.append(dataset("hive_wmf_pageview_hourly").get_sensor_for(dag))
    sensors.append(
        NamedHivePartitionSensor(
            task_id="wait_for_mediawiki_project_namespace_map",
            partition_names=[f"{props.namespace_map_table}/snapshot={props.mw_namespace_snapshot}"],
            poke_interval=timedelta(minutes=60).total_seconds(),
        )
    )

    format_pageview = SparkSqlOperator(
        task_id="format_monthly_pageview",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.hive_pageview_table,
            "wiki_map_table": props.wiki_map_table,
            "namespace_map_table": props.namespace_map_table,
            "destination_directory": monthly_temporary_directory,
            "destination_table": f"{props.temporary_database}.{monthly_temporary_table}",
            "mediawiki_snapshot": props.mw_namespace_snapshot,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "coalesce_partitions": 16,
        },
        driver_cores=2,
        driver_memory="4G",
        executor_cores=4,
        executor_memory="8G",
        conf={
            "spark.dynamicAllocation.maxExecutors": "64",
            "spark.executor.memoryOverhead": 2048,
        },
    )

    druid_loader = HiveToDruidOperator(
        task_id="load_to_druid_monthly",
        database=props.temporary_database,
        table=monthly_temporary_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_start | add_months(1) | to_ds_hour}}",
        executor_cores=4,
        executor_memory="8G",
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.temporary_database}.{monthly_temporary_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {monthly_temporary_directory}",
    )

    sensors >> format_pageview >> druid_loader >> hive_cleaner >> hdfs_cleaner
