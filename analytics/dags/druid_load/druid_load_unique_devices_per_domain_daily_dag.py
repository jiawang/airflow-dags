"""
Formats wmf.unique_devices_per_domain_daily and loads it to Druid.

The loading is done with 2 DAGs: a daily one and a monthly one.
The daily DAG loads daily data to Druid as soon as it's available in Hive.
The monthly DAG waits for a full month of data to be available in Hive,
and loads it all to Druid as a single monthly segment (more efficient).
If there were daily segments in Druid already, it will override them.
Use the monthly DAG for back-filling and re-runs.
"""

from datetime import datetime, timedelta

from airflow.sensors.external_task import ExternalTaskSensor
from delayed_timetables import DelayedMonthlyTimetable

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.helper import daily_execution_dates_of_month

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # DAG start dates.
    daily_start_date=datetime(2023, 4, 1),
    monthly_start_date=datetime(2023, 4, 1),
    # Source database and table.
    unique_devices_database="wmf_readership",
    unique_devices_table="unique_devices_per_domain_daily",
    # HQL query paths.
    druid_hql_directory=f"{hql_directory}/druid_load",
    daily_dag_hql="generate_druid_unique_devices_per_domain_daily.hql",
    monthly_dag_hql="generate_druid_unique_devices_per_domain_daily_aggregated_monthly.hql",
    # Intermediate (temporary) database and base directory.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Druid configs common to both DAGs.
    hive_to_druid_config={
        "druid_datasource": "unique_devices_per_domain_daily",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "domain",
            "country",
            "country_code",
        ],
        "metrics": [
            "uniques_underestimate",
            "uniques_offset",
            "uniques_estimate",
        ],
        "query_granularity": "day",
        "num_shards": 1,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid
        "temp_directory": None,
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLAs and alerts email.
    daily_dag_sla=timedelta(hours=6),
    monthly_dag_sla=timedelta(days=1),
    alerts_email=alerts_email,
)

default_tags = ["from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_unique_devices_per_domain_daily"]

# Daily DAG.
with create_easy_dag(
    dag_id="druid_load_unique_devices_per_domain_daily",
    doc_md="Loads unique devices per domain from Hive to Druid daily.",
    start_date=props.daily_start_date,
    schedule="@daily",
    tags=["daily"] + default_tags,
    sla=props.daily_dag_sla,
    email=props.alerts_email,
) as daily_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start | to_ds_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start | to_ds_nodash}}"
    )

    sensor = ExternalTaskSensor(
        task_id="wait_for_unique_devices_per_domain_daily_iceberg",
        external_dag_id="unique_devices_per_domain_daily",
        external_task_id="compute_per_domain_daily_iceberg",
    )

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.daily_dag_hql}",
        query_parameters={
            "source_table": f"{props.unique_devices_database}.{props.unique_devices_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "day": "{{data_interval_start | to_ds}}",
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        segment_granularity="day",
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]


# Monthly DAG.
with create_easy_dag(
    dag_id="druid_load_unique_devices_per_domain_daily_aggregated_monthly",
    doc_md="Loads unique devices per domain from Hive to Druid aggregated monthly.",
    start_date=props.monthly_start_date,
    schedule=DelayedMonthlyTimetable(timedelta(days=3)),
    tags=["monthly"] + default_tags,
    sla=props.monthly_dag_sla,
    email=props.alerts_email,
) as monthly_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_month_nodash}}"
    )

    sensor = ExternalTaskSensor(
        task_id="wait_for_unique_devices_per_domain_daily_over_a_month_iceberg",
        external_dag_id="unique_devices_per_domain_daily",
        external_task_id="compute_per_domain_daily_iceberg",
        execution_date_fn=daily_execution_dates_of_month,
    )

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=f"{props.druid_hql_directory}/{props.monthly_dag_hql}",
        query_parameters={
            "source_table": f"{props.unique_devices_database}.{props.unique_devices_table}",
            "destination_table": f"{props.intermediate_database}.{intermediate_table}",
            "destination_directory": intermediate_directory,
            "day": "{{data_interval_start | to_ds}}",
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        segment_granularity="month",
        reduce_memory=8192,
        **props.hive_to_druid_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {props.intermediate_database}.{intermediate_table};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
