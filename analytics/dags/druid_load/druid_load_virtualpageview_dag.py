"""
Formats wmf.virtualpageview_hourly
and loads it to Druid in segments with two granularities:

    * daily for the most recent month, with more dimensions
    * monthly for data older than the most recent month, with fewer dimensions
"""

from datetime import datetime, timedelta

from delayed_timetables import DelayedMonthlyTimetable

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # DAG start date.
    start_date=datetime(2023, 4, 26),
    # Source database and table.
    virtualpageview_fqtn="wmf.virtualpageview_hourly",
    # HQL query paths.
    daily_druid_hql_path=f"{hql_directory}/virtualpageview/generate_druid_daily_segment_of_virtualpageview_hourly.hql",
    monthly_druid_hql_path=f"{hql_directory}/virtualpageview/"
    "generate_druid_monthly_segment_of_virtualpageview_hourly.hql",
    # Intermediate (temporary) database and base directory.
    intermediate_database="tmp",
    intermediate_base_directory=f"{hadoop_name_node}/wmf/tmp/druid",
    # Druid configs are different for daily and monthly jobs (granularity, dimensions, and resources)
    hive_to_druid_daily_config={
        "druid_datasource": "virtualpageviews_hourly",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "project",
            "language_variant",
            "access_method",
            "agent_type",
            "referer_class",
            "continent",
            "country_code",
            "country",
            "subdivision",
            "city",
            "ua_device_family",
            "ua_browser_family",
            "ua_browser_major",
            "ua_os_family",
            "ua_os_major",
            "ua_os_minor",
            "ua_wmf_app_version",
        ],
        "metrics": [
            "view_count",
        ],
        "query_granularity": "hour",
        "segment_granularity": "day",
        "num_shards": 4,
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid
        "temp_directory": None,
    },
    hive_to_druid_monthly_config={
        "druid_datasource": "virtualpageviews_hourly",
        "timestamp_column": "dt",
        "timestamp_format": "auto",
        "dimensions": [
            "project",
            "language_variant",
            "access_method",
            "agent_type",
            "referer_class",
            "continent",
            "country_code",
            "country",
            "subdivision",
            "city",
            "ua_device_family",
            "ua_browser_family",
            "ua_browser_major",
            "ua_os_family",
            "ua_os_major",
            "ua_os_minor",
            "ua_wmf_app_version",
        ],
        "metrics": [
            "view_count",
        ],
        "query_granularity": "hour",
        "segment_granularity": "month",
        "num_shards": 8,
        "reduce_memory": 8192,
        "hadoop_queue": "production",
        "hive_to_druid_jar": artifact("refinery-job-0.2.14-shaded.jar"),
        # NOTE: for testing, set to a directory whose parent is owned by analytics-privatedata:druid
        "temp_directory": None,
    },
    # Configs for running very simple SparkSQL code from the Skein app.
    minimalist_spark_config={
        "master": "local",
        "driver_cores": 1,
        "driver_memory": "1G",
    },
    # SLA and alerts email.
    sla_daily=timedelta(hours=30),
    sla_monthly=timedelta(days=6),
    alerts_email=alerts_email,
)

default_tags = ["from_hive", "to_druid", "uses_hql", "uses_spark", "requires_wmf_virtualpageview_hourly"]


with create_easy_dag(
    dag_id="druid_load_virtualpageview_daily",
    doc_md="Loads virtualpageview hourly from Hive to Druid daily.",
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily"] + default_tags,
    sla=props.sla_daily,
    email=props.alerts_email,
) as daily_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_nodash}}"
    intermediate_directory = props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_nodash}}"
    intermediate_fqtn = f"{props.intermediate_database}.{intermediate_table}"

    sensor = dataset("hive_wmf_virtualpageview_hourly").get_sensor_for(daily_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=props.daily_druid_hql_path,
        query_parameters={
            "source_table": props.virtualpageview_fqtn,
            "destination_table": intermediate_fqtn,
            "destination_directory": intermediate_directory,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        **props.hive_to_druid_daily_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {intermediate_fqtn};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]


with create_easy_dag(
    dag_id="druid_load_virtualpageview_monthly",
    doc_md="Loads virtualpageview hourly from Hive to Druid monthly.",
    start_date=props.start_date,
    schedule=DelayedMonthlyTimetable(timedelta(days=3)),
    tags=["monthly"] + default_tags,
    sla=props.sla_monthly,
    email=props.alerts_email,
) as monthly_dag:
    # Table and directory where to store the temporary data to be loaded to Druid.
    intermediate_table = "tmp_{{dag.dag_id}}_{{data_interval_start|to_ds_month_nodash}}"
    intermediate_directory = (
        props.intermediate_base_directory + "/{{dag.dag_id}}/{{data_interval_start|to_ds_month_nodash}}"
    )
    intermediate_fqtn = f"{props.intermediate_database}.{intermediate_table}"

    sensor = dataset("hive_wmf_virtualpageview_hourly").get_sensor_for(monthly_dag)

    formatter = SparkSqlOperator(
        task_id="format_data_to_load",
        sql=props.monthly_druid_hql_path,
        query_parameters={
            "source_table": props.virtualpageview_fqtn,
            "destination_table": intermediate_fqtn,
            "destination_directory": intermediate_directory,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
        },
    )

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        database=props.intermediate_database,
        table=intermediate_table,
        since="{{data_interval_start | to_ds_hour}}",
        until="{{data_interval_end | to_ds_hour}}",
        **props.hive_to_druid_monthly_config,
    )

    hive_cleaner = SparkSqlOperator(
        task_id="remove_temporary_table",
        sql=f"DROP TABLE IF EXISTS {intermediate_fqtn};",
        **props.minimalist_spark_config,
    )

    hdfs_cleaner = SimpleSkeinOperator(
        task_id="remove_temporary_directory",
        script=f"hdfs dfs -rm -r {intermediate_directory}",
    )

    sensor >> formatter >> loader >> [hive_cleaner, hdfs_cleaner]
