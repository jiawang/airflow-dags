"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job consumes event data from event.mediawiki_page_content_change_v1 and merges it into an
Iceberg table containing all existing (wki_db, revisions) tuples. This target table is meant to be
an intermediary table to generate dumps from.

We accomplish this by running a pyspark job that runs a MERGE INTO
that de-duplicates events and mutates the target table.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump/-/blob/main/mediawiki_content_dump/events_merge_into.py
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    artifact,
    create_easy_dag,
    dataset,
    pool,
    spark_3_3_2_conf,
)
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # DAG settings
    start_date=datetime(2023, 8, 23, 0),
    sla=timedelta(hours=6),
    conda_env=artifact("mediawiki-content-dump-0.2.0.dev0-ingest-deletes-and-moves.conda.tgz"),
    # target table
    hive_wikitext_raw_table="wmf_dumps.wikitext_raw_rc2",
    # source tables
    hive_mediawiki_page_content_change_table="event.mediawiki_page_content_change_v1",
    hive_revision_visibility_change="event.mediawiki_revision_visibility_change",
    # Spark job tuning
    driver_memory="16G",
    driver_cores="4",
    executor_memory="16G",
    executor_cores="2",
    max_executors="64",
    spark_driver_maxResultSize="8G",
    # keep shuffler partitions low so that the final file fanout is low as well
    spark_sql_shuffle_partitions="64",
    # avoid java.lang.StackOverflowError when generating MERGE predicate pushdowns
    spark_extraJavaOptions="-Xss8m",
    # disable fetching HDFS BlockLocations to avoid very long query planning times.
    spark_sql_iceberg_locality_enabled="false",
)


with create_easy_dag(
    dag_id="dumps_merge_events_to_wikitext_raw",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=[
        "hourly",
        "from_hive",
        "to_iceberg",
        "requires_mediawiki_page_content_change_v1",
        "requires_mediawiki_revision_visibility_change",
        "uses_spark",
        "mediawiki_dumps",
    ],
    sla=props.sla,
    max_active_runs=1,  # MERGEs will step into each other, let's run them serially.
    email="xcollazo@wikimedia.org",  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    content_sensor = dataset("hive_event_mediawiki_page_content_change_v1").get_sensor_for(dag)
    visibility_sensor = dataset("hive_event_mediawiki_revision_visibility_change").get_sensor_for(dag)

    common_spark_conf = {
        **spark_3_3_2_conf,
        "spark.driver.maxResultSize": props.spark_driver_maxResultSize,
        "spark.dynamicAllocation.maxExecutors": props.max_executors,
        "spark.sql.shuffle.partitions": props.spark_sql_shuffle_partitions,
        "spark.sql.iceberg.locality.enabled": props.spark_sql_iceberg_locality_enabled,
    }

    util.dict_add_or_append_string_value(
        common_spark_conf, "spark.driver.extraJavaOptions", props.spark_extraJavaOptions
    )
    util.dict_add_or_append_string_value(
        common_spark_conf, "spark.executor.extraJavaOptions", props.spark_extraJavaOptions
    )

    # Usage: events_merge_into.py source_table, target_table, year, month, day, hour
    content_args = [
        props.hive_mediawiki_page_content_change_table,
        props.hive_wikitext_raw_table,
        "{{data_interval_start.year}}",
        "{{data_interval_start.month}}",
        "{{data_interval_start.day}}",
        "{{data_interval_start.hour}}",
    ]

    content_merge_into = SparkSubmitOperator.for_virtualenv(
        task_id="spark_content_merge_into",
        virtualenv_archive=props.conda_env,
        entry_point="bin/events_merge_into.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf=common_spark_conf,
        launcher="skein",
        application_args=content_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        # This pool with 1 slot allows us to have multiple DAGs with
        # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
        pool=pool("mutex_for_wmf_dumps_wikitext_raw"),
    )

    # Usage: visibility_merge_into.py source_table, target_table, year, month, day, hour
    visibility_args = [
        props.hive_revision_visibility_change,
        props.hive_wikitext_raw_table,
        "{{data_interval_start.year}}",
        "{{data_interval_start.month}}",
        "{{data_interval_start.day}}",
        "{{data_interval_start.hour}}",
    ]

    visibility_merge_into = SparkSubmitOperator.for_virtualenv(
        task_id="spark_visibility_merge_into",
        virtualenv_archive=props.conda_env,
        entry_point="bin/visibility_merge_into.py",
        driver_memory=props.driver_memory,
        driver_cores=props.driver_cores,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf=common_spark_conf,
        launcher="skein",
        application_args=visibility_args,
        use_virtualenv_spark=True,
        default_env_vars={
            "SPARK_HOME": "venv/lib/python3.10/site-packages/pyspark",  # point to the packaged Spark
            "SPARK_CONF_DIR": "/etc/spark3/conf",
        },
        # This pool with 1 slot allows us to have multiple DAGs with
        # MERGE INTOs that effectively run serially against table "wmf_dumps.wikitext_raw", thus mimicking a mutex.
        pool=pool("mutex_for_wmf_dumps_wikitext_raw"),
    )

    content_sensor >> content_merge_into >> visibility_sensor >> visibility_merge_into
