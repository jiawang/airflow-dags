"""
### Prepares an introspective dataset about HDFS files
* wait for the HDFS FSImage backup on HDFS (It's generated on the standby NameNode by an HDFS superuser)
* convert the image to XML
* send the XML to HDFS
* parse the XML
* reconstruct the inode paths
* aggregate data for easy visualization
* store the result in a Hive table partition
"""

import getpass
import os
from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    refinery_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = "hdfs_usage_weekly"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2022, 12, 3),
    # input directories
    hdfs_fsimage_directory="/wmf/data/raw/hdfs/fsimage",
    hdfs_xml_fsimage_directory="/wmf/data/raw/hdfs/xml_fsimage",
    # output
    output_table="wmf.hdfs_usage",
    convert_script_path=f"{refinery_directory}/current/bin/convert_fsimage_to_xml_on_hdfs.sh",
    refinery_job_jar=artifact("refinery-job-0.2.10-shaded.jar"),
    # log4j settings file path, e.g. hdfs:///path/to/log4j.properties
    custom_hdfs_log4j_properties=None,
    custom_spark_conf={
        # Optimization: the Spark job is memory intensive, as it use caching extensively.
        # The ratio memory/cores is higher than usual (2GB/core).
        # The behaviour of the job `HdfsXMLFsImageConverter.scala` depends on the following parameters.
        # Last adjustments of those parameters on 20221207 with a cluster of ~110M files.
        "spark.dynamicAllocation.maxExecutors": 128,
        "spark.driver.memory": "2g",
        "spark.driver.cores": 1,
        "spark.executor.memory": "6g",
        "spark.executor.cores": 2,
        "spark.executor.memoryOverhead": 2048,
        "spark.memory.fraction": 0.8,  # Use 80% of the worker memory as a storage (default 60%)
        # The dataset contains internal data about HDFS, including the list of files in the `/user/*` folders.
        # In order to replicate an equivalent level of privacy we have between HDFS users,
        # we set this dataset HDFS permission to: analytics:analytics-admins:750
        #
        # The HDFS permission of the Spark created partition is going to be the one of the parent folder.
        # It should have already been set for you here:
        # https://github.com/wikimedia/analytics-refinery/blob/master/hql/hdfs/create_hdfs_usage_table.hql
        #
        # To be more explicit about the default file permission, we set the umask here:
        "spark.hadoop.fs.permissions.umask-mode": "027",
    },
    # SLA and alert email
    dag_sla=timedelta(days=7),
    alerts_email=alerts_email,
)

fsimage = f"{props.hdfs_fsimage_directory}" + "/fsimage_{{ds}}.gz"
xml_fsimage = f"{props.hdfs_xml_fsimage_directory}" + "/fsimage_{{ds}}.xml"

snapshot = "{{ds}}"
current_user = getpass.getuser()

# The Spark job running HdfsXMLFsImageConverter is producing a lot of logs, like any large Spark application. If you
# want to study the logs without the noise, provide an alternative log4j.property file through a VariableProperties.
# The file needs to be local in client_mode (The local test machine or the local to the Skein app launcher.)
custom_log4j_conf = {}
if props.custom_hdfs_log4j_properties:
    log4j_configuration = f"-Dlog4j.configuration=file:{os.path.basename(props.custom_hdfs_log4j_properties)}"
    custom_log4j_conf = {
        "spark.driver.extraJavaOptions": log4j_configuration,
        "spark.executor.extraJavaOptions": log4j_configuration,
    }

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    # The schedule should match the one of the producer of the raw fsimage.
    # https://github.com/wikimedia/puppet/blob/107e3625ab6e29bb7c4afeea71d9e4714e8cba50/modules/profile/manifests/hadoop/backup/namenode.pp#L67
    schedule="0 0 * * 1",  # Weekly, starting on Monday at 00:00.
    tags=["weekly", "from_hdfs", "to_hive", "uses_skein", "uses_spark"],
    default_args={"sla": props.dag_sla},
    user_defined_filters=filters,
) as dag:
    # Wait for the raw FSImage on HDFS. The image is sent on HDFS by a systemd timer `hadoop-namenode-backup-hdfs` on
    # the standby NameNode. It's compressed.
    hdfs_sensor = URLSensor(
        task_id="wait_for_raw_fsimage_on_hdfs",
        url=f"hdfs://{fsimage}",
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # Convert HDFS FSImage to XML
    # ===========================
    #
    # * fetch the raw FSImage from HDFS
    # * uncompress it
    # * convert the image to XML
    # * send the XML to HDFS
    #
    # The fsimage weights ~10GB in raw, and ~40GB in XML (in 2022). So this scripts
    # assumes there is enough temporary disk space available on the worker.
    #
    # The job expects the target directory to be created. The source folder is owned by analytics:analytics-admins. And
    # the permission of the generated FSImage as XML is going to be the same as the folder.
    convert_fsimage_to_xml_on_hdfs = SimpleSkeinOperator(
        task_id="convert_fsimage_to_xml_on_hdfs",
        name="convert_fsimage_to_xml_on_hdfs",  # name of the Skein app
        queue="production",
        script=f"./convert_fsimage_to_xml_on_hdfs.sh {fsimage} {xml_fsimage}",
        files=props.convert_script_path,
        # The XML conversion consumes few memory as it converts the HDFS FSImage linearly.
        resources={"memory": "2 GiB", "vcores": 1},
    )

    # Aggregate data for easy visualization and store the result in a Hive table partition
    # For more information about this job, look at the HdfsXMLFsImageConverter file in analytics
    # refinery source.
    # https://github.com/wikimedia/analytics-refinery-source/blob/master/refinery-job/src/main/scala/org/wikimedia/analytics/refinery/job/HdfsXMLFsImageConverter.scala
    aggregate_and_extract_fsimage_data_to_parquet = SparkSubmitOperator(
        task_id="aggregate_and_extract_fsimage_data_to_parquet",
        application=props.refinery_job_jar,
        java_class="org.wikimedia.analytics.refinery.job.HdfsXMLFsImageConverter",
        # skein_app_log_collection_enabled=False,
        application_args=[
            "--fsimage_xml_path",
            xml_fsimage,
            "--output_table",
            props.output_table,
            "--snapshot",
            snapshot,
            "--checkpoints_dir",
            f"hdfs:///tmp/{current_user}_HdfsXMLFsImageConverter_{snapshot}",
        ],
        files=props.custom_hdfs_log4j_properties,  # Optional log4j configuration file
        conf={
            **custom_log4j_conf,  # Optional log4j configuration file
            **props.custom_spark_conf,
        },
    )

    hdfs_sensor >> convert_fsimage_to_xml_on_hdfs >> aggregate_and_extract_fsimage_data_to_parquet
