"""
### Creating the session length daily dataset
* Extracting data from the client session ticks dataset (partitioned hourly)
* Recreating virtual sessions by reindexing the data (As SessionId is not here.)
* Counting the number sessions per length
* Loading the result into a Hive table

The resulting dataset is:
* partitioned by year, month, day
* located in /wmf/data/event/mediawiki_client_session_tick/year=2022/month=3/day=8/
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import (
    alerts_email,
    create_easy_dag,
    dataset,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    start_date=datetime(2022, 6, 14),
    client_session_tick_table="event.mediawiki_client_session_tick",
    session_length_table="wmf.session_length_daily",
    session_length_iceberg_table="wmf_traffic.session_length",
    hql_path=f"{hql_directory}/session_length/session_length_daily.hql",
    hql_iceberg_path=f"{hql_directory}/session_length/session_length_iceberg.hql",
    sla=timedelta(hours=6),
    email=alerts_email,
)

with create_easy_dag(
    dag_id="session_length_daily",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    tags=["daily", "from_hive", "to_hive", "uses_hql", "requires_event_mediawiki_client_session_tick"],
    sla=props.sla,
    email=props.email,
) as dag:
    sensor = dataset("hive_event_mediawiki_client_session_tick").get_sensor_for(dag)

    hive_etl = SparkSqlOperator(
        task_id="process_session_length",
        sql=props.hql_path,
        query_parameters={
            "source_table": props.client_session_tick_table,
            "destination_table": props.session_length_table,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
        },
        conf={"spark.dynamicAllocation.maxExecutors": 32},
    )

    iceberg_etl = SparkSqlOperator(
        task_id="process_iceberg_session_length",
        sql=props.hql_iceberg_path,
        query_parameters={
            "source_table": props.client_session_tick_table,
            "destination_table": props.session_length_iceberg_table,
            "day": "{{ data_interval_start | to_ds }}",
            "coalesce_partitions": 1,
        },
        conf={"spark.dynamicAllocation.maxExecutors": 32},
    )

    sensor >> [hive_etl, iceberg_etl]
