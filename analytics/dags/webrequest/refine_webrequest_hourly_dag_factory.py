"""
# Verify webrequest logs, refine them, and create the webrequest hourly dataset

The basic verification analyzes each log line and checks that the timestamp and potentially other critical fields are
present, also checks the request's sequence number and computes per host statistics. It detects holes, duplicates, and
nulls in sequence numbers. But there is no check on new hosts arriving, hosts getting decommissioned, or on the amount
of per host traffic.

Then data get refined, meaning converted from raw JSON logs imported from Kafka into a clustered-bucketed table stored
in Parquet format with newly computed fields.

Summary:
* Detect the hourly _IMPORTED flag from Gobblin
* Add the raw webrequest partition
* Data validation steps (see below)
* Extract data from the raw webrequest dataset (on an hourly partition)
* Ensure the uniqueness of input rows
* Compute different fields with the help of Java UDFs
* Load the result into a partition in the webrequest hourly dataset.

The resulting dataset is:
* partitioned by webrequest_source, year, month, day, hour
* located in /wmf/data/wmf/webrequest/

Notes:
- We add the partition to the table before verification, and do not drop the partition if there is an error.
Hence, the table might contain partitions that contains duplicates/holes. This is for the ease of the developers when
trying to have a look at the data. The table is not meant for researchers.
- Almost the same dags are generated for the analytics and analytics-test cluster. That's the reason for this factory.
- Please update the record_version if you change the refined table content definition and/or schema.
"""

from datetime import datetime, timedelta
from typing import Any, Dict, List
from urllib.parse import urlparse

from airflow import DAG
from airflow.operators.python import BranchPythonOperator

from wmf_airflow_common import util
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.email import HdfsEmailOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters


def _check_if_hdfs_report(
    report_dir_path: str, next_tasks_with_report: List[str], next_tasks_without_report: List[str]
) -> List[str]:
    """
    Determines whether the last execution of one of the extract data loss task has produced a report and branch to the
    next tasks accordingly.
    """
    parsed_report_dir_path = urlparse(report_dir_path)
    if parsed_report_dir_path.scheme != "hdfs":
        raise ValueError(f"HDFS url expected for report_dir_path, got: f{report_dir_path}")
    hdfs = util.hdfs_client(f"{parsed_report_dir_path.scheme}://{parsed_report_dir_path.hostname}")
    file_info = hdfs.get_file_info(util.get_first_part_in_spark_output_dir(hdfs, parsed_report_dir_path.path))
    if file_info is not None and file_info.size > 0:
        return next_tasks_with_report  # Return the task_id of the next task to run.
    return next_tasks_without_report  # Otherwise, skip downstream tasks.


def generate_dag(
    dag_id: str,
    webrequest_source: str,
    default_start_date: datetime,
    tags: List[str],
    import_dag_config_for_test_cluster: bool = False,
    # Incomplete or lost data thresholds (%) for warning
    data_warning_threshold: float = 0.5,  # 0.5%
    # Incomplete or lost data thresholds (%) for error
    data_error_threshold: float = 2.0,  # 2%
) -> DAG:
    if import_dag_config_for_test_cluster:
        from analytics_test.config.dag_config import (
            alerts_email,
            artifact,
            default_args,
            hadoop_name_node,
        )
        from analytics_test.config.dag_config import (
            hql_directory as hql_directory_from_dag_config,
        )
    else:
        from analytics.config.dag_config import (
            alerts_email,
            artifact,
            default_args,
            hadoop_name_node,
        )
        from analytics.config.dag_config import (
            hql_directory as hql_directory_from_dag_config,
        )

    var_props = VariableProperties(f"{dag_id}_config")

    # Variable properties definitions
    # ===============================

    raw_webrequest_table = var_props.get("raw_webrequest_table", "wmf_raw.webrequest")
    raw_webrequest_location_base = var_props.get(
        "raw_webrequest_location_base", f"{hadoop_name_node}/wmf/data/raw/webrequest"
    )
    webrequest_sequence_stats_table = var_props.get(
        "webrequest_sequence_stats_table", "wmf_raw.webrequest_sequence_stats"
    )
    webrequest_sequence_stats_hourly_table = var_props.get(
        "webrequest_sequence_stats_hourly_table", "wmf_raw.webrequest_sequence_stats_hourly"
    )
    refined_webrequest_table = var_props.get("refined_webrequest_table", "wmf.webrequest")
    # Data threshold:
    #   If the proportion of incomplete or lost data is above the error value the job fails, the refine task is not
    #   launched and an email is sent.
    #   If the proportion is above the warning value, a warning email is sent.
    data_warning_threshold = var_props.get("data_warning_threshold", data_warning_threshold)
    data_error_threshold = var_props.get("data_error_threshold", data_error_threshold)
    hql_directory = var_props.get("hql_directory", hql_directory_from_dag_config)
    # data_loss_check_directory_base is the base directory in HDFS where to put data loss information when error
    # threshold is passed.
    data_loss_check_directory_base = var_props.get(
        "data_loss_check_directory_base", f"{hadoop_name_node}/wmf/data/raw/webrequests_data_loss"
    )
    refined_webrequest_location_base = var_props.get(
        "refined_webrequest_location_base", f"{hadoop_name_node}/wmf/data/wmf/webrequest"
    )
    # This property allows to exclude malformed webrequest rows from processing.
    # Its format should be: '<hostname_1>',<sequence_1>,'<hostname_2>',<sequence_2>,...
    # For example: 'cp1085.eqiad.wmnet',2225898272,'cp1027.eqiad.wmnet',2345678637
    # If left empty, no rows will be excluded.
    excluded_row_ids = var_props.get("excluded_row_ids", "")

    # Reused set of parameters
    # ========================
    sequence_statistics_tasks_common_params = {
        "driver_memory": "2G",
        "driver_cores": 1,
        "executor_memory": "4G",
        "executor_cores": 4,
        "conf": {
            # The generate_sequence_statistic job does not need much memory, but it needs to read the source files 2
            # times. Thus, the parallelization should match at least 2 times the number of files (txt.gz, not
            # splittable for read). Creating 4*128=512 Spark tasks is high enough to maximize parallelization in most
            # cases (202303: 168 files).
            "spark.dynamicAllocation.maxExecutors": 128
        },
    }
    extract_data_loss_common_params: Dict[str, Any] = {
        **sequence_statistics_tasks_common_params,
        "sql": f"{hql_directory}/webrequest/extract_data_loss.hql",
        # Extracting the report is a very small task. It shouldn't even create an executor.
        "conf": {"spark.dynamicAllocation.maxExecutors": 1},
    }

    with DAG(
        dag_id=dag_id,
        doc_md=__doc__,
        start_date=var_props.get_datetime("start_date", default_start_date),
        schedule="@hourly",
        tags=tags,
        default_args=var_props.get_merged(
            "default_args",
            {
                **default_args,
                "sla": timedelta(hours=5),
            },
        ),
        user_defined_filters=filters,
    ) as dag:
        # Variables common to multiple tasks
        # ==================================

        # The raw partition path is in the form:
        #   `webrequest_text/year=2023/month=03/day=02/hour=05`
        # It's not respecting the Hive table standard, but it's limited by the Gobblin possibilities.
        # The first part of the location being the Kafka topic.
        raw_partition_location = (
            f"{raw_webrequest_location_base}/webrequest_{webrequest_source}/"
            "year={{ data_interval_start.year }}/"
            'month={{ data_interval_start.format("MM") }}/'
            'day={{ data_interval_start.format("DD") }}/'
            'hour={{ data_interval_start.format("HH") }}'
        )

        sequence_statistics_tasks_common_query_params = {
            "webrequest_source": webrequest_source,
            "year": "{{ data_interval_start.year }}",
            "month": "{{ data_interval_start.month }}",
            "day": "{{ data_interval_start.day }}",
            "hour": "{{ data_interval_start.hour }}",
        }

        extract_data_loss_common_query_params = {
            **sequence_statistics_tasks_common_query_params,
            "source_table": webrequest_sequence_stats_hourly_table,
        }

        report_hdfs_base_path = (
            f"{data_loss_check_directory_base}/{webrequest_source}/"
            "{{data_interval_start.year}}/"
            "{{data_interval_start.month}}/"
            "{{data_interval_start.day}}/"
            "{{data_interval_start.hour}}"
        )
        report_hdfs_base_path_error = f"{report_hdfs_base_path}/ERROR"
        report_hdfs_base_path_warning = f"{report_hdfs_base_path}/WARNING"

        # Dag tasks
        # =========

        wait_for_gobblin_export = URLSensor(
            task_id="wait_for_gobblin_export",
            url=f"{raw_partition_location}/_IMPORTED",
            poke_interval=30,  # 30 seconds
        )

        add_raw_partition = SparkSqlOperator(
            task_id="add_raw_webrequest_partition",
            sql=f"{hql_directory}/utils/hive/add_partition.hql",
            # Run the request from the Yarn Skein app with minimal resources
            # In this case it's possible as the sql task only interacts with the metastore, and don't trigger
            # computations.
            master="local",  # local means the Yarn Skein app, not the Airflow worker (not yet on K8S).
            driver_memory="1G",
            driver_cores=1,
            query_parameters={
                "table": raw_webrequest_table,
                "partition_spec": f"webrequest_source='{webrequest_source}',"
                "year={{data_interval_start.year}},"
                "month={{data_interval_start.month}},"
                "day={{data_interval_start.day}},"
                "hour={{data_interval_start.hour}}",
                "location": raw_partition_location,
            },
        )

        generate_sequence_statistics = SparkSqlOperator(
            **sequence_statistics_tasks_common_params,
            task_id="generate_sequence_statistics",
            sql=f"{hql_directory}/webrequest/generate_sequence_statistics.hql",
            query_parameters={
                **sequence_statistics_tasks_common_query_params,
                "source_table": raw_webrequest_table,
                "destination_table": webrequest_sequence_stats_table,
            },
        )

        generate_sequence_statistics_hourly = SparkSqlOperator(
            **sequence_statistics_tasks_common_params,
            task_id="generate_sequence_statistics_hourly",
            sql=f"{hql_directory}/webrequest/generate_sequence_statistics_hourly.hql",
            query_parameters={
                **sequence_statistics_tasks_common_query_params,
                "source_table": webrequest_sequence_stats_table,
                "destination_table": webrequest_sequence_stats_hourly_table,
            },
        )

        extract_data_loss_errors = SparkSqlOperator(
            **extract_data_loss_common_params,
            task_id="extract_data_loss_errors",
            query_parameters={
                **extract_data_loss_common_query_params,
                "target": report_hdfs_base_path_error,
                "incomplete_data_threshold": str(data_error_threshold),
                "data_loss_threshold": str(data_error_threshold),
            },
        )

        extract_data_loss_warnings = SparkSqlOperator(
            **extract_data_loss_common_params,
            task_id="extract_data_loss_warnings",
            query_parameters={
                **extract_data_loss_common_query_params,
                "target": report_hdfs_base_path_warning,
                "incomplete_data_threshold": str(data_warning_threshold),
                "data_loss_threshold": str(data_warning_threshold),
            },
        )

        data_loss_errors_hdfs_report_fork = BranchPythonOperator(
            task_id="data_loss_errors_hdfs_report_fork",
            python_callable=_check_if_hdfs_report,
            op_kwargs={
                "report_dir_path": report_hdfs_base_path_error,
                "next_tasks_with_report": ["data_loss_errors_email"],
                "next_tasks_without_report": ["extract_data_loss_warnings"],
            },
        )

        data_loss_warnings_hdfs_report_fork = BranchPythonOperator(
            task_id="data_loss_warnings_hdfs_report_fork",
            python_callable=_check_if_hdfs_report,
            op_kwargs={
                "report_dir_path": report_hdfs_base_path_warning,
                "next_tasks_with_report": ["data_loss_warnings_email"],
                "next_tasks_without_report": [],
            },
        )

        data_loss_errors_email = HdfsEmailOperator(
            task_id="data_loss_errors_email",
            to=alerts_email,
            subject="Data Loss ERROR - Airflow Analytics {{ dag.dag_id }} {{ ds }}",
            html_content=(
                "The following data losses have been detected:<br/>"
                "Job: {{ dag.dag_id }}<br/>"
                "Date: {{ data_interval_start | to_ds_hour }}<br/>"
                f"Error threshold: {data_error_threshold}%<br/>"
                "Report content:<br/>"
            ),
            spark_output_directory=report_hdfs_base_path_error,
            hadoop_name_node=hadoop_name_node,
        )

        data_loss_warnings_email = HdfsEmailOperator(
            task_id="data_loss_warnings_email",
            to=alerts_email,
            subject="Data Loss WARNING - Airflow Analytics {{ dag.dag_id }} {{ ds }}",
            html_content=(
                "The following data losses have been detected:<br/>"
                "Job: {{ dag.dag_id }}<br/>"
                "Date: {{ data_interval_start | to_ds_hour }}<br/>"
                f"Warning threshold: {data_warning_threshold}%<br/>"
                "Report content:<br/>"
            ),
            spark_output_directory=report_hdfs_base_path_warning,
            hadoop_name_node=hadoop_name_node,
        )

        refine_webrequest = SparkSqlOperator(
            task_id="refine_webrequest",
            sql=f"{hql_directory}/webrequest/refine_webrequest_hourly.hql",
            query_parameters={
                "refinery_jar": artifact("refinery-hive-0.2.48-shaded.jar"),
                "source_table": raw_webrequest_table,
                "webrequest_source": webrequest_source,
                "destination_table": refined_webrequest_table,
                "year": "{{ data_interval_start.year }}",
                "month": "{{ data_interval_start.month }}",
                "day": "{{ data_interval_start.day }}",
                "hour": "{{ data_interval_start.hour }}",
                "record_version": "0.0.27",  # Sync with https://w.wiki/6Qpg
                "coalesce_partitions": 256,
                "spark_sql_shuffle_partitions": 256,
                "excluded_row_ids": excluded_row_ids,
            },
            driver_memory="4G",
            driver_cores=1,
            executor_memory="12G",
            executor_cores=2,
            conf={
                "spark.dynamicAllocation.maxExecutors": 128,
                "spark.hadoop.mapreduce.fileoutputcommitter.algorithm.version": 2,
            },
        )

        # While we have not yet migrated all jobs to Airflow, we need to generate _SUCCESS files so that Oozie is able
        # to trigger them.
        # TODO: Remove this task once the Oozie migration is complete.
        mark_done = URLTouchOperator(
            task_id="write_wmf_webrequest_success_file",
            url=f"{refined_webrequest_location_base}"
            f"/webrequest_source={webrequest_source}"
            "/year={{data_interval_start.year}}"
            "/month={{data_interval_start.month}}"
            "/day={{data_interval_start.day}}"
            "/hour={{data_interval_start.hour}}"
            "/_SUCCESS",
        )

        # Define the tasks trees
        # ======================

        (
            wait_for_gobblin_export
            >> add_raw_partition
            >> generate_sequence_statistics
            >> generate_sequence_statistics_hourly
            >> extract_data_loss_errors
            >> data_loss_errors_hdfs_report_fork
            >> [data_loss_errors_email, extract_data_loss_warnings]
        )

        extract_data_loss_warnings >> [refine_webrequest, data_loss_warnings_hdfs_report_fork]

        data_loss_warnings_hdfs_report_fork >> data_loss_warnings_email

        refine_webrequest >> mark_done

        return dag
