"""
Airflow DAG conversion of the wmcs report
"""

from datetime import datetime, timedelta

from airflow.models.baseoperator import chain

from analytics.config.dag_config import (
    alerts_email,
    artifact,
    create_easy_dag,
    dataset,
    hadoop_name_node,
    hql_directory,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator

dag_id = "wmcs_report_monthly"

# WARNING: This DagProperties object is tied to an Airflow variable in
# Airflow UI under a key by this Python script's file name.
# Any changes made to values in this DagProperties object inside this script
# WILL NOT BE PICKED UP by the Airflow UI, unless the key in the Airflow UI is
# deleted before re-deploying this script.
props = DagProperties(
    # dag start date
    start_date=datetime(2024, 2, 22),
    # JAR containing Data pivot application.
    pivot_application=artifact("refinery-job-0.2.33-shaded.jar"),
    # base path to query files
    wmcs_hql_basepath=f"{hql_directory}/reports/wmcs",
    # sources
    source_table="wmf.editors_daily",
    # temp directory for query results
    base_tmp_directory=f"{hadoop_name_node}/wmf/tmp/analytics/reports/wmcs",
    # moving to final destination
    agg_results_path=f"{hadoop_name_node}/wmf/data/published/datasets/periodic/reports/metrics/wmcs",
    # SLA and alert email
    dag_sla=timedelta(days=7),
    alerts_email=alerts_email,
)

# HQL files + pivot determination
queries = {
    "wikis_by_internet_and_wmcs_edits": False,
    "wikis_by_wmcs_edits": False,
    "wikis_by_wmcs_edits_percent": True,
}

with create_easy_dag(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@monthly",
    tags=[
        "monthly",
        "uses_hql",
        "from_hive",
        "uses_archiver",
        "uses_pivoter",
        "requires_wmf_editors_daily",
        "all_sites",
    ],
    sla=props.dag_sla,
    email=props.alerts_email,
) as dag:
    # check if partition for the week is available
    sensor = dataset("hive_wmf_editors_daily").get_sensor_for(dag)

    # generate reports for all pingback queries
    for query, pivot in queries.items():
        # collect SparkSqlOperator objects in this list
        operators = [sensor]

        sql = props.wmcs_hql_basepath + "/" + query + ".hql"
        agg_results_path = f"{props.agg_results_path}/{query}.tsv"
        unpivot_agg_results_path = f"{props.agg_results_path}/{query}_unpivot.tsv"

        tmp_results_dir = props.base_tmp_directory + "/" + query + "_{{data_interval_start|to_ds}}"

        cumulative_results_path = agg_results_path
        # for pivoted reports, we are generating unpivoted aggregated
        # results first, and then applying the pivot operator on that
        if pivot:
            cumulative_results_path = unpivot_agg_results_path

        # run the original SQL to generate the report
        query_params = {
            "source_table": props.source_table,
            "agg_results_path": cumulative_results_path,
            "destination_dir": tmp_results_dir,
            "start_date": "{{data_interval_start|to_ds}}",
            "end_date": "{{data_interval_end|to_ds}}",
        }

        compute_report = SparkSqlOperator(
            task_id=f"compute_{query}",
            sql=sql,
            query_parameters=query_params,
        )

        operators.append(compute_report)

        if pivot:
            # run DataPivoter on current, non-pivoted results
            pivot_dir = props.base_tmp_directory + "/" + query + "_pivoted_{{data_interval_start|to_ds}}"

            pivot_report = SparkSubmitOperator(
                task_id=f"pivot_{query}",
                application=props.pivot_application,
                java_class="org.wikimedia.analytics.refinery.job.DataPivoter",
                application_args={
                    "--source_directory": tmp_results_dir,
                    "--destination_directory": pivot_dir,
                },
            )

            operators.append(pivot_report)

            # archive unpivoted file back
            move_unpivoted_report_back = HDFSArchiveOperator(
                task_id=f"move_unpivoted_{query}",
                source_directory=tmp_results_dir,
                archive_file=cumulative_results_path,
                expected_filename_ending=".csv",
                check_done=True,
            )

            operators.append(move_unpivoted_report_back)

            # make sure HDFS archiver picks pivoted results
            tmp_results_dir = pivot_dir

        # archive file and move to final output path.
        move_report_to_archive = HDFSArchiveOperator(
            task_id=f"move_{query}",
            source_directory=tmp_results_dir,
            archive_file=agg_results_path,
            expected_filename_ending=".csv",
            check_done=True,
        )

        operators.append(move_report_to_archive)

        # chain the collected operators up
        chain(*operators)
