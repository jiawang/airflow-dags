import json
import subprocess
import urllib.request
from datetime import datetime, timedelta

import dns.resolver
from airflow.operators.dummy import DummyOperator
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import DateType, IntegerType, LongType, StringType

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact
)
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    # selected wikis from https://noc.wikimedia.org/conf/highlight.php?file=dblists/flaggedrevs.dblist
    wikis=[
        "alswiki",
        "arwiki",
        "bewiki",
        "bnwiki",
        "bswiki",
        "cewiki",
        "ckbwiki",
        "dewiki",
        "enwiki",
        "eowiki",
        "fawiki",
        "fiwiki",
        "hiwiki",
        "huwiki",
        "iawiki",
        "idwiki",
        "kawiki",
        "mkwiki",
        "plwiki",
        "ruwiki",
        "sqwiki",
        "trwiki",
        "ukwiki",
        "vecwiki",
        "zh_classicalwiki",
    ],
    start_date=datetime(2024, 8, 30, 5),
    sla=timedelta(hours=12),
    tags=["hourly", "to_iceberg"],
    catchup=False,
    destination_table="wmf_product.moderation_flagged_revisions_pending_hourly",
    alerts_email=product_analytics_alerts_email,

    # job repo artifact
    conda_env=artifact("moderation-mariadb-jobs-0.4.0-v0.4.0.conda.tgz"),
    
    # spark config settings
    spark_driver_memory="2G",
    spark_driver_cores="4",
    mysql_mariadb_driver=artifact("mysql-connector-j-8.2.0.jar"),

    mariadb_pw_file_path="/user/analytics-product/mysql-analytics-research-client-pw.txt"
)

with create_easy_dag(
    dag_id="flagged_revisions_pending_hourly",
    doc_md="Calculates the number of flagged revisions pending to be reviewed at each hour, on a given wiki.",
    schedule="@hourly",
    start_date=props.start_date,
    sla=props.sla,
    catchup=props.catchup,
    tags=props.tags,
    email=props.alerts_email,
    # temporary fix to reduce concurrency; more context in T376113
    # can be removed once Airflow to dse-k8s cluster (T362788)
    max_active_tasks=2,
) as dag:

    spark_conf = {
        "spark.driver.memory": props.spark_driver_memory,
        "spark.driver.cores": props.spark_driver_cores,
    }

    util.dict_add_or_append_string_value(spark_conf, "spark.jars", props.mysql_mariadb_driver, ",")

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    for wiki_db in props.wikis:

        content_args = [
            "--wiki_db",
            wiki_db,
            "--destination_table",
            props.destination_table,
            "--mariadb_pw_file",
            props.mariadb_pw_file_path
        ]

        fetch_pending_frevs = SparkSubmitOperator.for_virtualenv(
            task_id=f"{wiki_db}_pending_flagged_revisions",
            virtualenv_archive=props.conda_env,
            entry_point="bin/flagged_revisions_pending_hourly.py",
            conf=spark_conf,
            launcher="skein",
            application_args=content_args,
        )

        start >> fetch_pending_frevs >> end
        
