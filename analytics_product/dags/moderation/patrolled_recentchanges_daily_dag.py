from datetime import datetime, timedelta

from airflow.operators.dummy import DummyOperator

from analytics_product.config.dag_config import (
    create_easy_dag,
    product_analytics_alerts_email,
    artifact
)
from wmf_airflow_common import util
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

props = DagProperties(
    # wikis that have $wgUseRCPatrol enabled
    # from the top 15 per from wiki-comparision
    wikis=[
        "frwiki",
        "itwiki",
        "ptwiki",
        "fawiki",
        "idwiki",
        "nlwiki"
    ],
    start_date=datetime(2024, 9, 10, 5),
    sla=timedelta(hours=12),
    tags=["daily", "to_iceberg"],
    destination_table="wmf_product.moderation_patrolled_recentchanges_daily",
    alerts_email=product_analytics_alerts_email,

    # job repo artifact
    conda_env=artifact("moderation-mariadb-jobs-0.5.0-v0.5.0.conda.tgz"),
    
    # spark config settings
    spark_driver_memory="2G",
    spark_driver_cores="4",
    mysql_mariadb_driver=artifact("mysql-connector-j-8.2.0.jar"),

    mariadb_pw_file_path="/user/analytics-product/mysql-analytics-research-client-pw.txt"
)

with create_easy_dag(
    dag_id="patrolled_recentchanges_daily",
    doc_md="Calculates the various metrics related patrolled recentchanges, daily.",
    schedule="@daily",
    start_date=props.start_date,
    sla=props.sla,
    tags=props.tags,
    email=props.alerts_email,
    # temporary fix to reduce concurrency; more context in T376113
    # can be removed once Airflow to dse-k8s cluster (T362788)
    max_active_tasks=2,
) as dag:

    spark_conf = {
        "spark.driver.memory": props.spark_driver_memory,
        "spark.driver.cores": props.spark_driver_cores,
    }

    util.dict_add_or_append_string_value(spark_conf, "spark.jars", props.mysql_mariadb_driver, ",")

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    for wiki_db in props.wikis:

        content_args = [
            "--wiki_db",
            wiki_db,
            "--destination_table",
            props.destination_table,
            "--mariadb_pw_file",
            props.mariadb_pw_file_path
        ]

        calculate_patrolled_recentchanges = SparkSubmitOperator.for_virtualenv(
            task_id=f"{wiki_db}_patrolled_recentchanges",
            virtualenv_archive=props.conda_env,
            entry_point="bin/patrolled_recentchanges_daily.py",
            conf=spark_conf,
            launcher="skein",
            application_args=content_args,
        )

        start >> calculate_patrolled_recentchanges >> end
