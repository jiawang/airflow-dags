# Test dag used as an input for the Airflow metrics docker compose example.
# Used to test SLA miss.
# type: ignore

import time
from datetime import timedelta

import pendulum
from airflow import DAG
from airflow.decorators import task

with DAG(
    dag_id="sla_error_dag",
    schedule="3 * * * *",
    start_date=pendulum.datetime(2023, 1, 1, tz="UTC"),
    catchup=False,
    tags=["metrics-example"],
) as dag:

    @task(sla=timedelta(minutes=1))
    def print_array():
        """Print Numpy array."""
        # import numpy as np  # <- THIS IS HOW NUMPY SHOULD BE IMPORTED IN THIS CASE!
        time.sleep(10)
        error  # noqa: F821 unknown variable

    print_array()
