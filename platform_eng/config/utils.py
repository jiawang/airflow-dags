from datetime import datetime

# dumps are always the 1st & 20th of the day and become available a day after
# (resp. 2 & 21); adding another extra day (3 & 22) just to be on the safe side
def latest_html_dumps_snapshot_before(d: datetime) -> str:
    return datetime(
        d.year if d.month > 1 or d.day >= 3 else d.year - 1,
        d.month if d.day >= 3 else (d.month - 1 if d.month > 1 else 12),
        1 if 3 <= d.day < 22 else 20,
    ).strftime('%Y%m%d')
