#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate badly parsed pages
"""

from datetime import datetime, timedelta

from os import path

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hadoop_name_node

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

from airflow import DAG

from mergedeep import merge

props = DagProperties(
    # DAG
    alerts_email="sd-alerts@lists.wikimedia.org",
    dag_id="check_bad_parsing",
    start_date=datetime(2024, 9, 18),
    schedule="0 0 1 * *",  # The DAG starts on the first of each month
    timeout=timedelta(days=4 * 7),  # Times out after four weeks
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    monthly_snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    conda_env=artifact("section-topics-0.16.0-v0.16.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
)
args = merge(
    {},
    default_args,
    {
        "email": [default_email, props.alerts_email],
    }
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
) as dag:
    if not props.monthly_snapshot:
        monthly = "{{ data_interval_start.start_of('month').format('YYYY-MM') }}"
    else:
        monthly = props.monthly_snapshot

    wait_for_wikitext = URLSensor(
        task_id="wait_for_wikitext",
        # URL for `wmf.mediawiki_wikitext_current`
        url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
        poke_interval=props.sensors_poke_interval,
        timeout=timedelta(days=24).total_seconds(),
    )

    check_bad_parsing_dir = path.join(props.work_dir, 'bad_parsing')
    check_bad_parsing = SparkSubmitOperator.for_virtualenv(
        task_id="check_bad_parsing",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/scripts/check_bad_parsing.py",
        application_args=[
            monthly,
            # @todo look into adding more wikis at some point, but it might require some script optimization;
            #   *platform_eng.config.dag_config.wikis
            # Initial targets, only ptwiki was problematic
            "ptwiki",
            # First scale up - https://phabricator.wikimedia.org/T364374
            "ckbwiki", "frrwiki", "hywiki", "jvwiki", "kuwiki",
            "newiki", "pawiki", "simplewiki", "skwiki", "sqwiki",
            "--output", path.join(check_bad_parsing_dir, monthly),
        ],
        launcher="skein",
    )

    # remove all but the last 4 outputs
    cleanup_check_bad_parsing = SimpleSkeinOperator(
        task_id="cleanup_check_bad_parsing",
        script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {check_bad_parsing_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
    )

    wait_for_wikitext >> check_bad_parsing >> cleanup_check_bad_parsing
