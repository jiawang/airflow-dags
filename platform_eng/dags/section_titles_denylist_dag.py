#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate section titles denylist
"""

from datetime import datetime, timedelta

from os import path

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hadoop_name_node

from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

from airflow import DAG

from mergedeep import merge

props = DagProperties(
    # DAG
    alerts_email="sd-alerts@lists.wikimedia.org",
    dag_id="section_titles_denylist",
    start_date=datetime(2024, 9, 18),
    schedule="0 0 * * 1",  # The DAG starts on Mondays at midnight
    timeout=timedelta(days=6),  # Times out on Sundays, before the next run
    catchup=False,
    tags=["structured-data-team"],
    # Default snapshots are set within the DAG via templates.
    # Use these properties to override them.
    weekly_snapshot="",
    # Sensors
    sensors_poke_interval=timedelta(hours=1).total_seconds(),  # Check every hour
    conda_env=artifact("section-topics-0.16.0-v0.16.0.conda.tgz"),
    work_dir="/user/analytics-platform-eng/structured-data/section_topics",
    seal_path="/user/analytics-platform-eng/structured-data/seal/alignments/{weekly}",
)
args = merge(
    {},
    default_args,
    {
        "email": [default_email, props.alerts_email],
    }
)

with DAG(
    dag_id=props.dag_id,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=props.schedule,
    dagrun_timeout=props.timeout,
    catchup=props.catchup,
    tags=props.tags,
    default_args=args,
) as dag:
    if not props.weekly_snapshot:
        weekly = "{{ data_interval_start.format('YYYY-MM-DD') }}"
    else:
        weekly = props.weekly_snapshot

    seal_path = props.seal_path.format(weekly=weekly)
    wait_for_seal = URLSensor(
        task_id="wait_for_seal",
        url=f"{hadoop_name_node}/{seal_path}/_SUCCESS",
        poke_interval=props.sensors_poke_interval,
    )

    denylist_dir = path.join(props.work_dir, 'section_titles_denylist')
    gather_section_titles_denylist = SparkSubmitOperator.for_virtualenv(
        task_id="gather_section_titles_denylist",
        virtualenv_archive=props.conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/scripts/gather_section_titles_denylist.py",
        application_args=[
            "--aligned-sections", seal_path,
            "--output", path.join(denylist_dir, weekly),
        ],
        launcher="skein",
    )

    # remove all but the last 4 outputs
    cleanup_gather_section_titles_denylist = SimpleSkeinOperator(
        task_id="cleanup_gather_section_titles_denylist",
        script=f"xargs --no-run-if-empty hdfs dfs -rm -r <<< $(hdfs dfs -ls {denylist_dir} | head -n -4 | tr -s ' ' | cut -d' ' -f8)",
    )

    wait_for_seal >> gather_section_titles_denylist >> cleanup_gather_section_titles_denylist
