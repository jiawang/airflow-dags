import json

from airflow.models import Variable
from pydantic.json import pydantic_encoder
from typing_extensions import Self

from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = "hdfs://analytics-hadoop"
refinery_directory = f"{hadoop_name_node}/wmf/refinery"
hql_directory = f"{refinery_directory}/current/hql"
artifacts_directory = f"{refinery_directory}/current/artifacts"
hdfs_temp_directory = "/tmp/research"
swift_auth_file = f"{hadoop_name_node}/user/analytics-research/swift_auth_research_poc.env"

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# synatactic sugar to use when declaring artifacts in dags.
artifact = ArtifactRegistry.for_wmf_airflow_instance("research").artifact_url

# DAG default_args for this Airflow Instance.
instance_default_args = {
    "owner": "analytics-research",
    "email": "research-engineering-alerts@lists.wikimedia.org",
    "metastore_conn_id": "analytics-hive",
    "spark_sql_driver_jar_path": artifact("wmf-sparksqlclidriver-1.0.0.jar"),
    "hdfs_tools_shaded_jar_path": artifact("hdfs-tools-0.0.6-shaded.jar"),
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args["queue"] = "production"

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(instance_default_args)


class BaseProperties:
    """Base class to load an airflow variable into a dataclass"""

    @classmethod
    def from_variable(cls, variable_key: str) -> Self:
        """Load properties from the airflow variable associated with `variable_key`.
        If no value is found for the given key, the variable will be initialized
        with the defaults in `DagProperties`.
        """
        # Get the current value of the variable or initialize with
        # an empty dict and store that as the value in json
        variable_value = Variable.setdefault(variable_key, default=dict(), deserialize_json=True)

        # Initialize DagProperties and override properties found in value
        dag_properties = cls(**variable_value)

        dag_properties_json = json.dumps(dag_properties, indent=4, default=pydantic_encoder)
        if json.loads(dag_properties_json) != variable_value:
            Variable.update(key=variable_key, value=dag_properties_json)

        return dag_properties
