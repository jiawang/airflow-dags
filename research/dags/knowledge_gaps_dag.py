"""A DAG to execute the full knowledge gaps pipeline."""

import os
from dataclasses import field
from datetime import datetime, timedelta
from pathlib import Path
from typing import Literal

from airflow import DAG
from airflow.decorators import task_group
from airflow.operators.bash import BashOperator
from airflow.operators.empty import EmptyOperator
from pydantic.dataclasses import dataclass

from research.config import dag_config
from research.dags import snapshot_sensor
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

# the list of content gap metrics to release publicly
default_published_content_gaps: list[str] = [
    "gender",
    "geography_country",
    "geography_cultural_wmf_region",
    "geography_wmf_region",
    "multimedia_illustrated",
    "sexual_orientation",
    "time",
]


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    # Dag configuration
    mode: Literal["development", "production"] = "development"
    alerts_email: str = "research-engineering-alerts@lists.wikimedia.org"
    conda_env: str = dag_config.artifact("knowledge_gaps.tgz")
    # if specified, use CI built conda env for custom branch instead of `conda_env`
    gitlab_branch: str | None = None
    # Pipeline arguments
    mediawiki_snapshot: str = "{{ data_interval_start | to_ds_month }}"
    wikidata_snapshot: str = "{{data_interval_end | start_of_current_week | to_ds}}"
    start_time_bucket: str = "20010101"
    end_time_bucket: str = (
        "{{data_interval_start | end_of_current_month | to_ds_nodash}}"
    )
    time_bucket_freq: Literal["monthly", "yearly"] = "monthly"
    published_content_gaps: list[str] = field(
        default_factory=lambda: default_published_content_gaps
    )
    #  hdfs dir for intermediate data, cannot include include hdfs:// scheme
    hdfs_dir: Path = Path(dag_config.hdfs_temp_directory) / "knowledge_gaps"
    # hdfs dir for published dataset, cannot include include hdfs:// scheme
    publish_dir: Path | None = None
    # hive database for storing output tables
    output_database: str = "knowledge_gaps_dev"
    # table prefix for output hive tables
    table_prefix: str = "output_"
    # development configuration
    # hive database that contains dev input datasets
    dev_database_name: str = "knowledge_gaps_dev"
    dev_table_prefix: str = "dev_"
    # list of content gaps to generate, None means all.
    content_gaps: list[str] | None = None
    # control flow
    skip_metric_features: bool = False
    skip_content_gap_features: bool = False
    skip_content_gap_metrics: bool = False
    skip_output_datasets: bool = False


dag_id = "knowledge_gaps"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {"email": props.alerts_email}

# optionally use a custom conda environment
if props.gitlab_branch:
    knowledge_gaps_version = props.gitlab_branch
    knowledge_gaps_conda_env_name = f"knowledge_gaps-{knowledge_gaps_version}.conda"
    conda_environment = f"https://gitlab.wikimedia.org/api/v4/projects/212/packages/generic/knowledge_gaps/{knowledge_gaps_version}/{knowledge_gaps_conda_env_name}.tgz#venv"
else:
    conda_environment = props.conda_env


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=datetime(2022, 1, 1),
    schedule="@monthly",
    dagrun_timeout=timedelta(days=30),
    catchup=False,
    user_defined_filters=filters,
    tags=["spark", "hive", "research", "knowledge gaps"],
    default_args=default_args,
) as dag:
    # for production, the airflow variable has to be set to "production"
    mode = props.mode
    is_production = mode == "production"

    # extract fields that depend on templating via airflow variable
    mediawiki_snapshot = props.mediawiki_snapshot
    wikidata_snapshot = props.wikidata_snapshot

    # hdfs directory for storing state for this run
    hdfs_dir = str(props.hdfs_dir / mediawiki_snapshot)
    # development mode configuration
    dev_input_config = []
    content_gaps_to_generate = []
    if mode == "development":
        dev_input_config.extend(
            [
                "--dev_database_name",
                props.dev_database_name,
                "--dev_table_prefix",
                props.dev_table_prefix,
            ]
        )
        if props.content_gaps:
            content_gaps_to_generate = [
                "--content_gaps",
                props.content_gaps,
            ]

    common_params = {
        "launcher": "skein",
        "driver_memory": "4G",
        # As of 07/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        "skein_app_log_collection_enabled": False if is_production else True,
        # to keep the skein containers running for development
        # 'command_postamble': '; sleep 600',
    }

    spark_conf = {
        "spark.shuffle.service.enabled": "true",
        "spark.executor.memoryOverhead": "2048",
        "spark.dynamicAllocation.enabled": "true",
        "spark.hadoop.fs.permissions.umask-mode": "022",
    }

    def compute_feature_metrics():
        if props.skip_metric_features:
            return EmptyOperator(task_id="metric_features_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 120 if is_production else 10,
            "spark.sql.shuffle.partitions": 4000 if is_production else 20,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id="metric_features",
            **common_params,
            executor_memory="14G" if is_production else "4G",
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/knowledge_gaps_feature_metrics.py",
            application_args=[
                props.start_time_bucket,
                props.end_time_bucket,
                "--time_bucket_freq",
                props.time_bucket_freq,
                "--mediawiki_snapshot",
                mediawiki_snapshot,
                "--mode",
                mode,
                "--hdfs_dir",
                hdfs_dir,
            ]
            + dev_input_config,
        )

    def compute_content_features():
        if props.skip_content_gap_features:
            return EmptyOperator(task_id="content_gap_features_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 120 if is_production else 10,
            "spark.sql.shuffle.partitions": 2000 if is_production else 20,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id="content_gap_features",
            **common_params,
            executor_memory="14G" if is_production else "4G",
            executor_cores=2,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/knowledge_gaps_content_features.py",
            application_args=[
                "--mediawiki_snapshot",
                mediawiki_snapshot,
                "--wikidata_snapshot",
                wikidata_snapshot,
                "--mode",
                mode,
                "--hdfs_dir",
                hdfs_dir,
            ]
            + dev_input_config
            + content_gaps_to_generate,
        )

    def compute_content_gap_metrics():
        if props.skip_content_gap_metrics:
            return EmptyOperator(task_id="content_gap_metrics_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 120 if is_production else 10,
            "spark.sql.shuffle.partitions": 1000 if is_production else 20,
        }
        return SparkSubmitOperator.for_virtualenv(
            task_id="content_gap_metrics",
            **common_params,
            executor_memory="14G" if is_production else "4G",
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/knowledge_gaps_aggregation.py",
            application_args=[
                "--hdfs_dir",
                hdfs_dir,
            ]
            + content_gaps_to_generate,
        )

    def generate_output_datasets():
        if props.skip_output_datasets:
            return EmptyOperator(task_id="output_datasets_skipped")

        specific_conf = {
            "spark.dynamicAllocation.maxExecutors": 40 if is_production else 4,
            "spark.sql.shuffle.partitions": 20 if is_production else 2,
        }
        prefix = (
            []
            if props.table_prefix == ""
            else [
                "--table_prefix",
                props.table_prefix,
            ]
        )
        return SparkSubmitOperator.for_virtualenv(
            task_id="output_datasets",
            **common_params,
            executor_memory="8G",
            executor_cores=4,
            conf={**spark_conf, **specific_conf},
            virtualenv_archive=conda_environment,
            entry_point="bin/knowledge_gaps_datasets.py",
            application_args=[
                "--hdfs_dir",
                hdfs_dir,
                "--database_name",
                props.output_database,
                "--store_metric_features",
                "--store_content_gap_features",
                "--store_csv_format",
            ]
            + prefix,
        )

    # this is an example where it would be good to have the knowledge gap repo installed in the airflow instance
    # the path for the published data is defined in that repo, so it would be nice to use that path instead
    # of hardcoding it here
    @task_group(group_id="publish_datasets")
    def publish_datasets():
        if not props.publish_dir:
            EmptyOperator(task_id="publish_datasets_skipped")
        else:
            _ = [
                # by category is stored per content_gap
                HDFSArchiveOperator(
                    task_id=f"publish_parquet_by_category_{content_gap}_gap",
                    source_directory=f"{hdfs_dir}/single_parquet/by_category/content_gap={content_gap}",
                    expected_filename_ending=".parquet",
                    archive_file=os.path.join(
                        props.publish_dir,
                        "content_gaps",
                        mediawiki_snapshot,
                        "by_category",
                        f"{content_gap}.parquet",
                    ),
                )
                for content_gap in props.published_content_gaps
            ] + [
                # the remaining aggregation levels are stored in a single file
                HDFSArchiveOperator(
                    task_id=f"publish_parquet_{name}",
                    source_directory=f"{hdfs_dir}/single_parquet/{name}/",
                    expected_filename_ending=".parquet",
                    check_done=True,
                    archive_file=os.path.join(
                        props.publish_dir,
                        "content_gaps",
                        mediawiki_snapshot,
                        name,
                        f"{name}.parquet",
                    ),
                )
                for name in [
                    "by_category_all_wikis",
                    "by_content_gap",
                    "by_content_gap_all_wikis",
                ]
            ]

            _ = [
                HDFSArchiveOperator(
                    task_id=f"publish_csv_{content_gap}_gap",
                    source_directory=f"{hdfs_dir}/knowledge_gap_index_metrics_csv/content_gap={content_gap}",
                    expected_filename_ending=".csv",
                    archive_file=os.path.join(
                        props.publish_dir, "content_gaps", "csv", f"{content_gap}.csv"
                    ),
                )
                for content_gap in props.published_content_gaps
            ]

            # survey datasets
            _ = BashOperator(
                task_id="publish_surveys",
                bash_command=f"hdfs dfs -cp /wmf/data/research/knowledge_gaps/surveys {props.publish_dir} && hdfs dfs -chmod -R +r {props.publish_dir}/surveys",
            )

    @task_group(group_id="knowledge_gaps_sensors")
    def sensors():
        _ = [
            snapshot_sensor.wait_for_article_features(mediawiki_snapshot),
            snapshot_sensor.wait_for_mediawiki_page_history_snapshot(
                mediawiki_snapshot
            ),
            snapshot_sensor.wait_for_mediawiki_page_snapshot(mediawiki_snapshot),
            snapshot_sensor.wait_for_mediawiki_revision_snapshot(mediawiki_snapshot),
            snapshot_sensor.wait_for_wikidata_item_page_link_snapshot(
                wikidata_snapshot
            ),
            snapshot_sensor.wait_for_wikidata_entity_snapshot(wikidata_snapshot),
        ]

    _ = (
        sensors()
        >> [compute_feature_metrics(), compute_content_features()]
        >> compute_content_gap_metrics()
        >> generate_output_datasets()
        >> publish_datasets()
    )
