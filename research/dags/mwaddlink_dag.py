"""A DAG to execute the link-recommendation pipeline. Generates dataset for any number of
wiki lanaguages, trains a lanaguge agnostic model, and evalutes all the langauges with the
trained model.
"""

from dataclasses import field
from functools import reduce
import getpass
from datetime import datetime
from pathlib import Path

from airflow import DAG
from pydantic.dataclasses import dataclass

from research.config import args, dag_config
from research.config.wikis import WIKIS
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

# shards based on https://noc.wikimedia.org/db.php
SHARDS: dict[str, list[str]] = {
    "s1": ["enwiki"],
    "s2": [
        "bgwiki",
        "cswiki",
        "eowiki",
        "fiwiki",
        "idwiki",
        "itwiki",
        "nlwiki",
        "nowiki",
        "plwiki",
        "ptwiki",
        "svwiki",
        "thwiki",
        "trwiki",
        "zhwiki",
    ],
    "s5": [
        "altwiki",
        "amiwiki",
        "anpwiki",
        "avkwiki",
        "bbcwiki",
        "bewwiki",
        "blkwiki",
        "cebwiki",
        "dagwiki",
        "dewiki",
        "dgawiki",
        "dtpwiki",
        "fatwiki",
        "fonwiki",
        "gpewiki",
        "gucwiki",
        "gurwiki",
        "guwwiki",
        "iglwiki",
        "kcgwiki",
        "kuswiki",
        "lldwiki",
        "mniwiki",
        "niawiki",
        "madwiki",
        "muswiki",
        "pcmwiki",
        "pwnwiki",
        "skrwiki",
        "shwiki",
        "shiwiki",
        "smnwiki",
        "srwiki",
        "taywiki",
        "tlywiki",
        "trvwiki",
        "zghwiki",
    ],
    "s6": ["frwiki", "jawiki", "ruwiki"],
    "s7": [
        "eswiki",
        "huwiki",
        "hewiki",
        "ukwiki",
        "arwiki",
        "cawiki",
        "viwiki",
        "fawiki",
        "rowiki",
        "kowiki",
    ],
}
# rest of the wikis
SHARDS["s3"] = list(set(WIKIS).difference(set(sum(list(SHARDS.values()), []))))


@dataclass(frozen=True)
class DagProperties(dag_config.BaseProperties):
    conda_env: str = dag_config.artifact("research_datasets.tgz")
    # Pipeline arguments
    mediawiki_snapshot: str = "{{ data_interval_start | to_ds_month }}"
    wikidata_snapshot: str = "{{data_interval_end | start_of_current_week | to_ds}}"
    # model groups of wiki_dbs, one model is trained for each group
    model_groups: dict[str, list[str]] = field(default_factory=lambda: SHARDS)
    wikidata_properties_keep: list[str] = field(default_factory=lambda: list(["P31"]))
    # max number of sentences in the training dataset, per wiki
    max_training_sentences_per_wiki: int = 200000
    # use grid search during model training
    grid_search: bool = False
    eval_thresholds: list[float] = field(
        default_factory=lambda: [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
    )
    max_eval_recs: int | None = None
    hdfs_dir: Path = Path(dag_config.hdfs_temp_directory) / "mwaddlink"
    # Spark configuration
    shuffle_partitions: int = 4096
    driver_memory: str = "4G"
    executor_cores: int = 4
    executor_memory: str = "16G"
    executor_memory_overhead: str = "4G"
    max_executors: int = 98


dag_id = "mwaddlink"
props = DagProperties.from_variable(dag_id)
default_args = dag_config.default_args | {
    "do_xcom_push": True,
}
username = getpass.getuser()

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=datetime(2024, 5, 2),
    schedule=None,
    user_defined_filters=filters,
    default_args=dag_config.default_args | {},
    tags=[
        "research",
        "mwaddlink",
        "from_hive",
        "from_hdfs",
        "to_hdfs",
        "uses_spark",
    ],
    catchup=False,
) as dag:
    common_params = {"retries": 0}
    common_skein_params = {
        "files": {"venv": props.conda_env},
        "env": {"LD_LIBRARY_PATH": "venv/lib"},
        "resources": {"memory": "16 GiB", "vcores": 1},
        **common_params,
    }
    common_spark_params = {
        "launcher": "skein",
        "driver_memory": "16G",
        "executor_memory": "16G",
        "executor_cores": 4,
        "use_virtualenv_spark": True,
        "virtualenv_archive": props.conda_env,
        "env_vars": {"LD_LIBRARY_PATH": "venv/lib"},
        "conf": {
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.driver.maxResultSize": "2G",
            "spark.sql.execution.arrow.pyspark.enabled": True,
            "spark.executor.memoryOverhead": "2G",
        },
        **common_params,
    }

    def generate_anchor_dictionary(wiki_dbs: list[str]):
        c_args = args.MwaddlinkGenerateanchordictionaryRun(
            snapshot=props.mediawiki_snapshot, wiki_dbs=wiki_dbs, output=props.hdfs_dir
        )
        spark_params = common_spark_params.copy()
        spark_params["conf"]["spark.sql.shuffle.partitions"] = 2000
        spark_params["conf"]["spark.network.timeout"] = "1200s"
        spark_params["conf"]["spark.shuffle.io.retryWait"] = "1200s"

        command_postamble = (
            f"&& ls -l && hdfs dfs -put -f anchor.bloom {props.hdfs_dir}"
        )

        return SparkSubmitOperator.for_virtualenv(
            task_id="generate_anchor_dictionary",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.generateanchordictionary.run", c_args.json()],
            command_postamble=command_postamble,
            **spark_params,
        )

    def generate_wdproperties(wiki_dbs: list[str]):
        c_args = args.MwaddlinkGeneratewdpropertiesRun(
            wikidata_snapshot=props.wikidata_snapshot,
            wiki_dbs=wiki_dbs,
            wikidata_properties=props.wikidata_properties_keep,
            directory=props.hdfs_dir,
        )
        return SparkSubmitOperator.for_virtualenv(
            task_id="generate_wdproperties",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.generatewdproperties.run", c_args.json()],
            **common_spark_params,
        )

    def filter_dict_anchor():
        c_args = args.MwaddlinkFilterdictanchorRun(directory=props.hdfs_dir)
        return SparkSubmitOperator.for_virtualenv(
            task_id="filter_dict_anchor",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.filterdictanchor.run", c_args.json()],
            **common_spark_params,
        )

    def generate_backtesting_data(wiki_dbs: list[str]):
        c_args = args.MwaddlinkGeneratebacktestingdataRun(
            snapshot=props.mediawiki_snapshot,
            wiki_dbs=wiki_dbs,
            directory=props.hdfs_dir,
            max_sentences_per_wiki=props.max_training_sentences_per_wiki,
        )
        return SparkSubmitOperator.for_virtualenv(
            task_id="generate_backtesting_data",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.generatebacktestingdata.run", c_args.json()],
            **common_spark_params,
        )

    def generate_training_data():
        c_args = args.MwaddlinkGeneratetrainingdataRun(directory=props.hdfs_dir)
        command_preamble = (
            f"hdfs dfs -get {props.hdfs_dir /'anchor.bloom'} && ls -l && "
        )
        return SparkSubmitOperator.for_virtualenv(
            task_id="generate_training_data",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.generatetrainingdata.run", c_args.json()],
            command_preamble=command_preamble,
            **common_spark_params,
        )

    def generate_addlink_model(model_id: str, model_wiki_dbs: list[str]):
        c_args = args.MwaddlinkGenerateaddlinkmodelRun(
            wiki_dbs=model_wiki_dbs,
            model_id=model_id,
            directory=props.hdfs_dir,
            grid_search=props.grid_search,
        )
        skein_params = common_skein_params.copy()
        skein_params["resources"]["vcores"] = 8
        skein_params["resources"]["memory"] = "24 GiB"
        return SimpleSkeinOperator(
            task_id=f"model_{model_id}",
            script=" && ".join(
                [
                    "source venv/bin/activate",
                    f"python3 -m research_datasets mwaddlink.generateaddlinkmodel.run '{c_args.json()}'",
                ]
            ),
            **skein_params,
        )

    def generate_backtesting_eval(model_id: str, model_wiki_dbs: list[str]):
        c_args = args.MwaddlinkGeneratebacktestingevalRun(
            model_id=model_id,
            wiki_dbs=model_wiki_dbs,
            directory=props.hdfs_dir,
            thresholds=props.eval_thresholds,
            n_max=props.max_eval_recs,
        )
        # The driver does lots of work, beefy config
        spark_params = common_spark_params.copy()
        spark_params["driver_memory"] = "24G"
        spark_params["driver_cores"] = "8"

        return SparkSubmitOperator.for_virtualenv(
            task_id=f"model_eval_{model_id}",
            entry_point="bin/research_datasets_pipelines.py",
            application_args=["mwaddlink.generatebacktestingeval.run", c_args.json()],
            **spark_params,
        )

    dag_config.hadoop_name_node

    all_wiki_dbs = reduce(lambda l, r: l + r, props.model_groups.values())
    all_wiki_dbs = sorted(all_wiki_dbs)

    anchors = generate_anchor_dictionary(all_wiki_dbs)
    wd_properties = generate_wdproperties(all_wiki_dbs)
    filtered_anchors = filter_dict_anchor()
    backtesting_data = generate_backtesting_data(all_wiki_dbs)
    training_data = generate_training_data()
    anchors >> wd_properties >> filtered_anchors >> backtesting_data >> training_data

    for i, model_wiki_dbs in enumerate(props.model_groups.values()):
        model_wiki_dbs = sorted(model_wiki_dbs)
        model_id = f"model_{i}"
        train_model = generate_addlink_model(model_id, model_wiki_dbs)
        eval_model = generate_backtesting_eval(model_id, model_wiki_dbs)
        training_data >> train_model >> eval_model
