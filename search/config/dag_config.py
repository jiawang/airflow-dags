from typing import Sequence

from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = 'hdfs://analytics-hadoop'
hdfs_temp_directory = 'hdfs://analytics-hadoop/tmp'
data_path = f'{hadoop_name_node}/wmf/data/discovery'

refinery_directory = f'{hadoop_name_node}/wmf/refinery'
refinery_artifacts = f'{refinery_directory}/current/artifacts'

# Emails.
alerts_email = 'discovery-alerts@lists.wikimedia.org'

# default_args for this Airflow Instance.
instance_default_args = {
    'owner': 'analytics-search',
    'email': [alerts_email],
    'hadoop_name_node': hadoop_name_node,
    'metastore_conn_id': 'analytics-hive',
    'deploy_mode': 'cluster',
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args['queue'] = 'production'

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance('search')
artifact = artifact_registry.artifact_url


# Default arguments for all operators used by this airflow instance.
def get_default_args():
    # default_args for this Airflow Instance.
    instance_default_args = {
        'owner': 'analytics-search',
        'email': [alerts_email],
        'hadoop_name_node': hadoop_name_node,
        'metastore_conn_id': 'analytics-hive',
        'deploy_mode': 'cluster',
        'conf': {
            # user read/write/exec, group read/exec, deny others
            'spark.hadoop.fs.permissions.umask-mode': '0027',
        }
    }

    # For jobs running in production instances, set the production yarn queue.
    if is_wmf_airflow_instance():
        instance_default_args['queue'] = 'production'

    return dag_default_args.get(instance_default_args)


# Discolytics environment used by most python dags
discolytics_env_version = '0.26.0'
discolytics_conda_env_tgz = artifact(f'discolytics-{discolytics_env_version}.conda.tgz')

wmf_props = VariableProperties('wmf_conf')
eventgate_datacenters = wmf_props.get("eventgate_datacenters", ["eqiad", "codfw"])

# Local path to the root of the refinery repository (analytics/refinery in gerrit) on the airflow server
refinery_local_directory = wmf_props.get('wikimedia_discovery_analytics_path', '/srv/deployment/analytics/refinery')

# Path to the jar containing WDQS/WCQS spark jobs
wdqs_spark_tools = artifact('rdf-spark-tools-0.3.146-jar-with-dependencies.jar')

# execution date formatted as hive partition with year=/month=/day=/hour=
YMDH_PARTITION = \
    'year={{ execution_date.year }}/month={{ execution_date.month }}/' \
    'day={{ execution_date.day }}/hour={{ execution_date.hour }}'

# execution date formatted as hive partition with year=/month=/day=
YMD_PARTITION = \
    'year={{ execution_date.year }}/month={{ execution_date.month }}/' \
    'day={{ execution_date.day }}'

# Local path to the operations/mediawiki-config repository on the airflow server
mediawiki_config_path = wmf_props.get('mediawiki_config_path', '/srv/mediawiki-config')

https_proxy = wmf_props.get('https_proxy', 'http://webproxy.eqiad.wmnet:8080')

# Local path to credentials for the mariadb replicas on the airflow server
mariadb_credentials_path = wmf_props.get('mariadb_credentials_path',
                                         f'{hadoop_name_node}'
                                         f'/user/analytics-search/mysql-analytics-research-client-pw.txt')


def eventgate_partitions(table_tmpl: str,
                         data_centers: Sequence[str] = eventgate_datacenters,
                         datetime_partition: str = YMDH_PARTITION) -> Sequence[str]:
    """Sequence of hive partition spec strings for (one hour) of eventgate input

    Eventgate inputs are consistent across use cases. They come with a
    partition for every active datacenter, and have the same hourly
    partitioning scheme across all tables.

    By default, it extracts hourly data. If datetime_partition is provided,
    it can extract daily or monthly data.

    Input table_tmpl is a templated string. Output suitable for use in
    NamedHivePartitionSensor.partition_names
    """
    return [
        f"{table_tmpl}/datacenter={dc}/{datetime_partition}"
        for dc in data_centers
    ]
