""" Subgraph And Query Metrics Init Dag
      Create table and partitions for subgraph_metrics_weekly dag and subgraph_query_metrics_daily
      dag
    Subgraph Metrics Weekly Dag
      Collects aggregate metrics about wikidata subgraphs and saves into a table.
      Also collects per-subgraph and subgraph-pair metrics about wikidata subgraphs
      and saves into tables.
      Runs every monday to match with the wikidata dumps and other input data.
    Subgraph Query Metrics Daily Dag
      Extracts general query metrics, subgraph query metrics, per-subgraph query
      metrics, and subgraph-pair query metrics and saves each in tables.
      Runs daily.
"""

from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator

from search.config.dag_config import data_path, wdqs_spark_tools, YMD_PARTITION, get_default_args, eventgate_datacenters
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor, RangeHivePartitionSensor

var_props = VariableProperties('subgraph_and_query_metrics_conf')

wikidata_dumps_table = var_props.get("wikidata_dumps_table", "discovery.wikibase_rdf")
all_subgraphs_table = var_props.get("all_subgraphs_table", "discovery.all_subgraphs")
top_subgraph_items_table = var_props.get("top_subgraph_items_table", "discovery.top_subgraph_items")
top_subgraph_triples_table = var_props.get("top_subgraph_triples_table", "discovery.top_subgraph_triples")
min_items = var_props.get("min_items", 10000)
event_sparql_query_table = var_props.get("event_sparql_query_table", "event.wdqs_external_sparql_query")
processed_query_table = var_props.get("processed_query_table", "discovery.processed_external_sparql_query")
subgraph_qitem_match_table = var_props.get("subgraph_qitem_match_table", "discovery.subgraph_qitems_match")
subgraph_predicate_match_table = var_props.get("subgraph_predicate_match_table", "discovery.subgraph_predicates_match")
subgraph_uri_match_table = var_props.get("subgraph_uri_match_table", "discovery.subgraph_uri_match")
subgraph_query_mapping_table = var_props.get("subgraph_query_mapping_table", "discovery.subgraph_queries")
filtering_limit = var_props.get("filtering_limit", 99)
top_n = var_props.get("top_n", 100)
wiki = var_props.get("wiki", "wikidata")

# public data tables
general_subgraph_metrics_table = var_props.get("general_subgraph_metrics_table", "discovery.general_subgraph_metrics")
per_subgraph_metrics_table = var_props.get("per_subgraph_metrics_table", "discovery.per_subgraph_metrics")
subgraph_pair_metrics_table = var_props.get("subgraph_pair_metrics_table", "discovery.subgraph_pair_metrics")

# query (possibly PII) data tables
general_subgraph_query_metrics_table = var_props.get("general_subgraph_query_metrics_table",
                                                     "discovery.general_subgraph_query_metrics")
general_query_metrics_table = var_props.get("general_query_metrics_table", "discovery.general_query_metrics")
per_subgraph_query_metrics_table = var_props.get("per_subgraph_query_metrics_table",
                                                 "discovery.per_subgraph_query_metrics")
subgraph_pair_query_metrics_table = var_props.get("subgraph_pair_query_metrics_table",
                                                  "discovery.subgraph_pair_query_metrics")

# public data folders
rel_general_subgraph_metrics_location = var_props.get("rel_general_subgraph_metrics_location",
                                                      "wikidata/subgraph_analysis/general_subgraph_metrics")
rel_per_subgraph_metrics_location = var_props.get("rel_per_subgraph_metrics_location",
                                                  "wikidata/subgraph_analysis/per_subgraph_metrics")
rel_subgraph_pair_metrics_location = var_props.get("rel_subgraph_pair_metrics_location",
                                                   "wikidata/subgraph_analysis/subgraph_pair_metrics")

# query (possibly PII) data folders
rel_general_query_metrics_location = var_props.get("rel_general_query_metrics_location",
                                                   "query_service/general_query_metrics")
rel_general_subgraph_query_metrics_location = var_props.get("rel_general_subgraph_query_metrics_location",
                                                            "query_service/general_subgraph_query_metrics")
rel_per_subgraph_query_metrics_location = var_props.get("rel_per_subgraph_query_metrics_location",
                                                        "query_service/per_subgraph_query_metrics")
rel_subgraph_pair_query_metrics_location = var_props.get("rel_subgraph_pair_query_metrics_location",
                                                         "query_service/subgraph_pair_query_metrics")

# Default kwargs for all Operators
default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2022, 7, 1)),
})

public_dataset_sparkconf = {
    **default_args.get('conf', {}),
    "spark.hadoop.fs.permissions.umask-mode": "0022",
}

with DAG(
        'subgraph_and_query_metrics_init',
        default_args=default_args,
        schedule='@once'
) as subgraph_and_query_metrics_dag_init:
    complete = DummyOperator(task_id='complete')
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE IF NOT EXISTS {general_subgraph_metrics_table} (
                `total_items`                        bigint  COMMENT 'Total number of items in Wikidata',
                `total_triples`                      bigint  COMMENT 'Total number of triples in Wikidata',
                `percent_subgraph_item`              double  COMMENT 'Percentage of items covered by the top subgraphs',
                `percent_subgraph_triples`           double  COMMENT 'Percentage of triples covered by the top subgraphs',
                `num_subgraph`                       bigint  COMMENT 'Number of subgraphs in wikidata (using groups of P31)',
                `num_top_subgraph`                   bigint  COMMENT 'Number of top subgraphs (has at least `minItems` items)',
                `subgraph_size_percentiles`          array<double>  COMMENT 'List of values containing the percentile values from 0.1 to 0.9 of the size of subgraphs (in triples)',
                `subgraph_size_mean`                 double  COMMENT 'Mean of the size (in triples) of all subgraphs'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_general_subgraph_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {per_subgraph_metrics_table} (
                `subgraph`                          string  COMMENT 'URI of the subgraphs in wikidata',
                `item_count`                        bigint  COMMENT 'Total number of items/entities in subgraph',
                `triple_count`                      bigint  COMMENT 'Total number of triples in subgraph',
                `predicate_count`                   bigint  COMMENT 'Total number of distinct predicates in subgraph',
                `item_percent`                      double  COMMENT 'Percent of items/entities in subgraph compared to total items in Wikidata',
                `triple_percent`                    double  COMMENT 'Percent of triples in subgraph compared to total triples in Wikidata',
                `density`                           double  COMMENT 'Average triples per item, represents density of subgraphs',
                `item_rank`                         bigint  COMMENT 'Rank of the subgraph by number of items it contains in descending order',
                `triple_rank`                       bigint  COMMENT 'Rank of the subgraph by number of triples it contains in descending order',
                `triples_per_item_percentiles`      array<double>  COMMENT 'List of 0.1 to 0.9 percentile of triples per item in each subgraph',
                `triples_per_item_mean`             double  COMMENT 'Mean of triples per item in each subgraph',
                `num_direct_triples`                bigint  COMMENT 'Number of direct triples (triples that are not statements)',
                `num_statements`                    bigint  COMMENT 'Number of statements (wikidata.org/prop/)',
                `num_statement_triples`             bigint  COMMENT 'Number of triples in the full statements (everything within the statements)',
                `predicate_counts`                  map<string, bigint>  COMMENT 'Map of predicates and the number of its occurrences in the subgraph',
                `subgraph_to_WD_triples`            bigint  COMMENT 'Number of triples connecting this subgraph to other subgraphs',
                `WD_to_subgraph_triples`            bigint  COMMENT 'Number of triples connecting other subgraphs to this subgraph'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_per_subgraph_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {subgraph_pair_metrics_table} (
                `subgraph1`                                string  COMMENT 'First subgraph of the subgraph pair',
                `subgraph2`                                string  COMMENT 'Second subgraph of the subgraph pair',
                `triples_from_1_to_2`                      bigint  COMMENT 'Number of directed triples that connect from subgraph1 to subgraph2',
                `triples_from_2_to_1`                      bigint  COMMENT 'Number of directed triples that connect from subgraph2 to subgraph1',
                `common_predicate_count`                   bigint  COMMENT 'Number of predicates found in both subgraphs',
                `common_item_count`                        bigint  COMMENT 'Number of items found in both subgraphs',
                `common_item_percent_of_subgraph1_items`   double  COMMENT 'Percent of common items w.r.t total items in subgraph1',
                `common_item_percent_of_subgraph2_items`   double  COMMENT 'Percent of common items w.r.t total items in subgraph2'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_pair_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {general_query_metrics_table} (
                `total_query_count`                 bigint  COMMENT 'Total number of queries',
                `processed_query_count`             bigint  COMMENT 'Total number of queries that were parsed/processed successfully',
                `percent_processed_query`           double  COMMENT 'Percentage of queries that were parsed successfully (w.r.t 200 and 500 queries, except monitoring queries)',
                `distinct_query_count`              bigint  COMMENT 'Number of distinct processed queries',
                `percent_query_repeated`            double  COMMENT 'Percentage of query repeated',
                `total_ua_count`                    bigint  COMMENT 'Total distinct user-agents (from UA string)',
                `total_time`                        bigint  COMMENT 'Total time in milliseconds taken to run all the processed queries',
                `status_code_query_count`           map<bigint, bigint>  COMMENT 'Number of queries per status code',
                `query_time_class_query_count`      map<string, bigint>  COMMENT 'Number of queries per query time class'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_general_query_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {general_subgraph_query_metrics_table} (
                `total_subgraph_query_count`        bigint  COMMENT 'Number of queries that access the top subgraphs',
                `ua_subgraph_dist`                  array<struct<subgraph_count:bigint, ua_count:bigint> >  COMMENT 'List of the number of user-agents accessing how many subgraphs each',
                `query_subgraph_dist`               array<struct<subgraph_count:bigint, query_count:bigint> >  COMMENT 'List of the number of queries accessing how many subgraphs at once',
                `query_time_class_subgraph_dist`    array<
                                                        struct<
                                                           subgraph_count: string,
                                                           query_time_class: map<string, bigint>
                                                        >
                                                    >  COMMENT 'Query time class distribution of queries that access 1,2,3,4,4+,n/a subgraphs. Here `n/a` means a query does not access any of the top subgraphs'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_general_subgraph_query_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {per_subgraph_query_metrics_table} (
                `subgraph`                             string  COMMENT 'URI of the subgraphs in wikidata',
                `query_count`                          bigint  COMMENT 'Number of queries accessing this subgraph',
                `query_time`                           bigint  COMMENT 'Total time of queries accessing this subgraph',
                `ua_count`                             bigint  COMMENT 'Number of distinct user agents accessing this subgraph',
                `query_type`                           bigint  COMMENT 'Distinct types of queries in this subgraph (rough estimate from operator list)',
                `percent_query_count`                  double  COMMENT 'Percent queries in this subgraph w.r.t total parsed queries',
                `percent_query_time`                   double  COMMENT 'Percent total query time in this subgraph w.r.t total parsed query time',
                `percent_ua_count`                     double  COMMENT 'Percent unique user-agents w.r.t total user-agents of parsed queries',
                `query_count_rank`                     integer COMMENT 'Rank of the subgraph in terms of number of query in descending order',
                `query_time_rank`                      integer COMMENT 'Rank of the subgraph in terms of query time in descending order',
                `avg_query_time`                       double  COMMENT 'Average time(ms) per query in this subgraph',
                `qid_count`                            bigint  COMMENT 'Number of queries that matched to this subgraph due to the subgraph Qid match',
                `item_count`                           bigint  COMMENT 'Number of queries that matched to this subgraph due to the subgraph items Qid match',
                `pred_count`                           bigint  COMMENT 'Number of queries that matched to this subgraph due to predicate match',
                `uri_count`                            bigint  COMMENT 'Number of queries that matched to this subgraph due to URI match',
                `literal_count`                        bigint  COMMENT 'Number of queries that matched to this subgraph due to literals match',
                `query_time_class_counts`              map<string, bigint>  COMMENT 'Number of queries per query time class',
                `ua_info`                              array<
                                                           struct<
                                                                ua_rank: integer,
                                                                ua_query_count: bigint,
                                                                ua_query_time: bigint,
                                                                ua_query_type: bigint,
                                                                ua_query_percent: double,
                                                                ua_query_time_percent: double,
                                                                ua_avg_query_time: double,
                                                                ua_query_type_percent: double
                                                            >
                                                        >      COMMENT 'List of top user-agents (by query count) using this subgraph and other aggregate info about its queries. The percents are w.r.t the subgraphs data',
                `ua_query_count_percentiles`           array<double>  COMMENT 'List of 0.1 to 0.9 percentile of query count per user-agent in each subgraph',
                `ua_query_count_mean`                  double  COMMENT 'Mean of query count per user-agent in each subgraph',
                `subgraph_composition`                 array<
                                                            struct<
                                                                item: boolean,
                                                                predicate: boolean,
                                                                uri: boolean,
                                                                qid: boolean,
                                                                literal: boolean,
                                                                count: bigint
                                                                >
                                                            >  COMMENT 'List of various combinations is which queries match with a subgraph and the number of such queries',
                `query_only_accessing_this_subgraph`   bigint  COMMENT 'Number of queries that access only this subgraph alone',
                `top_items`                            map<string, bigint>  COMMENT 'Top items in this subgraph that caused a query match mapped to the number of queries that matched',
                `matched_items_percentiles`            array<double>  COMMENT 'List of 0.1 to 0.9 percentile of queries matched per item in each subgraph',
                `matched_items_mean`                   double  COMMENT 'Mean of queries matched per item in this subgraph',
                `top_predicates`                       map<string, bigint>  COMMENT 'Top predicates in this subgraph that caused a query match mapped to the number of queries that matched',
                `matched_predicates_percentiles`       array<double>  COMMENT 'List of 0.1 to 0.9 percentile of queries matched per predicate in each subgraph',
                `matched_predicates_mean`              double  COMMENT 'Mean of queries matched per predicate in this subgraph',
                `top_uris`                             map<string, bigint>  COMMENT 'Top URIs in this subgraph that caused a query match mapped to the number of queries that matched',
                `matched_uris_percentiles`             array<double>  COMMENT 'List of 0.1 to 0.9 percentile of queries matched per URI in each subgraph',
                `matched_uris_mean`                    double  COMMENT 'Mean of queries matched per URI in this subgraph',
                `service_counts`                       map<string, bigint>  COMMENT 'List of services and the number of queries that use the service',
                `path_counts`                          map<string, bigint>  COMMENT 'List of top paths used and the number of queries that use the path'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_per_subgraph_query_metrics_location}'
            ;

            CREATE TABLE IF NOT EXISTS {subgraph_pair_query_metrics_table} (
                `subgraph1`         string  COMMENT 'First subgraph of the subgraph pair',
                `subgraph2`         string  COMMENT 'Second subgraph of the subgraph pair',
                `query_count`       bigint  COMMENT 'Number of queries that access subgraph1 and subgraph2'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_pair_query_metrics_location}'
            ;
            """  # noqa
    ) >> complete

with DAG(
        'subgraph_metrics_weekly',
        default_args=default_args,
        # all input tables are available every friday (snapshot dated for the previous monday)
        # schedule this dag for friday and give previous monday's date to keep the dates in sync
        # wikidata dumps come in by ~6 UTC and other tables start populating at 7 UTC
        schedule='0 9 * * 5',
        # As a weekly job there should never really be more than
        # one running at a time.
        max_active_runs=1,
        catchup=True,
        user_defined_macros={
            'p': pendulum,
        }
) as subgraph_metrics_dag:
    last_monday = "{{ execution_date.previous(day_of_week=p.MONDAY).format('YYYYMMDD') }}"
    wikidata_table_and_partition: str = f'{wikidata_dumps_table}/date={last_monday}/wiki={wiki}'
    all_subgraphs_table_and_partition: str = f'{all_subgraphs_table}/snapshot={last_monday}/wiki={wiki}'
    top_subgraph_items_table_and_partition: str = f'{top_subgraph_items_table}/snapshot={last_monday}/wiki={wiki}'
    top_subgraph_triples_table_and_partition: str = f'{top_subgraph_triples_table}/snapshot={last_monday}/wiki={wiki}'
    general_subgraph_metrics_table: str = f'{general_subgraph_metrics_table}/snapshot={last_monday}/wiki={wiki}'

    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        sla=timedelta(days=2),
        retries=4,
        partition_names=[wikidata_table_and_partition,
                         all_subgraphs_table_and_partition,
                         top_subgraph_items_table_and_partition,
                         top_subgraph_triples_table_and_partition],
    )

    extract_general_subgraph_metrics = SparkSubmitOperator(
        task_id='extract_general_subgraph_metrics',
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.metrics.SubgraphMetricsLauncher",
        max_executors=64,
        executor_cores=4,
        executor_memory="8g",
        driver_memory="8g",
        conf=public_dataset_sparkconf,
        application_args=[
            "general-subgraph-metrics",
            "--wikidata-triples-table", wikidata_table_and_partition,
            "--all-subgraphs-table", all_subgraphs_table_and_partition,
            "--top-subgraph-items-table", top_subgraph_items_table_and_partition,
            "--top-subgraph-triples-table", top_subgraph_triples_table_and_partition,
            "--general-subgraph-metrics-table", general_subgraph_metrics_table,
            "--min-items", min_items
        ]
    )

    extract_detailed_subgraph_metrics = SparkSubmitOperator(
        task_id='extract_detailed_subgraph_metrics',
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.metrics.SubgraphMetricsLauncher",
        max_executors=64,
        executor_cores=4,
        executor_memory="8g",
        driver_memory="8g",
        conf=public_dataset_sparkconf,
        application_args=[
            "detailed-subgraph-metrics",
            "--all-subgraphs-table", all_subgraphs_table_and_partition,
            "--top-subgraph-items-table", top_subgraph_items_table_and_partition,
            "--top-subgraph-triples-table", top_subgraph_triples_table_and_partition,
            "--general-subgraph-metrics-table", general_subgraph_metrics_table,
            "--min-items", min_items,
            "--per-subgraph-metrics-table",
            f'{per_subgraph_metrics_table}/snapshot={last_monday}/wiki={wiki}',
            "--subgraph-pair-metrics-table",
            f'{subgraph_pair_metrics_table}/snapshot={last_monday}/wiki={wiki}',
        ]
    )
    complete = DummyOperator(task_id='complete')

    (wait_for_data
     >> extract_general_subgraph_metrics
     >> extract_detailed_subgraph_metrics  # depends on the output table from previous task
     >> complete)

with DAG(
        'subgraph_query_metrics_daily',
        default_args=default_args,
        schedule='@daily',
        max_active_runs=4,
        catchup=True,
) as subgraph_query_metrics_dag:
    subgraph_query_table_and_partition: str = f'{subgraph_query_mapping_table}/{YMD_PARTITION}/wiki={wiki}'
    subgraph_qitem_match_table_and_partition: str = f'{subgraph_qitem_match_table}/{YMD_PARTITION}/wiki={wiki}'
    subgraph_predicate_match_table_and_partition: str = f'{subgraph_predicate_match_table}/{YMD_PARTITION}/wiki={wiki}'
    subgraph_uri_match_table_and_partition: str = f'{subgraph_uri_match_table}/{YMD_PARTITION}/wiki={wiki}'

    wait_for_event_sparql_queries = RangeHivePartitionSensor(
        task_id='wait_for_event_sparql_queries',
        table_name=event_sparql_query_table,
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=1) }}',
        granularity='@hourly',
        pre_partitions=[[f"datacenter={dc}"] for dc in eventgate_datacenters],
    )

    wait_for_processed_sparql_queries = RangeHivePartitionSensor(
        task_id='wait_for_processed_sparql_queries',
        table_name=processed_query_table,
        from_timestamp='{{ execution_date }}',
        to_timestamp='{{ execution_date.add(days=1) }}',
        granularity='@hourly',
        pre_partitions=[f"wiki={wiki}"],
    )

    wait_for_subgraph_query_table = NamedHivePartitionSensor(
        task_id='wait_for_subgraph_query_table',
        mode='reschedule',
        sla=timedelta(days=4),
        retries=4,
        partition_names=[subgraph_query_table_and_partition]
    )

    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        sla=timedelta(days=4),
        retries=4,
        partition_names=[
            subgraph_qitem_match_table_and_partition,
            subgraph_predicate_match_table_and_partition,
            subgraph_uri_match_table_and_partition
        ]
    )

    extract_subgraph_query_metrics = SparkSubmitOperator(
        task_id='extract_subgraph_query_metrics',
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.metrics.SubgraphMetricsLauncher",
        max_executors=64,
        executor_cores=4,
        executor_memory="8g",
        driver_memory="8g",
        application_args=[
            "query-metrics",
            "--event-query-table",
            f'{event_sparql_query_table}/{YMD_PARTITION}',
            "--processed-query-table",
            f'{processed_query_table}/{YMD_PARTITION}/wiki={wiki}',
            "--matched-Qitems-table", subgraph_qitem_match_table_and_partition,
            "--matched-predicates-table", subgraph_predicate_match_table_and_partition,
            "--matched-uris-table", subgraph_uri_match_table_and_partition,
            "--subgraph-query-table", subgraph_query_table_and_partition,
            "--top-n", top_n,
            "--general-query-metrics-table",
            f'{general_query_metrics_table}/{YMD_PARTITION}/wiki={wiki}',
            "--general-subgraph-query-metrics-table",
            f'{general_subgraph_query_metrics_table}/{YMD_PARTITION}/wiki={wiki}',
            "--per-subgraph-query-metrics-table",
            f'{per_subgraph_query_metrics_table}/{YMD_PARTITION}/wiki={wiki}',
        ]
    )

    extract_subgraph_pair_query_metrics = SparkSubmitOperator(
        task_id='extract_subgraph_pair_query_metrics',
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.metrics.SubgraphMetricsLauncher",
        max_executors=64,
        executor_cores=4,
        executor_memory="8g",
        driver_memory="8g",
        application_args=[
            "subgraph-pair-query-metrics",
            "--subgraph-query-table", subgraph_query_table_and_partition,
            "--subgraph-pair-query-metrics-table",
            f'{subgraph_pair_query_metrics_table}/{YMD_PARTITION}/wiki={wiki}',
        ]
    )

    complete = DummyOperator(task_id='complete')

    ([
         wait_for_event_sparql_queries,
         wait_for_processed_sparql_queries,
         wait_for_subgraph_query_table,
         wait_for_data
     ]
     >> extract_subgraph_query_metrics
     >> complete
     )

    wait_for_subgraph_query_table >> extract_subgraph_pair_query_metrics >> complete
