from datetime import datetime, timedelta
from typing import Union

from airflow import DAG, AirflowException
from airflow.decorators import task
from airflow.operators.empty import EmptyOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from airflow.sensors.external_task import ExternalTaskSensor

from search.config.dag_config import get_default_args
from search.shared.transfer_to_es import convert_and_upload
from wmf_airflow_common.config.variable_properties import VariableProperties

var_props = VariableProperties('transfer_to_es_config')

# Default kwargs for all Operators
default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    # This DAG updates the state of the search engine. The sequence of updates is
    # important for the final state to be correct. As such the previous run must
    # always complete before the next.
    'depends_on_past': True,
    'start_date': datetime(2021, 1, 24),
})

# Useful to set false when testing to run the full pipeline without shipping
# anything to prod.
ship_upload_events = var_props.get_parsed('ship_upload_events', bool, True)

image_suggestions_manual_run_arg_name = 'image_suggestions_input_partition_spec'

sensor_timeout_kwargs = dict(
    timeout=timedelta(hours=3).total_seconds(),
    retries=4,
    email_on_retry=True)


def maybe_event_stream(event_stream: Union[bool, str] = True) -> Union[bool, str]:
    if ship_upload_events:
        return event_stream
    else:
        return False


with DAG(
    'transfer_to_es_weekly',
    default_args=dict(
        default_args,
        start_date=var_props.get_datetime('transfer_weekly_start_date', datetime(2023, 3, 12))
    ),
    # Once a week at midnight on Sunday morning
    schedule_interval='0 0 * * 0',
    max_active_runs=1,
    catchup=True,
) as weekly_dag:
    # Wait for popularity to compute
    sensors = [
        ExternalTaskSensor(
            task_id='wait_for_popularity_score',
            mode='reschedule',
            external_dag_id='popularity_score_weekly',
            external_task_id='complete',
            **sensor_timeout_kwargs
        ),
        # prefer SLA notification for the wait_for_incoming_links sensor instead of
        # notifications on timeouts&retries
        ExternalTaskSensor(
            task_id='wait_for_incoming_links',
            mode='reschedule',
            external_dag_id='incoming_links_weekly',
            external_task_id='complete',
            sla=timedelta(hours=30),  # 28hours for cirrus imports + 2 hours for inc_links
        ),
    ]

    convert, probe, upload = convert_and_upload('weekly', 'freq=weekly', maybe_event_stream())
    sensors >> convert >> probe >> upload >> EmptyOperator(task_id='complete')


with DAG(
    'image_suggestions_manual',
    default_args=default_args,
    schedule_interval=None,
) as imagerec_dag:
    @task(task_id="check_image_suggestions_manual_dag_run_arg")
    def check_dag_run_arg(ds=None, **kwargs):
        if 'dag_run' not in kwargs and image_suggestions_manual_run_arg_name not in kwargs['dag_run']:
            raise AirflowException(f"Please set the config key "
                                   f"{image_suggestions_manual_run_arg_name} "
                                   f"when triggering this DAG")

    # Run this with:
    # airflow dags trigger 'image_suggestions_manual'
    #   -r 'image_suggestions_fixup_TXXXX'
    #   -e 2023-08-21T00:00:00+00:00
    #   --conf '{"image_suggestion_input_partition_spec":"analytics_platform_eng.image_suggestions_search_index_full/snapshot=2023-08-21"}'
    check_arg = check_dag_run_arg()
    convert, probe, upload = convert_and_upload(
        'image_suggestion_manual',
        'freq=manual/image_suggestions',
        maybe_event_stream(),
        '{{ dag_run.conf.%s }}' % image_suggestions_manual_run_arg_name
    )
    check_arg >> convert >> probe >> upload >> EmptyOperator(task_id='complete')


with DAG(
        'image_suggestions_fixup_T320656',
        default_args=default_args,
        schedule_interval=None,
) as imagerec_fixup_T320656_dag:
    # See https://phabricator.wikimedia.org/T320656
    convert, probe, upload = convert_and_upload(
        'image_suggestions_fixup_T320656',
        'freq=manual/image_suggestions_fixup_T320656',
        maybe_event_stream())
    convert >> probe >> upload >> EmptyOperator(task_id='complete')


with DAG(
    'image_suggestions_weekly',
    default_args=dict(
        default_args,
        depends_on_past=True,
        email=['discovery-alerts@lists.wikimedia.org', 'sd-alerts@lists.wikimedia.org'],
        start_date=var_props.get_datetime('image_suggestions_start_date', datetime(2023, 3, 20)),
    ),
    schedule_interval='0 0 * * 1',
    catchup=True,
) as imagerec_dag_weekly:

    table_name = 'analytics_platform_eng.image_suggestions_search_index_delta'
    # timeout should 7 days by default
    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        sla=timedelta(days=6),
        partition_names=[table_name + '/snapshot={{ds}}'],
    )
    convert, probe, upload = convert_and_upload(
        'image_suggestion_weekly',
        'freq=weekly/image_suggestions',
        maybe_event_stream())
    wait_for_data >> convert >> probe >> upload >> EmptyOperator(task_id='complete')
