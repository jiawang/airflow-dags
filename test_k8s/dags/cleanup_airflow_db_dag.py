import logging
import subprocess
import textwrap
from datetime import date, datetime, timedelta

from airflow import DAG
from airflow.models import Variable
from airflow.operators.python import PythonOperator

logger = logging.getLogger(__name__)


def run_cleanup_airflow_db_command():
    """Generate the bash command used to cleanup the airflow database.

    This task depends on the following Airflow variables:
    - db_cleanup_tables: a comma-separated list of tables from which to regularly clean up old vealues
    - db_cleanup_retention_days: the number of days of data to keep in the tables defined in `db_cleanup_tables`

    If these variables aren't set, the task will abort.

    """
    db_cleanup_tables = ",".join([table.strip() for table in Variable.get("db_cleanup_tables", "").split(",")])
    db_cleanup_retention_days = int(Variable.get("db_cleanup_retention_days", 0))

    if not all((db_cleanup_tables, db_cleanup_retention_days)):
        logging.info("The db_cleanup_tables and/or db_cleanup_retention_days variables are not defined. Skipping.")
        return

    logging.info(f"{db_cleanup_retention_days}d of data will be kept in tables {db_cleanup_tables}")
    clean_before_timestamp = str(date.today() - timedelta(days=db_cleanup_retention_days))
    cmd = [
        "airflow",
        "db",
        "clean",
        "--tables",
        db_cleanup_tables,
        "--clean-before-timestamp",
        clean_before_timestamp,
        "--yes",
    ]
    logging.info(" ".join(cmd))
    subprocess.run(cmd)


with DAG(
    "cleanup_airflow_db",
    default_args={
        "owner": "airflow",
        "depends_on_past": False,
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that cleans up old objects from the airflow database",
    start_date=datetime(2024, 9, 26),
    schedule="@daily",
    catchup=False,
    tags=["airflow_maintenance"],
) as dag:
    task = PythonOperator(
        task_id="run_cleanup_airflow_db_command",
        python_callable=run_cleanup_airflow_db_command,
        dag=dag,
        doc_md=textwrap.dedent(str(run_cleanup_airflow_db_command.__doc__)),
    )
    task
