import logging
from datetime import datetime

from airflow import DAG
from airflow.exceptions import AirflowException
from airflow.operators.python_operator import PythonOperator

from wmf_airflow_common.clients.s3 import get_s3_client

logger = logging.getLogger(__name__)


def list_buckets():
    """Make sure that S3 can be connected to"""
    logging.info("Attempting to connect to S3 and perform a basic list bucket action")
    try:
        s3_client = get_s3_client("s3_dpe")
        s3_client.list_buckets()
    except Exception as exc:
        logging.exception("S3 connection/basic action failed", exc_info=exc)
        raise AirflowException(str(exc))
    else:
        logging.info("S3 server connection and basic list bucket action were successful")


with DAG(
    "test_s3_connection",
    default_args={
        "owner": "analytics",
        "depends_on_past": False,
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 1,
    },
    description="A DAG that checks whether S3 can be connected to",
    start_date=datetime(2024, 10, 14),
    schedule="@hourly",
    catchup=False,
    tags=["monitoring"],
) as dag:
    task = PythonOperator(
        task_id="test_s3_connection",
        python_callable=list_buckets,
        dag=dag,
    )
    task
