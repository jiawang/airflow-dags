from datetime import datetime

from airflow import DAG
from airflow.operators.python_operator import PythonOperator


def crash_on_purpose():
    raise ValueError("This DAG was meant to fail, as a way to test that emails were sent correctly")


with DAG(
    "test_email_notification",
    default_args={
        "owner": "analytics",
        "depends_on_past": False,
        "email": "data-platform-alerts@wikimedia.org",
        "email_on_failure": True,
        "email_on_retry": False,
        "retries": 0,
    },
    description="A DAG meant to fail, to test email alerting",
    schedule=None,
    start_date=datetime(2024, 10, 8),
) as dag:
    task = PythonOperator(
        task_id="crash_on_purpose",
        python_callable=crash_on_purpose,
        dag=dag,
    )
    task
