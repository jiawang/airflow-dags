import os

import pytest
from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "canary_events", "canary_events_dag.py"]


def test_canary_events_hourly_dag_loaded(dagbag, mocker, compare_with_fixture):
    # Basic assertions about the DAG
    assert dagbag.import_errors == {}
    dag_id = "canary_events"
    dag = dagbag.get_dag(dag_id=dag_id)
    assert dag is not None
    assert len(dag.tasks) == 3

    # Mock the dataset configurations input
    stream_configs_fixture = os.path.join(os.path.dirname(__file__), "stream_configs_example.json")
    with open(stream_configs_fixture, "r") as file:
        mock_stream_configs_call(file.read(), mocker)

    # Create a dagrun
    data_interval = (dag.start_date, dag.start_date.add(minutes=33))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    # Test data interval
    execution_dates_iter = dag.iter_dagrun_infos_between(None, dag.start_date.add(years=1))
    data_interval = next(execution_dates_iter).data_interval
    assert data_interval.start.strftime("%Y-%m-%dT%H:%M:%S") == "2024-05-21T12:02:00"
    assert data_interval.end.strftime("%Y-%m-%dT%H:%M:%S") == "2024-05-21T12:35:00"

    # Test the task in charge of getting the configuration
    fetch_stream_configs_ti = create_and_run_task("fetch_streams_configurations", dagrun)
    result = fetch_stream_configs_ti.xcom_pull(task_ids=fetch_stream_configs_ti.task.task_id)
    assert len(result) == 2

    # Test the task in charge of preparing the spark job
    prepare_scripts_commands_ti = create_and_run_task("prepare_scripts_commands", dagrun, map_index=0)
    result = prepare_scripts_commands_ti.xcom_pull(task_ids=prepare_scripts_commands_ti.task.task_id, map_indexes=0)
    assert len(result) == 2
    assert (
        result[0] == "spark3-submit --master local[1] "
        "--class org.wikimedia.analytics.refinery.job.ProduceCanaryEvents "
        "hdfs:///wmf/cache/artifacts/airflow/analytics/refinery-job-0.2.44-shaded.jar "
        "--schema_base_uris=https://schema.discovery.wmnet/repositories/primary/jsonschema,"
        "https://schema.discovery.wmnet/repositories/secondary/jsonschema "
        "--event_stream_config_uri=https://meta.wikimedia.org/w/api.php "
        "--event_service_config_uri=file://$(pwd)/event_intake_service_urls.json "
        "--stream_name eventlogging_CentralNoticeBannerHistory "
        "--timestamp 2024-05-21T12:35:00Z "
        "--dry_run false"
    )
    assert prepare_scripts_commands_ti.rendered_map_index == "eventlogging_CentralNoticeBannerHistory"


def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_stream_configs_call(stream_configs_json, mocker):
    mocker.patch(
        "analytics.dags.canary_events.canary_events_dag.fsspec.open",
        mocker.mock_open(read_data=stream_configs_json),
    )
