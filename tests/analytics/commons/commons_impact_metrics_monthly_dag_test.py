import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "commons", "commons_impact_metrics_monthly_dag.py"]


def test_commons_impact_metrics_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="commons_impact_metrics_monthly")
    assert dag is not None
    assert len(dag.tasks) == 16
