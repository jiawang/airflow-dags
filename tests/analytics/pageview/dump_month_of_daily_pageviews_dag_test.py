import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "pageview", "dump_month_of_daily_pageviews_dag.py"]


def test_dump_month_of_daily_pageviews_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="dump_month_of_daily_pageviews")
    assert dag is not None
    assert len(dag.tasks) == 5
