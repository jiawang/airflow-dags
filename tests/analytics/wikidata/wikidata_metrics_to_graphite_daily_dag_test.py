import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "wikidata", "wikidata_metrics_to_graphite_daily_dag.py"]


def test_wikidata_coeditors_metrics_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wikidata_metrics_to_graphite_daily")
    assert dag is not None
    assert len(dag.tasks) == 6
    etl1 = dag.get_task("generate_and_send_articleplaceholder_metrics")
    etl2 = dag.get_task("generate_and_send_reliability_metrics")
    etl3 = dag.get_task("generate_and_send_specialentity_data_metrics")
    etl4 = dag.get_task("generate_and_send_special_entity_schema_text_metrics")
    etl5 = dag.get_task("generate_and_send_entity_schema_namespace_metrics")
    application_args1 = etl1.application_args
    application_args2 = etl2.application_args
    application_args3 = etl3.application_args
    application_args4 = etl4.application_args
    application_args5 = etl5.application_args
    assert len(application_args1) == 18
    assert len(application_args2) == 18
    assert len(application_args3) == 18
    assert len(application_args4) == 18
    assert len(application_args5) == 18
