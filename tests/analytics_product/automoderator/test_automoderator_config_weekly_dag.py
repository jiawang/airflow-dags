import pytest

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "automoderator", "automoderator_config_weekly_dag.py"]

def test_automoderator_config_weekly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="automoderator_config_weekly")
    assert dag is not None
    assert len(dag.tasks) == 2+2 # start, end, SparkSubmitOperator, HDFSArchiveOperator

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"
