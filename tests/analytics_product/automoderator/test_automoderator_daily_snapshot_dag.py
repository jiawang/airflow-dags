import os
import pytest
import csv

from airflow.models import DagRun, TaskInstance
from airflow.utils.session import create_session
from airflow.utils.state import DagRunState
from airflow.utils.types import DagRunType

WIKIDBS_XCOM_KEY = "wiki_dbs"

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "automoderator", "automoderator_daily_snapshot_dag.py"]

def test_automoderator_snapshot_daily_loaded(dagbag, mocker, compare_with_fixture):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="automoderator_monitoring_snapshot_daily")
    assert dag is not None
    assert dag.dag_id == 'automoderator_monitoring_snapshot_daily'
    # start, end, fetch db list, fetch snapshot, publish snapshot, purge snapshot
    assert len(dag.tasks) == 2+3+1

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"

    base_path = os.path.dirname(__file__)
    mock_config_path = os.path.join(base_path, "automoderator_config_fixture.tsv")

    with open(mock_config_path, "r") as file:
        automod_config = file.read()

    mock_configured_dbs_call(mocker, automod_config)

    data_interval = (dag.start_date, dag.start_date.add(days=1))
    dagrun = dag.create_dagrun(
        state=DagRunState.RUNNING,
        execution_date=dag.start_date,
        run_id=DagRun.generate_run_id(DagRunType.MANUAL, dag.start_date),
        start_date=dag.start_date,
        data_interval=data_interval,
    )

    fetch_dblist_task_ti = create_and_run_task("fetch_configured_dbs", dagrun)
    result = fetch_dblist_task_ti.xcom_pull(task_ids=fetch_dblist_task_ti.task.task_id)
    assert sorted(result) == sorted(["testwiki", "trwiki", "idwiki", "ukwiki"])

    # test fetch snapshots
    fetch_daily_snapshot = dag.get_task("fetch_snapshots.fetch_automoderator_daily_snapshot")
    fetch_daily_snapshot_ti = TaskInstance(fetch_daily_snapshot, run_id=dagrun.run_id, map_index=0)
    fetch_daily_snapshot_ti.dag_run = dagrun
    context = fetch_daily_snapshot_ti.get_template_context()
    fetch_daily_snapshot_ti.render_templates(context=context)
    skein_hook = fetch_daily_snapshot._get_hook()._skein_hook
    kwargs = {"serde": "str", "content": skein_hook._application_spec.to_yaml()}
    fixture_id = "analytics_product_dags_automoderator_automoderator_daily_snapshot_dag.py-automoderator_monitoring_snapshot_daily-fetch_automoderator_daily_snapshot"
    compare_with_fixture(group="spark_skein_specs", fixture_id=fixture_id, **kwargs)

# source: https://w.wiki/BMyt
def create_and_run_task(task_id: str, dag_run: DagRun, map_index: int | None = None) -> TaskInstance:
    task = dag_run.dag.get_task(task_id)
    if map_index is not None:
        task_instance = TaskInstance(task, run_id=dag_run.run_id, map_index=map_index)
    else:
        # If map_index kwargs is provided, the task is considered as a mapped task.
        task_instance = TaskInstance(task, run_id=dag_run.run_id)
    task_instance.dag_run = dag_run
    if map_index is not None:
        # The mapped operator are not expended when the dagrun is created, unlike the simple tasks.
        with create_session() as session:
            session.add(task_instance)
            session.commit()
    task_instance.run(
        verbose=True,
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )
    return task_instance


def mock_configured_dbs_call(mocker, content_list):
    mock_one = mocker.Mock()
    mock_one.status_code = 200
    mock_one.text = content_list

    mock_get = mocker.patch("analytics_product.dags.automoderator.automoderator_daily_snapshot_dag.requests.get")
    mock_get.side_effect = [mock_one]



