import pytest

@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics_product", "dags", "automoderator", "automoderator_potential_vandalism_reverted_monthly_dag.py"]

def test_automoderator_snapshot_monthly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="automoderator_potential_vandalism_reverted_monthly")
    assert dag is not None
    # mwh_sensor, generate, write tsv, publish tsv, purge, complete
    assert len(dag.tasks) == 6

    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args["metastore_conn_id"] == "analytics-hive"
