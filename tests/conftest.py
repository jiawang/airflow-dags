"""
Set of fixtures and hooks reused across all DAG tests
"""

import getpass
import json
import os
from contextlib import contextmanager
from copy import deepcopy
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, Sequence, TextIO
from unittest import mock

import airflow.providers.apache.hive.macros.hive as hive_macros
import pytest
from airflow.models import BaseOperator, DagBag, DagRun, MappedOperator, TaskInstance
from airflow.timetables.base import DataInterval
from airflow.utils.task_group import MappedTaskGroup, TaskGroup

from search.shared.auto_size_spark import AutoSizeSparkSubmitOperator
from search.shared.hive_table_path import HiveTablePath

test_base_path = Path(__file__).parent
repo_base_path = test_base_path.parent


def pytest_addoption(parser):
    parser.addoption("--runslow", action="store_true", default=False, help="run slow tests")


def pytest_collection_modifyitems(config, items):
    if config.getoption("--runslow"):
        # --runslow given in cli: do not skip slow tests
        return
    skip_slow = pytest.mark.skip(reason="need --runslow option to run")
    for item in items:
        if "slow" in item.keywords:
            item.add_marker(skip_slow)


def setup_airflow(tmp_path_factory):
    """
    Sets up an airflow SQLlite database and airflow home
    used by the entire test session.
    """
    from airflow.utils import db

    airflow_environ = {
        "AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS": "False",
        "AIRFLOW__CORE__LOAD_EXAMPLES": "False",
        "AIRFLOW__CORE__UNIT_TEST_MODE": "True",
        "AIRFLOW__CORE__HOSTNAME_CALLABLE": "airflow.utils.net.getfqdn",
        "AIRFLOW_HOME": os.path.join(tmp_path_factory.mktemp("airflow_home")),
        "AIRFLOW_INSTANCE_NAME": "wmf_pytest",
        "REBUILD_FIXTURES": os.environ.get("REBUILD_FIXTURES", "no"),
        # Limit the number of duplicate fixtures generated
        # TODO: Define a generic way for instances to augment test environment?
        "AIRFLOW_VAR_MJOLNIR_WEEKLY_CONFIG": json.dumps(
            {
                "wikis": ["arwiki", "enwiki"],
            }
        ),
        "AIRFLOW_VAR_ORES_PREDICTIONS_CONF": json.dumps(
            {
                "articletopic_wikis": ["enwiki"],
                "drafttopic_namespaces": [0],
            }
        ),
    }
    # Perhaps a little awkward, but it's annoying to hold a context manager
    # open between session start/finish. We don't really need the environment
    # cleanup, so simply start the mock and let it be.
    mock.patch.dict(os.environ, airflow_environ, clear=True).start()
    db.resetdb()


def pytest_configure(config):
    config.addinivalue_line(
        "markers",
        "for_each_task(kind=None, instance_name=None): " "Parametrizes test based on existing airflow tasks)",
    )
    config.addinivalue_line("markers", "slow: mark test as slow to run")


def pytest_sessionstart(session):
    setup_airflow(session.config._tmp_path_factory)
    # Collect the set of valid tasks, to be used to parametrize tests
    session.config._all_tasks = list(collect_all_tasks())


def pytest_generate_tests(metafunc) -> None:
    """Generates new tests based on a for_each_task marker"""
    for marker in metafunc.definition.iter_markers(name="for_each_task"):
        assert marker.args == tuple(), "for_each_task marker only accepts kwargs"
        kwargs = marker.kwargs
        kwargs["all_tasks"] = metafunc.config._all_tasks
        if "instance_name" not in kwargs:
            # auto-scope for_each_task to the per-instance test paths
            kwargs = dict(kwargs)
            kwargs["instance_name"] = instance_name_for_path(Path(metafunc.module.__file__))
        metafunc.parametrize(("dag_path", "dag_id", "task_id"), list(for_each_task(**kwargs)), _param_mark=marker)


def instance_name_for_path(path: Path) -> str:
    try:
        # test file path
        parts = path.relative_to(test_base_path).parts
    except ValueError:
        # dag file path
        parts = path.relative_to(repo_base_path).parts
    return parts[0]


@pytest.fixture(name="instance_name")
def instance_name_fixture(request):
    if "dag_path" in request.fixturenames:
        # Prefer basing instance_name off the dag_path, if available.
        # This supports getting the correct instance_name from global
        # test cases.
        path = Path(repo_base_path, *request.getfixturevalue("dag_path"))
    else:
        # Otherwise work from the test file name
        path = Path(request.fspath)
    return instance_name_for_path(path)


@contextmanager
def load_dagbag(dag_path, instance_name):
    # Note that this doesn't always work as expected. If the value was read at
    # module import time, as is common in {instance_name}/config/dag_config.py,
    # then that module configures with whatever it was at the time of import
    # (called during pytest sessionstart, inside collect_all_tasks).
    instance_environ = {"AIRFLOW_INSTANCE_NAME": f"wmf_{instance_name}"}
    with (
        mock.patch.dict(os.environ, instance_environ, clear=False),
        mock.patch.object(getpass, "getuser", return_value="pytest_user"),
    ):
        dag_bag = DagBag(None, include_examples=False, read_dags_from_db=False)
        dag_file = os.path.join(os.path.abspath(os.getcwd()), *dag_path)
        dag_bag.process_file(dag_file)
        yield dag_bag


@pytest.fixture(name="dagbag")
def dagbag_fixture(dag_path, instance_name):
    """
    Loads a single DAG from its path, passed as parameter

    :param dag_path:
        The path of the DAG as a list of strings (one string per folder/ file)
    """
    with load_dagbag(dag_path, instance_name) as dag_bag:
        yield dag_bag


@pytest.fixture(name="task")
def task_fixture(dagbag, dag_id, task_id):
    return dagbag.get_dag(dag_id).get_task(task_id)


@dataclass
class FixtureSerDe:
    encode: Callable[[TextIO, Any], None]
    decode: Callable[[TextIO], Any]
    roundtrip: Callable[[Any], Any]


@pytest.fixture(name="fixture_dir")
def fixture_dir_fixture(instance_name):
    return Path(test_base_path, instance_name, "fixtures")


@pytest.fixture
def compare_with_fixture(fixture_dir):
    """Returns a function that compares against a fixture

    :param fixture_dir
        The path to write fixtures to.
    """

    def compare(group, fixture_id, content, serde="json"):
        path = Path(fixture_dir, group, fixture_id + ".expected")
        serde = compare_with_fixture.serde[serde]

        if path.exists():
            with path.open("r") as f:
                expect = serde.decode(f)
            # Some encoders, like json, convert tuple->list, so we need
            # a roundtrip to normalize
            assert serde.roundtrip(content) == expect
        elif os.environ.get("REBUILD_FIXTURES") == "yes":
            if not path.parent.exists():
                path.parent.mkdir(parents=True)
            with path.open("w") as f:
                serde.encode(f, content)
        else:
            raise Exception(f"No fixture [{path}] and REBUILD_FIXTURES != yes")

    return compare


def normalize_paths(val: str) -> str:
    # Prevent paths to the repository from being written to fixtures
    return val.replace(str(repo_base_path), "/pytest/path/to")


def to_json(f: TextIO, val: Any) -> None:
    f.write(normalize_paths(json.dumps(val, indent=4, sort_keys=True)))


def to_string(f: TextIO, val: Any) -> None:
    f.write(normalize_paths(val))


compare_with_fixture.serde = {
    "json": FixtureSerDe(
        encode=to_json,
        decode=json.load,
        roundtrip=lambda val: json.loads(normalize_paths(json.dumps(val))),
    ),
    "str": FixtureSerDe(
        encode=to_string,
        decode=lambda f: f.read(),
        roundtrip=lambda val: normalize_paths(val),
    ),
}


@pytest.fixture(name="render_task")
def render_task_fixture(request, fixture_dir, instance_name, mocker):
    """Returns a function that will render an airflow task"""

    def fn(task):
        # This will change the task, take a copy
        task = deepcopy(task)
        # Prevent this from talking to hive
        mocker.patch.object(HiveTablePath, "__call__").side_effect = lambda t: (
            t if t.startswith("hdfs://") else f"/path/to/{t}"
        )
        max_partition = mocker.patch.object(hive_macros, "max_partition", spec=["__call__", "__name__"])
        max_partition.return_value = "pytest"
        max_partition.__name__ = "max_partition"

        # get config for the dag run, useful for features that accepts a conf when triggering the dag
        # TODO: get the dag filename into this filename to avoid collisions...
        dag_run_conf_file = f"{fixture_dir}/dag_run_conf/{task.dag_id}.json"
        dag_run_conf = {}
        if os.path.exists(dag_run_conf_file):
            with open(dag_run_conf_file) as conf:
                dag_run_conf = json.load(conf)
        # Some dags have expectations, such as it always runs on sunday or that
        # it should read a previous runs data. Some date manipulation can
        # depend on this. To ensure changes to date manipulation are properly
        # evaluated for consistency render the task against the second run date
        # of the task, and set the prev success date to the first run.
        if task.dag.schedule_interval is None:
            # manually invoked tasks
            data_interval = DataInterval.exact(task.start_date)
            prev_data_interval = None
        else:
            execution_dates_iter = task.dag.iter_dagrun_infos_between(None, task.start_date.add(years=1))
            prev_data_interval = next(execution_dates_iter).data_interval
            try:
                data_interval = next(execution_dates_iter).data_interval
            except StopIteration:
                # @once tasks don't have a second interval
                data_interval = prev_data_interval
                prev_data_interval = None

        prev_dagrun = (
            None
            if prev_data_interval is None
            else DagRun(
                dag_id=task.dag_id,
                run_id="pytest_render_task_prev_dagrun",
                data_interval=prev_data_interval,
            )
        )
        dagrun = DagRun(dag_id=task.dag_id, run_id="pytest_render_task", data_interval=data_interval, conf=dag_run_conf)
        ti = TaskInstance(task, run_id=dagrun.run_id)
        mocker.patch.object(ti, "get_dagrun", return_value=dagrun)
        mocker.patch.object(ti, "get_previous_dagrun", return_value=prev_dagrun)
        mocker.patch.object(TaskInstance, "execution_date", mock.PropertyMock(return_value=data_interval.end))
        context = ti.get_template_context()
        context["run_id"] = dagrun.run_id
        ti.render_templates(context=context)

        # Perhaps a bit misplaced, but not clear where else to integrate this.
        if isinstance(task, AutoSizeSparkSubmitOperator):
            if task._autosize_for_matrix:
                task._autosize_for_matrix.metadata_uri = (
                    f"file://{fixture_dir}/autosize_metadata/{task.dag_id}-{task.task_id}.json"
                )
            task._get_auto_sizer().apply(task)
        return task

    return fn


@dataclass
class TaskMeta:
    instance_name: str
    dag_path: Sequence[str]
    dag_id: str
    task_type: type
    task_id: str

    @property
    def dag_path_name(self) -> str:
        return "_".join(self.dag_path)

    def issubclass(self, kind: type) -> bool:
        return issubclass(self.task_type, kind)


def collect_all_tasks():
    for dag_path in repo_base_path.glob("*/dags/**/*.py"):
        instance_name = instance_name_for_path(dag_path)
        dag_path_parts = list(dag_path.relative_to(repo_base_path).parts)
        with load_dagbag(dag_path_parts, instance_name) as dag_bag:
            for dag_id in dag_bag.dag_ids:
                dag = dag_bag.get_dag(dag_id)
                for task in dag.tasks:
                    path = dag_path.relative_to(repo_base_path).parts
                    if not is_mapped(task):
                        yield TaskMeta(instance_name, path, dag_id, type(task), task.task_id)


@pytest.fixture(name="instance_tasks")
def instance_tasks_fixture(request, instance_name):
    return [meta for meta in request.config._all_tasks if meta.instance_name == instance_name]


def for_each_task(all_tasks, kind=object, instance_name=None):
    for meta in all_tasks:
        if instance_name and meta.instance_name != instance_name:
            continue
        if not meta.issubclass(kind):
            continue
        yield pytest.param(
            meta.dag_path,
            meta.dag_id,
            meta.task_id,
            id=f"{meta.dag_path_name}-{meta.dag_id}-{meta.task_id}",
        )


def is_mapped(task: BaseOperator | TaskGroup) -> bool:
    return issubclass(task.get_closest_mapped_task_group().__class__, MappedTaskGroup) or issubclass(
        task.__class__, MappedOperator
    )
