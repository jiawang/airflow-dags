#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'check_bad_parsing_dag.py']


def test_check_bad_parsing_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="check_bad_parsing")
    assert dag is not None
    assert len(dag.tasks) == 3
