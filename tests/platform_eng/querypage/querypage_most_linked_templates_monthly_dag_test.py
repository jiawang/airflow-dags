import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["platform_eng", "dags", "querypage", "querypage_most_linked_templates_monthly_dag.py"]


def test_querypage_most_linked_templates_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="querypage_most_linked_templates_monthly")
    assert dag is not None
    assert len(dag.tasks) == 6
