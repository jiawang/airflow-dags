import json

import pytest
from airflow.models import DagBag, TaskInstance
from airflow.utils.state import DagRunState

from research.config.wikis import WIKIS


@pytest.fixture
def dag_path() -> list[str]:
    return ["research", "dags", "revert_risk_dag.py"]


def revert_risk_pipeline(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="revert_risk_pipeline")
    assert dag
    assert len(dag.tasks) == 4


def test_base_features_pipeline(dagbag: DagBag) -> None:
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="base_features_pipeline")
    assert dag
    assert len(dag.tasks) == 2


def test_base_features_pipeline_make_args(dagbag: DagBag) -> None:
    dag = dagbag.get_dag(dag_id="base_features_pipeline")
    assert dag

    dag_run = dag.create_dagrun(
        run_id="test",
        state=DagRunState.RUNNING,
        start_date=dag.start_date,
        execution_date=dag.start_date,
    )
    ti = TaskInstance(task=dag.get_task(task_id="make_args"), run_id=dag_run.run_id)
    ti.run(
        ignore_all_deps=True,
        ignore_task_deps=True,
        ignore_ti_state=True,
        test_mode=True,
    )

    args_str = ti.xcom_pull(ti.task_id)
    assert args_str

    args = json.loads(args_str)
    expected = {
        "snapshot": "2024-04",
        "wikis": WIKIS,
        "period": {"start": "2024-04-01T00:00:00", "end": "2024-04-01T00:00:00"},
        "output": "/tmp/research/revert_risk/base_features_dataset",
        "partition_output": ["wiki_db", 10],
    }
    assert args == expected
