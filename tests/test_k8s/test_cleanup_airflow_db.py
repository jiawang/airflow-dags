import pytest
from freezegun import freeze_time

from test_k8s.dags.cleanup_airflow_db_dag import run_cleanup_airflow_db_command


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["test_k8s", "dags", "cleanup_airflow_db_dag.py"]


@pytest.fixture()
def airflow_variables(monkeypatch):
    monkeypatch.setenv("AIRFLOW_VAR_DB_CLEANUP_TABLES", "log,job")
    monkeypatch.setenv("AIRFLOW_VAR_DB_CLEANUP_RETENTION_DAYS", "30")


@freeze_time("2024-09-26")
def test_run_cleanup_airflow_db_command(airflow_variables, mocker):
    subproces_run_mock = mocker.stub()
    mocker.patch("subprocess.run", subproces_run_mock)
    run_cleanup_airflow_db_command()
    subproces_run_mock.assert_called_once_with(
        [
            "airflow",
            "db",
            "clean",
            "--tables",
            "log,job",
            "--clean-before-timestamp",
            "2024-08-27",
            "--yes",
        ]
    )


@freeze_time("2024-09-26")
def test_run_cleanup_airflow_db_command_without_variables_set(mocker):
    subproces_run_mock = mocker.stub()
    mocker.patch("subprocess.run", subproces_run_mock)
    run_cleanup_airflow_db_command()
    assert subproces_run_mock.call_count == 0


def test_dag_structure(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="cleanup_airflow_db")
    assert dag is not None
    assert "airflow_maintenance" in dag.tags
    assert dag.schedule_interval == "@daily"
    assert dag.task_count == 1
    task = dag.task_dict["run_cleanup_airflow_db_command"]
    assert callable(task.python_callable)
    assert isinstance(task.doc_md, str)
