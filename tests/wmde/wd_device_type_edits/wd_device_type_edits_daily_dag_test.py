"""
Tests for wd_device_type_edits_daily_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["wmde", "dags", "wd_device_type_edits", "wd_device_type_edits_daily_dag.py"]


def test_wd_device_type_edits_daily_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_device_type_edits_daily")
    assert dag is not None
    assert len(dag.tasks) == 3  # 5
