"""
Tests for wd_rest_api_metrics_monthly_dag.
"""

import pytest


# This fixture defines the dag_path for the shared dagbag one.
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["wmde", "dags", "wd_rest_api_metrics", "wd_rest_api_metrics_monthly_dag.py"]


def test_wd_rest_api_metrics_monthly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wd_rest_api_metrics_monthly")
    assert dag is not None
    assert len(dag.tasks) == 4
