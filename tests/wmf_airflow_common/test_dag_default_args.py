from pytest import raises

from wmf_airflow_common.config import dag_default_args


def test_get_in_non_wmf_env(monkeypatch):
    # Make sure these are unset so we non-wmf env variants.
    monkeypatch.delenv("AIRFLOW_ENVIRONMENT_NAME", raising=False)
    monkeypatch.delenv("AIRFLOW_INSTANCE_NAME", raising=False)
    assert dag_default_args.get() == dag_default_args.base_default_args
    assert dag_default_args.get({"myarg": "hello"}) == {
        **dag_default_args.base_default_args,
        **{"myarg": "hello"},
    }


def test_get_in_wmf_dev_env(monkeypatch):
    # Make sure these are set as in a wmf dev-env variants.
    monkeypatch.setenv("AIRFLOW_ENVIRONMENT_NAME", "dev_wmf")
    monkeypatch.delenv("AIRFLOW_INSTANCE_NAME", raising=False)
    assert dag_default_args.get() == dag_default_args.wmf_base_default_args
    assert dag_default_args.get({"myarg": "hello"}) == {
        **dag_default_args.wmf_base_default_args,
        **{"myarg": "hello"},
    }


def test_get_in_wmf_prod_env(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf-prod env variants.
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "analytics-test")
    assert dag_default_args.get() == dag_default_args.wmf_prod_base_default_args
    assert dag_default_args.get({"myarg": "hello"}) == {
        **dag_default_args.wmf_prod_base_default_args,
        **{"myarg": "hello"},
    }


def test_get_with_nested_dict(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv("AIRFLOW_INSTANCE_NAME", "analytics-test")
    result = dag_default_args.get(
        {"conf": {"spark.dynamicAllocation.enabled": "false"}},
    )

    expected = {
        **dag_default_args.wmf_prod_base_default_args,
        **{
            "conf": {
                **dag_default_args.wmf_prod_base_default_args["conf"],
                **{"spark.dynamicAllocation.enabled": "false"},
            }
        },
    }

    assert result == expected


def test_global_confs_are_immutable():
    with raises(TypeError):
        dag_default_args.base_default_args["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.wmf_base_default_args["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.wmf_prod_base_default_args["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.cassandra_default_conf["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.druid_default_conf["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.spark_3_3_2_conf["trytomodify"] = "modification"  # type: ignore
    with raises(TypeError):
        dag_default_args.get()["trytomodify"] = "modification"  # type: ignore
