# [WMDE](https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmde)

DAGs maintained by the analytics team at Wikimedia Deutschland. For the associated jobs please see [repos/wmde/analytics](https://gitlab.wikimedia.org/repos/wmde/analytics).
