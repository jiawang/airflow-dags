"""
Derives weekly segments of Wikidata items based on the following criteria:

- Population A: Items that contain a sitelink to one or more Wikimedia projects
- Population B: Items that those in Population A are connected to (A -> B)
- Population C: Items not included in Population A and Population B

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_item_sitelink_segments
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    ARCHIVE_CSV,
    COMPUTE,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    WEEKLY,
    WEEKLY_STARTING_ON_MONDAY,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

TASK_ID = "wd_item_sitelink_segments"
DAG_ID = f"{TASK_ID}_{WEEKLY}"
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{TASK_ID}"

WD_ITEM_SITELINK_SEGMENTS = TASK_ID
WD_ITEM_SITELINK_SEGMENTS_WEEKLY = DAG_ID
WD_ITEM_SITELINK_SEGMENTS_GEN_CSV_WEEKLY = f"{WD_ITEM_SITELINK_SEGMENTS}_{GEN_CSV}_{WEEKLY}"

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_wikidata_entity_table="wmf.wikidata_entity",
    # HDFS destination table:
    hive_wmde_wd_item_sitelink_segments_weekly=f"wmde.{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}",
    # Task Hive query:
    hql_wd_item_sitelink_segments_weekly=f"{GITLAB_HQL_TASK_DIR}/{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}.hql",
    # TMP export directory:
    tmp_dir_wd_item_sitelink_segments_weekly=(
        f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}"
    ),
    # TMP export query:
    hql_gen_csv_wd_item_sitelink_segments_weekly=(
        f"{GITLAB_HQL_TASK_DIR}/{WD_ITEM_SITELINK_SEGMENTS_GEN_CSV_WEEKLY}.hql"
    ),
    # Archive export directory:
    pub_data_dir_wd_item_sitelink_segments_weekly=(
        f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}"
    ),
    # Archive export file:
    archive_csv_wd_item_sitelink_segments_weekly=f"{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}.csv",
    # Metadata:
    start_date=datetime(2024, 5, 6),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "from_hive",
        "requires_wmf_wikidata_entity",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
        "weekly",
    ],
)

with create_easy_dag(
    dag_id=DAG_ID,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule=WEEKLY_STARTING_ON_MONDAY,
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    sensor = dataset("hive_wmf_wikidata_entity").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}",
        sql=props.hql_wd_item_sitelink_segments_weekly,
        query_parameters={
            "source_table": props.hive_wikidata_entity_table,
            "destination_table": props.hive_wmde_wd_item_sitelink_segments_weekly,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSV

    gen_csv = SparkSqlOperator(
        task_id=f"{GEN_CSV}_{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}",
        sql=props.hql_gen_csv_wd_item_sitelink_segments_weekly,
        query_parameters={
            "source_table": props.hive_wmde_wd_item_sitelink_segments_weekly,
            "destination_directory": props.tmp_dir_wd_item_sitelink_segments_weekly,
        },
    )

    # MARK: Archive Dataset

    archive_csv = HDFSArchiveOperator(
        task_id=f"{ARCHIVE_CSV}_{WD_ITEM_SITELINK_SEGMENTS_WEEKLY}",
        source_directory=props.tmp_dir_wd_item_sitelink_segments_weekly,
        archive_file=props.pub_data_dir_wd_item_sitelink_segments_weekly
        + "/"
        + props.archive_csv_wd_item_sitelink_segments_weekly,
        expected_filename_ending=".csv",
        check_done=True,
    )

    # MARK: Execute DAG

    sensor >> compute >> gen_csv >> archive_csv
