"""
Derives daily partitions of Wikidata Query service queries based on various criteria.

# See: https://gitlab.wikimedia.org/repos/wmde/analytics/-/tree/main/hql/airflow_jobs/wd_query_segments
"""

from datetime import datetime, timedelta

from wmde.config.dag_config import (
    COMPUTE,
    DAILY,
    GEN_CSV,
    GITLAB_WMDE_HQL_JOBS_DIR,
    HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR,
    HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR,
    WMDE_ANALYTICS_ALERTS_EMAIL,
    create_easy_dag,
    dataset,
    spark_sql_operator_default_args,
)
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator

# MARK: Identifiers

TASK_ID = "wd_query_segments"
DAG_ID = f"{TASK_ID}_{DAILY}"
GITLAB_HQL_TASK_DIR = f"{GITLAB_WMDE_HQL_JOBS_DIR}/{TASK_ID}"

WD_QUERY_SEGMENTS = TASK_ID
WD_QUERY_SEGMENTS_DAILY = DAG_ID
GEN_CSV_WD_QUERY_SEGMENTS_DAILY = f"{GEN_CSV}_{WD_QUERY_SEGMENTS_DAILY}"

# MARK: Properties

props = DagProperties(
    # HDFS source table:
    hive_discovery_processed_external_sparql_query="discovery.processed_external_sparql_query",
    # HDFS destination table:
    hive_wmde_wd_query_segments_daily=f"wmde.{WD_QUERY_SEGMENTS_DAILY}",
    # Task Hive query:
    hql_wd_query_segments_daily=f"{GITLAB_HQL_TASK_DIR}/{WD_QUERY_SEGMENTS_DAILY}.hql",
    # TMP export directory:
    tmp_dir_wd_query_segments_daily=f"{HDFS_TMP_WMDE_ANALYTICS_AIRFLOW_DIR}/{WD_QUERY_SEGMENTS_DAILY}",
    # TMP export query:
    hql_gen_csv_wd_query_segments_daily=f"{GITLAB_HQL_TASK_DIR}/{GEN_CSV_WD_QUERY_SEGMENTS_DAILY}.hql",
    # Archive export directory:
    pub_data_dir_wd_query_segments_daily=(f"{HDFS_PUBLISHED_DATASETS_WMDE_ANALYTICS_DIR}/{WD_QUERY_SEGMENTS_DAILY}"),
    # Archive export file:
    archive_csv_wd_query_segments_daily=f"{WD_QUERY_SEGMENTS_DAILY}.csv",
    # Metadata:
    start_date=datetime(2024, 7, 1),
    sla=timedelta(hours=6),
    alerts_email=WMDE_ANALYTICS_ALERTS_EMAIL,
    tags=[
        "daily",
        "from_hive",
        "requires_discovery_processed_external_sparql_query",
        "to_hive",
        "to_published_datasets",
        "uses_hql",
    ],
)

with create_easy_dag(
    dag_id=DAG_ID,
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@daily",
    sla=props.sla,
    email=props.alerts_email,
) as dag:
    # MARK: Sensor

    wiki_partition_list = [["wiki=wikidata"]]

    sensor = dataset("hive_discovery_processed_external_sparql_query_wikidata").get_sensor_for(dag)

    # MARK: Compute Metrics

    compute = SparkSqlOperator(
        task_id=f"{COMPUTE}_{WD_QUERY_SEGMENTS_DAILY}",
        sql=props.hql_wd_query_segments_daily,
        query_parameters={
            "source_table": props.hive_discovery_processed_external_sparql_query,
            "destination_table": props.hive_wmde_wd_query_segments_daily,
            "year": "{{data_interval_start.year}}",
            "month": "{{data_interval_start.month}}",
            "day": "{{data_interval_start.day}}",
        },
        **spark_sql_operator_default_args,
    )

    # MARK: Generate CSVs

    # gen_csv = SparkSqlOperator(
    #     task_id=f"{GEN_CSV}_{WD_QUERY_SEGMENTS_DAILY}",
    #     sql=props.hql_gen_csv_wd_query_segments_daily,
    #     query_parameters={
    #         "source_table": props.hive_wmde_wd_query_segments_daily,
    #         "destination_directory": props.tmp_dir_wd_query_segments_daily,
    #     },
    # )

    # MARK: Archive Datasets

    # archive_csv = HDFSArchiveOperator(
    #     task_id=f"{ARCHIVE_CSV}_{WD_QUERY_SEGMENTS_DAILY}",
    #     source_directory=props.tmp_dir_wd_query_segments_daily,
    #     archive_file=props.pub_data_dir_wd_query_segments_daily + "/" + props.archive_csv_wd_query_segments_daily,
    #     expected_filename_ending=".csv",
    #     check_done=True,
    # )

    # MARK: Execute DAG

    sensor >> compute  # >> gen_csv >> archive_csv
