import re
from abc import ABC, abstractmethod
from datetime import timedelta
from typing import Dict, List, Optional, cast

import datahub.emitter.mce_builder as datahub_mce_builder
import yaml
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)
from airflow.sensors.base import BaseSensorOperator
from datahub.metadata.schema_classes import MetadataChangeEventClass
from mergedeep import Strategy, merge

from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.partitions_builder import (
    PrePartitions,
    daily_partitions,
    partition_names_by_granularity,
    snapshot_partition_names,
)
from wmf_airflow_common.sensors.druid import DruidSegmentSensor
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.util import one_line


class Dataset(ABC):
    """
    Abstract class for all dataset classes, like HiveDataset.
    A Dataset instance will represent and contain all properties of a dataset,
    including where to locate it, its ID, partitioning scheme, etc.
    """

    # Default Sensor poke interval for each DAG schedule.
    POKE_INTERVALS_BY_SCHEDULE = {
        "@hourly": timedelta(minutes=5).total_seconds(),
        "@daily": timedelta(minutes=15).total_seconds(),
        "@weekly": timedelta(minutes=30).total_seconds(),
        "@weekly_starting_monday": timedelta(minutes=30).total_seconds(),
        "@monthly": timedelta(minutes=60).total_seconds(),
    }

    # Map between cron-syntax DAG schedule intervals and
    # their corresponding dataset partitioning schemes.
    EQUIVALENT_INTERVALS = {
        "0 0 * * 1": "@weekly_starting_monday",
        "0 0-22 * * *": "@hourly",  # Hourly except for the hour 23.
    }

    def __init__(self) -> None:
        """
        For now this does nothing.
        Add here parameters common to all types of Datasets.
        """
        pass

    def _get_normalized_schedule_interval(self, dag: DAG) -> str:
        """
        Returns the normalized schedule interval (@hourly, @daily, etc.)
        corresponding to the given DAG's schedule interval.
        For example: Given "0 0 * * 1" (weekly starting Mondays),
        it returns "@weekly_starting_monday", which is equivalent.
        Raises an error if the schedule interval is not supported.
        """
        # For custom timetables, we get the schedule from a compatibility function
        if "timetable" in dir(dag) and "schedule_compatibility" in dir(dag.timetable):
            schedule_interval: str = dag.timetable.schedule_compatibility()
        else:
            schedule_interval = cast(str, dag.schedule_interval)

        if dag.schedule_interval in Dataset.EQUIVALENT_INTERVALS:
            schedule_interval = Dataset.EQUIVALENT_INTERVALS[schedule_interval]
        if schedule_interval not in Dataset.POKE_INTERVALS_BY_SCHEDULE:
            raise ValueError(f"The schedule interval {schedule_interval} is not supported")
        return schedule_interval

    def _get_poke_interval(self, dag: DAG) -> float:
        """
        Returns the Sensor poke interval (in seconds) to apply
        for the given DAG's normalized schedule interval.
        """
        schedule_interval = self._get_normalized_schedule_interval(dag)
        return Dataset.POKE_INTERVALS_BY_SCHEDULE[schedule_interval]

    @abstractmethod
    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None) -> BaseSensorOperator:
        """
        All Datasets should implement this method.
        It receives a DAG, and it should return a Sensor that checks for the
        existence of the Dataset's source data at the correct time interval.
        """
        pass

    @abstractmethod
    def datahub_dataset_urn(self, datahub_environment: str) -> str:
        pass

    def datahub_upstream_lineage_mce(
        self, upstream_datasets: List["Dataset"], datahub_environment: str
    ) -> MetadataChangeEventClass:
        return datahub_mce_builder.make_lineage_mce(
            upstream_urns=[
                dataset.datahub_dataset_urn(datahub_environment=datahub_environment) for dataset in upstream_datasets
            ],
            downstream_urn=self.datahub_dataset_urn(datahub_environment=datahub_environment),
        )

    @abstractmethod
    def name_snakecase(self) -> str:
        pass


class HiveDataset(Dataset):
    """
    Dataset for Hive tables.
    """

    # Constants for parameter checking.
    TABLE_NAME_REGEX = r"^[a-z_]+\.[a-z_0-9]+$"
    PRE_PARTITION_REGEX = r"^[a-z_]+=[A-Za-z0-9_\-.]+$"
    SUPPORTED_PARTITIONING = list(Dataset.POKE_INTERVALS_BY_SCHEDULE.keys())

    def __init__(
        self,
        table_name: str,
        partitioning: str,
        pre_partitions: Optional[PrePartitions] = None,
        custom_partition_format: Optional[str] = None,
    ):
        """
        Initializes a HiveDataset object.
        :param table_name:
            Fully qualified table name of the dataset, i.e. `database.table`.
        :param partitioning:
            Partitioning scheme of the dataset: `@hourly`, `@daily`,
            `@weekly`, `@weekly_starting_monday`, `@monthly`.
        :param pre_partitions:
            Extra partitions that the dataset might have prepended to the time-based partitions.
        :param custom_partition_format:
            Specify a custom partition format to use instead of the default one
            See wmf_airflow_common.partitions_builder::PrePartitions
        """
        super().__init__()

        # Check format of parameters.
        if not re.match(HiveDataset.TABLE_NAME_REGEX, table_name):
            raise ValueError(f"Invalid table name {table_name}")
        if partitioning not in HiveDataset.SUPPORTED_PARTITIONING:
            raise ValueError(f"Invalid partitioning {partitioning}")
        if pre_partitions is not None:
            for pre_partition in pre_partitions:
                if type(pre_partition) == str:
                    partition_values = [pre_partition]
                else:  # type(pre_partition) == List[str]
                    partition_values = cast(List[str], pre_partition)
                for partition_value in partition_values:
                    if not re.match(HiveDataset.PRE_PARTITION_REGEX, partition_value):
                        raise ValueError(f"Invalid pre-partition value {partition_value}")

        self.table_name = table_name
        self.partitioning = partitioning
        self.pre_partitions = pre_partitions
        self.custom_partition_format = custom_partition_format

    def _get_task_id(self) -> str:
        """
        Returns the normalized task name of the sensor.
        """
        return "wait_for_" + self.table_name.replace(".", "_") + "_partitions"

    def _raise_schedule_error(self, partitioning: str, schedule_interval: str) -> None:
        """
        Raises an error with a message indicating that the current
        dataset partitioning and schedule interval are not compatible.
        """
        raise RuntimeError(f"Can not define a Sensor for a {partitioning} " f"Dataset and a {schedule_interval} DAG.")

    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None) -> BaseSensorOperator:
        """
        Returns a Sensor that will check for the existence of the appropriate partitions given
        the DAG's schedule interval, the dataset's granularity and the dag_run's execution date.
        :param dag:
            DAG object for the sensor to run in.
        :param custom_task_id:
            Custom name for the sensor. If not provided, the name is going to be auto-generated.
        """

        # Parameters common to all Sensors.
        task_id = custom_task_id or self._get_task_id()
        schedule_interval = self._get_normalized_schedule_interval(dag)
        poke_interval = self._get_poke_interval(dag)

        # For DAGs that have the same schedule interval as the source data granularity
        # the NamedHivePartitionSensor should be used. The partition_names parameter
        # will be passed a single partition. Weekly DAGs are excluded here for now.
        if (self.partitioning, schedule_interval) in [
            ("@hourly", "@hourly"),
            ("@daily", "@daily"),
            ("@monthly", "@monthly"),
        ]:
            sensor = NamedHivePartitionSensor(
                task_id=task_id,
                partition_names=partition_names_by_granularity(
                    table=self.table_name,
                    granularity=self.partitioning,
                    pre_partitions=self.pre_partitions,
                    custom_partition_format=self.custom_partition_format,
                ),
                poke_interval=poke_interval,
                dag=dag,
            )

        # For DAGs that have a @daily schedule interval and query an @hourly source data
        # the NamedHivePartitionSensor should be used as well. The partition_names parameter
        # will be passed a list of all the hourly partitions within the required day.
        elif (self.partitioning, schedule_interval) in [
            ("@hourly", "@daily"),
        ]:
            sensor = NamedHivePartitionSensor(
                task_id=task_id,
                partition_names=daily_partitions(
                    table=self.table_name,
                    granularity=self.partitioning,
                    pre_partitions=self.pre_partitions,
                    custom_partition_format=self.custom_partition_format,
                ),
                poke_interval=poke_interval,
                dag=dag,
            )

        # For DAGs that have @weekly or @monthly schedule interval and query @hourly or @daily
        # source data the RangeHiveParititionSensor should be used. It will calculate the
        # final partitions at execution time, from the passed from_timestamp and to_timestamp.
        elif (self.partitioning, schedule_interval) in [
            ("@hourly", "@weekly"),
            ("@hourly", "@weekly_starting_monday"),
            ("@hourly", "@monthly"),
            ("@daily", "@weekly"),
            ("@daily", "@weekly_starting_monday"),
            ("@daily", "@monthly"),
        ]:
            if schedule_interval == "@monthly":
                to_timestamp = "{{data_interval_start.add(months=1)}}"
            else:
                to_timestamp = "{{data_interval_start.add(days=7)}}"
            sensor = RangeHivePartitionSensor(
                task_id=task_id,
                table_name=self.table_name,
                from_timestamp="{{data_interval_start}}",
                to_timestamp=to_timestamp,
                granularity=self.partitioning,
                pre_partitions=self.pre_partitions,
                custom_partition_format=self.custom_partition_format,
                poke_interval=poke_interval,
                dag=dag,
            )

        else:
            self._raise_schedule_error(self.partitioning, schedule_interval)

        return cast(BaseSensorOperator, sensor)

    def datahub_dataset_urn(self, datahub_environment: str) -> str:
        return str(datahub_mce_builder.make_dataset_urn("hive", self.table_name, datahub_environment))

    def name_snakecase(self) -> str:
        return f"hive_{self.table_name.replace('.', '_')}"


class HiveSnapshotDataset(HiveDataset):
    """
    Dataset for snapshot-based Hive tables.
    """

    # Constants for parameter checking.
    SUPPORTED_PARTITIONING = ["@weekly", "@weekly_starting_monday", "@monthly"]
    SNAPSHOT_FIELD_REGEX = r"^[a-z_]+$"

    def __init__(
        self,
        table_name: str,
        partitioning: str,
        pre_partitions: Optional[PrePartitions] = None,
        snapshot_field: str = "snapshot",
    ):
        """
        Initializes a HiveSnapshotDataset object.
        :param table_name:
            Fully qualified table name of the dataset, i.e. `database.table`.
        :param partitioning:
            Partitioning scheme of the dataset: `@weekly`,
            `@weekly_starting_monday`, `@monthly`.
        :param pre_partitions:
            Extra partitions that the dataset might have prepended to the time-based partitions.
            See wmf_airflow_common.partitions_builder::PrePartitions
        :param snapshot_field:
            Table field that contains the partition snapshot value.
        """
        if partitioning not in HiveSnapshotDataset.SUPPORTED_PARTITIONING:
            raise ValueError(f"Invalid partitioning {partitioning}")
        if not re.match(HiveSnapshotDataset.SNAPSHOT_FIELD_REGEX, snapshot_field):
            raise ValueError(f"Invalid snapshot field value {snapshot_field}")
        self.snapshot_field = snapshot_field
        super().__init__(
            table_name=table_name,
            partitioning=partitioning,
            pre_partitions=pre_partitions,
        )

    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None) -> BaseSensorOperator:
        """
        Returns a Sensor that will check for the existence of the appropriate partitions given
        the DAG's schedule interval, the dataset's granularity and the dag_run's execution date.
        :param dag:
            DAG object for the sensor to run in.
        """
        # Parameters common to all Sensors.
        task_id = custom_task_id or self._get_task_id()
        schedule_interval = self._get_normalized_schedule_interval(dag)
        poke_interval = self._get_poke_interval(dag)

        # For DAGs that have a @weekly or @monthly schedule interval and query a snapshot-
        # based dataset of the same frequency, a NamedHivePartitionSensor should be used.
        # The partition_names parameter will be passed the corresponding snapshot partition.
        if (self.partitioning, schedule_interval) in [
            ("@weekly", "@weekly"),
            ("@weekly_starting_monday", "@weekly_starting_monday"),
            ("@monthly", "@monthly"),
        ]:
            sensor = NamedHivePartitionSensor(
                task_id=task_id,
                partition_names=snapshot_partition_names(
                    table=self.table_name,
                    granularity=self.partitioning,
                    pre_partitions=self.pre_partitions,
                    snapshot_field=self.snapshot_field,
                ),
                poke_interval=poke_interval,
                dag=dag,
            )

        else:
            self._raise_schedule_error(self.partitioning, schedule_interval)

        return cast(BaseSensorOperator, sensor)


class DruidDataset(Dataset):
    """
    Dataset for Druid datasources.
    """

    # Constants for parameter checking.
    DATASOURCE_NAME_REGEX = r"^[a-z0-9_]+$"
    SUPPORTED_GRANULARITIES = ["@hourly", "@daily", "@monthly"]

    def __init__(
        self,
        datasource_name: str,
        segment_granularity: str,
    ):
        """
        Initializes a DruidDataset object.
        :param datasource_name:
            Name of the Druid datasource.
        :param segment_granularity:
            Granularity of the dataset's segments: `@hourly`, `@daily`, `@monthly`.
        """
        super().__init__()

        # Check format of parameters.
        if not re.match(DruidDataset.DATASOURCE_NAME_REGEX, datasource_name):
            raise ValueError(f"Invalid datasource name {datasource_name}")
        if segment_granularity not in DruidDataset.SUPPORTED_GRANULARITIES:
            raise ValueError(f"Invalid segment granularity {segment_granularity}")

        self.datasource_name = datasource_name
        self.segment_granularity = segment_granularity

    def _get_task_id(self) -> str:
        """
        Returns the normalized task name of the sensor.
        """
        return f"wait_for_{self.datasource_name}_segments"

    def _raise_schedule_error(self, segment_granularity: str, schedule_interval: str) -> None:
        """
        Raises an error with a message indicating that the current
        dataset segment granularity and schedule interval are not compatible.
        """
        raise RuntimeError(
            f"Can not define a Druid Sensor for a {segment_granularity} " f"Dataset and a {schedule_interval} DAG."
        )

    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None) -> BaseSensorOperator:
        """
        Returns a Sensor that will check for the existence of the appropriate segments given
        the DAG's schedule interval, the datasource's granularity and the dag_run's execution date.
        :param dag:
            DAG object for the sensor to run in.
        :param custom_task_id:
            Custom name for the sensor. If not provided, the name is going to be auto-generated.
        """

        # Parameters common to all Sensors.
        task_id = custom_task_id or self._get_task_id()
        schedule_interval = self._get_normalized_schedule_interval(dag)
        poke_interval = self._get_poke_interval(dag)

        # For any of the following pairs of segment granularity and dag schedule,
        # the DruidSegmentSensor will be used. The sensor will calculate the segments
        # to check for at execution time depending on timestamps and granularity.
        # Weekly DAGs are excluded here for now.
        if (self.segment_granularity, schedule_interval) in [
            ("@hourly", "@hourly"),
            ("@daily", "@daily"),
            ("@monthly", "@monthly"),
            ("@hourly", "@daily"),
        ]:
            sensor = DruidSegmentSensor(
                task_id=task_id,
                datasource=self.datasource_name,
                from_timestamp="{{data_interval_start.isoformat()}}",
                to_timestamp="{{data_interval_end.isoformat()}}",
                granularity=self.segment_granularity,
                poke_interval=poke_interval,
                dag=dag,
            )

        else:
            self._raise_schedule_error(self.segment_granularity, schedule_interval)

        return cast(BaseSensorOperator, sensor)

    def datahub_dataset_urn(self, datahub_environment: str) -> str:
        return str(datahub_mce_builder.make_dataset_urn("druid", self.datasource_name, datahub_environment))

    def name_snakecase(self) -> str:
        return f"druid_{self.datasource_name.replace('.', '_')}"


class IcebergDataset(Dataset):
    """
    Dataset for Iceberg tables.

    See DEFAULT_MAINTENANCE for possible maintenance configurations.
    """

    # Currently supported maintenance schedules
    SUPPORTED_MAINTENANCE_SCHEDULES = ["@daily", "@weekly", "@monthly"]

    # Defaults conf for routine maintenance
    DEFAULT_MAINTENANCE = {
        "enabled": True,
        "schedule": "@monthly",
        "remove_orphan_files": {
            "enabled": True,
            "older_than_days": 2,
            "concurrency": 10,
        },
        "expire_snapshots": {
            "enabled": True,
            "older_than_days": 90,
            "concurrency": 10,
        },
        "rewrite_manifests": {
            "enabled": True,
        },
        "data_delete": {
            "enabled": False,
        },
        "rewrite_data_files": {
            "enabled": False,
            "spark_kwargs": {},
            "spark_conf": {},
        },
    }

    def __init__(
        self,
        table_name: str,
        maintenance: Optional[dict] = None,
    ):
        """
        Initializes an IcebergDataset object.
        :param table_name:
            Fully qualified table name of the dataset, i.e. `database.table`.
        :param maintenance:
            A dict containing the maintenance configuration for this IcebergDataset,
            or None if we want the defaults as in IcebergDataset.DEFAULT_MAINTENANCE.
        """
        super().__init__()

        # Check format of parameters.
        if not re.match(HiveDataset.TABLE_NAME_REGEX, table_name):
            raise ValueError(f"Invalid table name {table_name}")

        merged_maintenance = merge(
            {}, IcebergDataset.DEFAULT_MAINTENANCE, maintenance or {}, strategy=Strategy.TYPESAFE_REPLACE
        )
        if merged_maintenance["schedule"] not in IcebergDataset.SUPPORTED_MAINTENANCE_SCHEDULES:
            raise ValueError(
                f"{IcebergDataset.SUPPORTED_MAINTENANCE_SCHEDULES} are the only maintenance schedules "
                f"supported right now. You provided {merged_maintenance['schedule']}."
            )

        self.table_name = table_name
        self.maintenance = merged_maintenance

    def _get_task_id(self) -> str:
        """
        Returns the normalized task name of the sensor.
        """
        # TODO: This is a stub implementation to be filled in via https://phabricator.wikimedia.org/T372647
        raise NotImplementedError("_get_task_id() not implemented yet.")

    def get_sensor_for(self, dag: DAG, custom_task_id: Optional[str] = None) -> BaseSensorOperator:
        """
        Returns a Sensor that will check for the existence of the appropriate partitions given
        the DAG's schedule interval, the dataset's granularity and the dag_run's execution date.
        :param dag:
            DAG object for the sensor to run in.
        :param custom_task_id:
            Custom name for the sensor. If not provided, the name is going to be auto-generated.
        """
        # TODO: This is a stub implementation to be filled in via https://phabricator.wikimedia.org/T372647
        raise NotImplementedError("get_sensor_for() not implemented yet.")

    def datahub_dataset_urn(self, datahub_environment: str) -> str:
        return str(datahub_mce_builder.make_dataset_urn("iceberg", self.table_name, datahub_environment))

    def name_snakecase(self) -> str:
        return f"iceberg_{self.table_name.replace('.', '_')}"

    def get_maintenance_tasks(self, dag, spark_conf) -> List[SparkSqlOperator]:
        """
        Returns a List[SparkSqlOperator] of all the maintenance tasks that are currently enabled
        in this IcebergDataset.
        :param dag:
            DAG object for the sensor to run in.
        :param spark_conf:
            A dict with any extra Spark configurations.
        """
        maintenance_tasks = []
        if self.maintenance["enabled"]:
            if self.maintenance["remove_orphan_files"]["enabled"]:
                older_than_days = self.maintenance["remove_orphan_files"]["older_than_days"]
                concurrency = self.maintenance["remove_orphan_files"]["concurrency"]
                maintenance_tasks.append(
                    SparkSqlOperator(
                        task_id="remove_orphan_files",
                        sql=one_line(
                            f"""
                            CALL spark_catalog.system.remove_orphan_files(
                                table => '{self.table_name}',
                                older_than =>
                                  TIMESTAMP '{{{{ data_interval_end | subtract_days({older_than_days}) | to_dt() }}}}',
                                max_concurrent_deletes => {concurrency}
                            )
                        """
                        ),
                        dag=dag,
                        conf=spark_conf,
                    )
                )
            if self.maintenance["expire_snapshots"]["enabled"]:
                older_than_days = self.maintenance["expire_snapshots"]["older_than_days"]
                concurrency = self.maintenance["expire_snapshots"]["concurrency"]
                maintenance_tasks.append(
                    SparkSqlOperator(
                        task_id="expire_snapshots",
                        sql=one_line(
                            f"""
                            CALL spark_catalog.system.expire_snapshots(
                                table => '{self.table_name}',
                                older_than =>
                                  TIMESTAMP '{{{{ data_interval_end | subtract_days({older_than_days}) | to_dt() }}}}',
                                max_concurrent_deletes => {concurrency},
                                stream_results => true
                            )
                            """
                        ),
                        dag=dag,
                        conf=spark_conf,
                    )
                )

            if self.maintenance["rewrite_manifests"]["enabled"]:
                maintenance_tasks.append(
                    SparkSqlOperator(
                        task_id="rewrite_manifests",
                        sql=one_line(
                            f"""
                            CALL spark_catalog.system.rewrite_manifests(
                                table => '{self.table_name}'
                            )
                            """
                        ),
                        dag=dag,
                        conf=spark_conf,
                    )
                )

            if self.maintenance["data_delete"]["enabled"]:
                where = self.maintenance["data_delete"]["where"]
                maintenance_tasks.append(
                    SparkSqlOperator(
                        task_id="delete_data",
                        sql=one_line(
                            f"""
                            DELETE
                            FROM {self.table_name}
                            WHERE {where}
                            """
                        ),
                        dag=dag,
                        conf=spark_conf,
                    )
                )

            # Spark's CALL rewrite_data_files().
            # Iceberg 1.6.1 docs at: https://iceberg.apache.org/docs/1.6.1/spark-procedures/#rewrite_data_files
            if self.maintenance["rewrite_data_files"]["enabled"]:
                where = self.maintenance["rewrite_data_files"].get("where")
                strategy = self.maintenance["rewrite_data_files"].get("strategy")
                sort_order = self.maintenance["rewrite_data_files"].get("sort_order")
                options = self.maintenance["rewrite_data_files"].get("options")
                maybe_call_args = [
                    f"table => '{self.table_name}'",
                    f"where => '{where}'" if where else None,
                    f"strategy => '{strategy}'" if strategy else None,
                    f"sort_order => '{sort_order}'" if sort_order else None,
                    f"""
                    options => map({', '.join([f"'{k}', '{v}'" for k, v in options.items()])})
                    """ if options else None,  # fmt: skip
                ]
                call_args = [arg for arg in maybe_call_args if arg is not None]
                maintenance_tasks.append(
                    SparkSqlOperator(
                        task_id="rewrite_data_files",
                        **self.maintenance["rewrite_data_files"]["spark_kwargs"],
                        sql=one_line(
                            f"""
                            CALL spark_catalog.system.rewrite_data_files(
                                {', '.join(call_args)}
                            )
                            """
                        ),
                        dag=dag,
                        conf=merge({}, spark_conf, self.maintenance["rewrite_data_files"]["spark_conf"]),
                    )
                )

        return maintenance_tasks


class DatasetRegistry:
    """
    Class containing all Dataset objects parsed from a list of Yaml dataset files.
    Provides a get() method that retrieves a Dataset by dataset name.
    """

    # The supported datastore values and their respective Dataset classes.
    datastore_to_dataset_map = {
        "hive": HiveDataset,
        "hive_snapshot": HiveSnapshotDataset,
        "druid": DruidDataset,
        "iceberg": IcebergDataset,
        # Add more datastores and Datasets here.
    }

    def __init__(self, dataset_file_paths: List[str]) -> None:
        """
        Initializes a DatasetRegistry by parsing from a set of Yaml dataset files.
        It uses a different Dataset constructor depending on the dataset's datastore property.
        """
        self.datasets: Dict[str, Dataset] = {}
        for path in dataset_file_paths:
            with open(path, "r") as dataset_file:
                datasets = yaml.safe_load(dataset_file) or {}
                for dataset_name, dataset_params in datasets.items():
                    datastore = dataset_params["datastore"]
                    # The datastore and upstream properties should not be passed to Dataset constructor.
                    del dataset_params["datastore"]
                    # Initialize the Dataset depending on its datastore property.
                    if datastore in DatasetRegistry.datastore_to_dataset_map:
                        dataset_class = DatasetRegistry.datastore_to_dataset_map[datastore]
                        self.datasets[dataset_name] = dataset_class(**dataset_params)
                    else:
                        raise ValueError(f"Datastore {datastore} not supported.")

    def get_dataset(self, dataset_name: str) -> Dataset:
        """
        Returns the Dataset with the given dataset name,
        or raises an error if the dataset name does not correspond to any Dataset.
        """
        if dataset_name not in self.datasets:
            raise KeyError(f"Dataset name {dataset_name} not declared.")
        return self.datasets[dataset_name]

    def get_all_datasets(self) -> List[Dataset]:
        return list(self.datasets.values())
