import os
import shlex
from typing import Any, Dict, List, Optional, Union

import airflow.providers.apache.spark.hooks.spark_submit
from airflow.providers.apache.spark.hooks.spark_submit import (
    SparkSubmitHook as AirflowSparkSubmitHook,
)

from wmf_airflow_common.hooks.skein import SkeinHookBuilder, parse_file_source

# List of all acceptable values for spark binary.
airflow.providers.apache.spark.hooks.spark_submit.ALLOWED_SPARK_BINARIES = [
    "spark-submit",  # default
    "spark3-submit",
    "/usr/lib/spark/bin/spark-submit",
    "venv/bin/spark-submit",
]


class SparkSubmitHook(AirflowSparkSubmitHook):
    """
    Wraps the apache.spark SparkSubmitHook provided with Airflow to accomplish the following:

    * Make Spark application an instance parameter,
      rather than requiring it to be passed to the submit method

    * Support not using Airflow connections. Most of the time, pre-defining
      'spark connections' in Airflow doesn't really make much sense.
      Connection paramters (like yarn deploy-mode, etc.) should be defineable per task.
      This is still backwards compatible with connections, if conn_id is not None.

    * Add driver_cores and driver_java_options parameters.
    """

    def __init__(
        self,
        application: str,
        driver_cores: Optional[int] = None,
        driver_java_options: Optional[str] = None,
        master: Optional[str] = "yarn",
        deploy_mode: Optional[str] = "client",
        queue: Optional[str] = None,
        spark_home: Optional[str] = None,
        conn_id: Optional[str] = None,  # overridden here to change default value
        **kwargs: Any,
    ):
        """

        :param application:
            Spark application, either a .jar, .py or R file.
        :type application: str

        :param launcher:
            Launcher to use. If 'skein' the spark-submit command will
            be run on a YARN worker via skein.  Otherwise, spark-submit
            will be run locally. Defaults to None.
        :type launcher: Optional[str], optional

        :param driver_cores:
            spark-submit --driver-cores, defaults to None.
        :type driver_cores: Optional[int], optional

        :param driver_java_options:
            spark-submit --driver-java-options, defaults to None.
        :type driver_java_options: Optional[str], optional

        :param master:
            spark-submit --master, defaults to 'yarn'.
        :type master: Optional[str], optional

        :param deploy_mode:
            spark-submit --deploy-mode, defaults to 'client'.
        :type deploy_mode: Optional[str], optional

        :param queue:
            spark-submit --queue, defaults to None.
        :type queue: Optional[str], optional

        :param spark_home:
            Used to find spark-binary, defaults to None.
        :type spark_home: Optional[str], optional

        :param conn_id:
            Airflow connectionid.  Overridden here to
            change the default to None, so that passed in parameters
            will take precedence over airflow connections.  If you
            want to use an airflow connection, set this explicitly.
        :type conn_id: Optional[str], optional

        :param **kwargs:
            kwargs to pass to parent Airflow SparkSubmitHook.

        """
        self._application: str = application
        self._master: Optional[str] = master
        self._deploy_mode: Optional["str"] = deploy_mode
        self._queue: Optional[str] = queue
        self._driver_cores: Optional[str] = None
        if driver_cores:
            self._driver_cores = str(driver_cores)

        self._driver_java_options: Optional[str] = driver_java_options
        self._spark_home: Optional[str] = spark_home

        # Since we will be using application_args in a command line string,
        # cast them all to strings now to avoid type errors later.
        # NOTE: This works since application_args are not set in default_args
        if "application_args" in kwargs and kwargs["application_args"] is not None:
            kwargs["application_args"] = [str(arg) for arg in kwargs["application_args"]]

        super().__init__(
            # conn_id needs to be passed explicitly
            # since we override its default value.
            conn_id=conn_id,
            deploy_mode=deploy_mode,
            **kwargs,
        )

    def _resolve_connection(self) -> Dict[str, Any]:
        """
        Overrides Airflow SparkSubmitHook to support using instance properties
        if conn_id is not defined.

        :return: connection dict.
        :rtype: Dict[str, Any]
        """
        if self._conn_id is not None:
            return dict(super()._resolve_connection())

        spark_binary = self.spark_binary

        # Build from connection from instance parameters instead of connection
        return {
            "master": self._master,
            "deploy_mode": self._deploy_mode,
            "queue": self._queue,
            "spark_home": self._spark_home,
            "spark_binary": spark_binary or "spark-submit",
            "namespace": None,
        }

    def _build_spark_submit_command(self, application: Optional[str] = None) -> List[str]:
        """
        Overrides Airflow SparkSubmitHook to support some extra options, including
        application, driver_cores, driver_java_options.  This also
        extends env_vars support to have them set in Spark executors as well as driver.

        :param application:
            Only used for compatibility with parent method. Will be used if set,
            but the default of None will make the instance's self._application be used.
        :type application: Optional[str], optional

        :return: spark-submit command
        :rtype: List[str]
        """
        application = application or self._application
        connection_cmd: List[str] = super()._build_spark_submit_command(application)

        # If _env_vars, then make sure these are set in executor as well as
        # driver.  (Parent Airflow SparkSubmitHook handles setting them for driver).
        if self._env_vars and (self._is_kubernetes or self._is_yarn):
            for key, val in self._env_vars.items():
                connection_cmd.insert(1, f"spark.executorEnv.{key}={val}")
                connection_cmd.insert(1, "--conf")

        if self._driver_cores:
            connection_cmd.insert(1, self._driver_cores)
            connection_cmd.insert(1, "--driver-cores")

        if self._driver_java_options:
            connection_cmd.insert(1, self._driver_java_options)
            connection_cmd.insert(1, "--driver-java-options")

        self.log.info("%s spark-submit final cmd: %s", self, self._mask_cmd(connection_cmd))
        return connection_cmd

    def submit(self, application: Optional[str] = None, **kwargs: Any) -> Any:
        """
        Overrides Airflow SparkSubmitHook to use our extra params.

        :param application:
            Only used for compatibility with parent method. Will be used if set,
            but the default of None will make the instance's self._application be used.
        """
        application = application or self._application
        return super().submit(application, **kwargs)

    def __str__(self) -> str:
        desc = self.__class__.__name__
        if self._name:
            desc += f"({self._name})"
        return desc


class SparkSkeinSubmitHook(SparkSubmitHook):
    """
    Overrides our SparkSubmitHook too:

    * Add the ability to use SkeinSubmitHook to run the spark-submit command,
      rather than the provided Airflow SparkSubmitHook's submit method, which
      uses subprocess.Popen locally to run spark-submit.
      This allows to fully keep spark processes off of the airflow executor.
      spark in cluster mode does keep work off of the airflow executor, but
      still requires that e.g. python scripts or other resources needed to
      launch the spark job are deployed locally to the executor.  By using
      skein, we can pull down files/archives.
      SkeinSubmitHook is fully wrapped by these SparkSubmitHook parameters.

    """

    def __init__(  # noqa: C901
        self,
        command_preamble: Optional[str] = None,
        command_postamble: Optional[str] = None,
        skein_master_log_level: str = "INFO",
        skein_client_log_level: str = "INFO",
        skein_app_log_collection_enabled: bool = True,
        vcores: Optional[int] = None,
        memory: Optional[int] = None,
        **kwargs: Any,
    ):
        """
        Creates a SkeinSubmitHook to run a spark-submit command, and
        mutates a few SparkSubmitHook properties to work when run on the
        remote YARN worker instead of locally.

        After returning, self._skein_hook will be ready to run.

        :param command_preamble:
            Set this if you ned to dynamically run a command before spark-submit is run.
            This is literally prefixed to the spark-submit command. Commonly used
            to set e.g. SPARK_DIST_CLASSPATH=$(hadoop classpath), allowing
            for hadoop dependencies to be added dynamically.
            Defaults to None.

        :param skein_master_log_level:
            Log level to set for skein YARN AppMaster.
            Defaults to INFO.

        :param skein_client_log_level:
            Log level to set for skein Client.
            Defaults to INFO.

        :param skein_app_log_collection_enabled:
            If YARN AppMaster logs should be collected and logged locally
            after the app finishes.

        :param vcores:
            Number of vcores to use for the Skein application master.
            If not set, defaults to the Spark driver cores in cluster mode, or 1 in other modes.
            See _skein_resources() for more details.

        :param memory
            Number of MB to ask for the Skein application master.
            If not set, defaults to the Spark driver memory in cluster mode, or 4096 in other modes.
            See _skein_resources() for more details.
        """
        super().__init__(**kwargs)

        self._vcores = vcores
        self._memory = memory

        if self._keytab is None:
            self.log.warning(
                "%s launching via skein without a keytab. "
                "If the Spark job needs to interact with services that require "
                "Kerberos authentication (HDFS, Hive, etc.), the job will fail "
                "because it cannot authenticate with Kerberos from the YARN "
                "application master without a keytab.",
                self,
            )

        self._command_preamble = command_preamble
        self._command_postamble = command_postamble

        skein_hook_builder = SkeinHookBuilder()

        # Name the Skein launcher AppMaster after the spark job name.
        spark_name = " " + self._name or ""
        skein_hook_builder.name(f"Airflow {self.__class__.__name__} skein launcher{spark_name}")

        skein_hook_builder.resources(self._skein_resources())

        # If skein_master_log_level set, use it the Skein Yarn APP Master.
        if skein_master_log_level:
            skein_hook_builder.master_log_level(skein_master_log_level)

        # If skein_client_log_level set, use it the Skein Client.
        if skein_client_log_level:
            skein_hook_builder.client_log_level(skein_client_log_level)

        # Set whether we should collect yarn app logs and log them.
        skein_hook_builder.app_log_collection_enabled(skein_app_log_collection_enabled)

        if self._queue:
            skein_hook_builder.queue(self._queue)

        if self._env_vars:
            skein_hook_builder.env(self._env_vars)

        if self._principal:
            skein_hook_builder.principal(self._principal)

        if self._keytab:
            # Spark _keytab will be converted to a skein file for use by
            # the spark-submit command on a YARN worker below, but we
            # also need the skein client running locally to use the same
            # keytab.  Set the skein client's keytab setting
            # before self._keytab is mutated.
            skein_hook_builder.keytab(self._keytab)

        for prop in ["_files", "_py_files", "_archives", "_keytab"]:
            # Each of these spark-submit options expects the file to be available
            # on the YARN worker where the spark-submit command will actually be run.
            # Use skein Master files config to upload the file to the YARN worker,
            # and then mutate the SparkSubmitHook property to reference the
            # relative path on the YARN worker.
            spark_files_str = getattr(self, prop)
            if spark_files_str:
                # Iterate over each of the files given for this prop and collect
                # a possibly transformed list of files to provide to spark-submit
                # on the remote worker.
                spark_files_new = []
                for spark_file_str in spark_files_str.split(","):
                    (alias, skein_file_dict) = parse_file_source(spark_file_str, as_dict=True)

                    # Ship this file to the Skein YARN AppMaster.
                    skein_hook_builder.files({alias: skein_file_dict}, append=True)

                    if not skein_file_dict["source"].startswith("file://"):
                        # If not a local file, then we can expect both Skein and Spark
                        # can access the file remotely.  Skein will download the file
                        # into the AppMaster, and Spark executors will download the file
                        # as well.
                        spark_files_new += [spark_file_str]
                    else:
                        # Else if a local file, mutate Spark's property to use the alias path.
                        # Since spark-submit will run remotely, assume it doesn't have access
                        # to any locally referenced files. We need to give them to spark-submit
                        # as the relative alias name that will be available on the remote worker.

                        if prop == "_archives":
                            # If a local file for --archives, we have to get a little tricky.
                            # This is a case where both the Skein AppMaster and the Spark executors
                            # probably need to have the unpacked archive. We can only use skein
                            # to upload to the YARN AppMaster, and by default Skein will unpack
                            # an archive into a directory.  We can't just provide that unpacked
                            # directory to spark in --archives, but we still need a way to ship
                            # the archive from the AppMaster to the executors.
                            # Here, we add a duplicate skein File for the archive value that
                            # has already been added, but this time add is as a regular FILE,
                            # instead of letting skein infer that it is an ARCHIVE via file ext.
                            # We name alias this packed file with the base name.
                            # Example:
                            #   self._archives was set as local /path/to/conda_env.tgz#conda_env ->
                            #   skein files will be:
                            #   {
                            #       'conda_env': File('/path/to/conda_env.tgz', type=ARCHIVE)
                            #       'conda_env.tgz': File('/path/to/conda_env.tgz', type=FILE)
                            #   }
                            # and the final spark-submit --archives will be
                            #   --archives conda_env.tgz#conda_env
                            #
                            # The 'conda_env/' directory will be unpacked on the AppMaster
                            # and availble there for use, and Spark will then ship
                            # the packed conda_env.tgz to executors.
                            (spark_alias, skein_file_dict) = parse_file_source(
                                spark_file_str, as_dict=True, type="FILE"
                            )
                            skein_alias = "_skein_archive_orig." + os.path.basename(skein_file_dict["source"])
                            # - spark_alias is what is infered from the arg,
                            #   either after # or the basename
                            # - skein_alias will be the name of the packed archive
                            #   on the AppMaster.
                            # The --archives value of this file given to spark-submit
                            # will then be skein_alias#spark_alias.
                            # This allows everything everywhere to refer to the unpacked
                            # archive as spark_alias.
                            skein_hook_builder.files({skein_alias: skein_file_dict}, append=True)

                            spark_file_str_new = f"{skein_alias}#{spark_alias}"
                            spark_files_new += [spark_file_str_new]
                            self.log.info(
                                "%s - Converted file for spark %s from '%s' "
                                "to '%s' for use with skein, "
                                "and uploading extra packed archive named %s.",
                                self,
                                prop,
                                spark_file_str,
                                spark_file_str_new,
                                skein_alias,
                            )
                        else:
                            # Else not a local archive file, so we can just let Skein
                            # upload it to the AppMaster, and then refernce the file
                            # via Spark arg with the relative local alias path on the master.
                            spark_files_new += [alias]
                            self.log.info(
                                "%s - Converted file for spark %s from '%s' " "to '%s' for use with skein.",
                                self,
                                prop,
                                spark_file_str,
                                alias,
                            )

                # Mutate SparkSubmitHook's property to use the collected spark_files_new.
                setattr(self, prop, ",".join(spark_files_new))

        # Finally build the script based on the current SparkSubmitHook properties.
        spark_submit_command = self._build_safe_command(*self._build_spark_submit_command())
        command_preamble = f"{self._command_preamble} " if self._command_preamble else ""
        command_postamble = f" {self._command_postamble}" if self._command_postamble else ""
        script = f"{command_preamble}{spark_submit_command}{command_postamble}"

        skein_hook_builder.script(script)

        self._skein_hook = skein_hook_builder.build()

    def _build_safe_command(self, executable: str, *args: str) -> str:
        """Quote command arguments for execution in a shell script"""
        return executable + " " + " ".join(shlex.quote(arg) for arg in args)

    def submit(self, application: Optional[str] = None, **kwargs: Any) -> Any:
        """
        Build and call a SkeinSubmitHook with our spark-submit command
        to launch spark-submit via YARN.

        :param application:
            Unused, here for compatibility with Airflow SparkSubmitHook only.

        :param kwargs:
            Unused, here for compatibility with Airflow SparkSubmitHook only.

        """
        return self._skein_hook.submit()

    def on_kill(self) -> None:
        """
        Overrides Airflow SparkSubmitHook to handle on_kill if using skein as the launcher.
        """
        return self._skein_hook.on_kill()

    def _skein_resources(self, default_memory: Union[int, str] = 4096, default_vcores: Union[int, str] = 1) -> dict:
        """
        Infers proper skein Resources based on our spark submit settings.
        Returns a dict that can be used by skein.Resources.from_dict()

        Uses default memory and vcores if deploy_mode is cluster,
        since in that case the Skein application master isn't doing any
        Spark work, all Spark resources are managed by YARN. However,
        if deploy_mode != cluster, we should use the Spark Driver settings
        for Skein YARN AppMaster memory and vcores, since the Spark Driver
        will run in the Skein YARN AppMaster.

        When running Spark in local mode within the Skein YARN AppMaster,
        it is advised to set the memory and vcores to a bigger value than
        the Spark Driver settings, to keep some resources for Skein.
        """
        memory = self._memory or default_memory
        vcores = self._vcores or default_vcores

        if self._deploy_mode != "cluster":  # client
            memory = self._memory or self._driver_memory or default_memory
            if self._driver_cores:
                vcores = int(self._vcores or self._driver_cores)

        return {
            "memory": memory,
            "vcores": vcores,
        }


def kwargs_for_virtualenv(  # noqa: C901
    virtualenv_archive: str,
    entry_point: Optional[str] = None,
    application: Optional[str] = None,
    use_virtualenv_python: bool = True,
    use_virtualenv_spark: bool = False,
    **kwargs: Any,
) -> dict:
    """
    Helper method to construct SparkSubmit kwargs for use with a virtualenv.
    This should work with both Conda and regular virtualenvs.
    One of entry_point or application must be set.

    :param virtualenv_archive:
        URL to the archive virtualenv file. If no '#alias' suffix is provided,
        The unpacked alias will be 'venv'.

    :param entry_point:
        This should be a relative path to your spark job file
        inside of the virtualenv_archive.
        If you set this, you MUST NOT set application.

    :param application:
        SparkSubmitHook application.  If you set this, you MUST NOT
        set entry_point.

    :param use_virtualenv_python:
        Whether python in side of the venv should be used.
        defaults to True

    :param use_virtualenv_spark:
        Whether bin/spark-submit inside of the venv should be used.
        defaults to False.
        Note that if you set this to true, a spark_home and spark_binary
        kwargs will be overriden.

    :raises ValueError:
        - If virtualenv_archive is not a .zip, .tar.gz or .tgz file.
        - If both entry_point and application are provided.

    :return: dict of kwargs
    """

    ext_found = False
    for ext in (".zip", ".tar.gz", ".tgz"):
        if virtualenv_archive.endswith(ext) or f"{ext}#" in virtualenv_archive:
            ext_found = True
            break
    if not ext_found:
        raise ValueError(f"virtualenv_archive must be a .zip, .tar.gz or .tgz file. Was {virtualenv_archive}")

    # Set archives.
    if "#" in virtualenv_archive:
        alias = virtualenv_archive.rsplit("#", 1)[1]
    else:
        alias = "venv"
        virtualenv_archive += "#" + alias

    if "archives" in kwargs:
        kwargs["archives"] = virtualenv_archive + "," + kwargs["archives"]
    else:
        kwargs["archives"] = virtualenv_archive

    # Set kwargs[application].
    if (entry_point and application) or (not entry_point and not application):
        raise ValueError("Must set only one of entry_point or application.")

    if entry_point:
        # Set application to the entry_point path in the unpacked virtualenv.
        kwargs["application"] = os.path.join(alias, entry_point)
    else:
        kwargs["application"] = application

    # Set PYSPARK_PYTHON* vars.
    env_vars = {}
    if use_virtualenv_python:
        python_exec = os.path.join(alias, "bin", "python")
        env_vars["PYSPARK_PYTHON"] = python_exec

        # Setting PYSPARK_DRIVER_PYTHON will only work if
        # python exec is available where spark-submimt is being
        # called from.  This will work well if launcher == 'skein'
        # because we unpack the archive on the skein app master
        # and call spark-submit from there.  If launcher != skein
        # and deploy_mode != cluster, this requires that python_exec
        # is available locally to the Airflow Scheduler.
        if kwargs.get("deploy_mode") != "cluster":
            env_vars["PYSPARK_DRIVER_PYTHON"] = python_exec
    env_vars.update(kwargs.get("env_vars", {}))

    # Set spark_binary
    if use_virtualenv_spark:
        kwargs["spark_binary"] = os.path.join(alias, "bin", "spark-submit")
        # Make sure spark_home is not set, since we will be using spark_binary relative path.
        kwargs["spark_home"] = None
        env_vars.pop("SPARK_HOME", None)

    kwargs["env_vars"] = env_vars

    return kwargs
