from __future__ import annotations

import datetime
import logging
import warnings
from typing import Callable, Collection, Iterable

from airflow.configuration import conf
from airflow.exceptions import AirflowException
from airflow.sensors.external_task import ExternalTaskSensor
from airflow_client import client

from wmf_airflow_common.sensors.rest_helper import (
    _get_count,
    _get_dag,
    _get_external_task_group_task_ids,
    _get_groups,
    _get_task_ids,
)

logger = logging.getLogger(__name__)


class RestExternalTaskSensor(ExternalTaskSensor):
    """Implementation of ExternalTaskSensor that works over Airflow's REST API"""

    AIRFLOW_REST_API = "/api/v1"

    def __init__(
        self,
        *,
        external_instance_uri: str,
        external_dag_id: str,
        external_task_id: str | None = None,
        external_task_ids: Collection[str] | None = None,
        external_task_group_id: str | None = None,
        allowed_states: Iterable[str] | None = None,
        skipped_states: Iterable[str] | None = None,
        failed_states: Iterable[str] | None = None,
        execution_delta: datetime.timedelta | None = None,
        execution_date_fn: Callable | None = None,
        check_existence: bool = False,
        poll_interval: float = 2.0,
        deferrable: bool = conf.getboolean("operators", "default_deferrable", fallback=False),
        **kwargs,
    ):
        # Let the base class ExternalTaskSensor validate the shared arguments
        super().__init__(
            external_dag_id=external_dag_id,
            external_task_id=external_task_id,
            external_task_ids=external_task_ids,
            external_task_group_id=external_task_group_id,
            allowed_states=allowed_states,
            skipped_states=skipped_states,
            failed_states=failed_states,
            execution_delta=execution_delta,
            execution_date_fn=execution_date_fn,
            check_existence=check_existence,
            poll_interval=poll_interval,
            deferrable=deferrable,
            **kwargs,
        )

        if external_instance_uri is None:
            raise ValueError("External instance location argument external_instance_uri is required.")

        self.external_instance_uri = external_instance_uri
        full_host_uri = self.external_instance_uri + RestExternalTaskSensor.AIRFLOW_REST_API
        self.api_config = client.Configuration(host=full_host_uri)

    def _check_for_existence(self, session) -> None:
        found_dag = _get_dag(self.api_config, self.external_dag_id)

        if not found_dag:
            raise AirflowException(
                f"The external DAG {self.external_dag_id} " f"on instance {self.external_instance_uri} does not exist."
            )

        found_task_ids = _get_task_ids(self.api_config, self.external_dag_id)

        if self.external_task_ids:
            for external_task_id in self.external_task_ids:
                if external_task_id not in found_task_ids:
                    raise AirflowException(
                        f"The external task {external_task_id} in "
                        f"DAG {self.external_dag_id} on instance {self.external_instance_uri} "
                        "does not exist. If the task is a part of a task group, "
                        "prefix the task ID with the task group ID and a dot: "
                        "task_group_id.task_id"
                    )

        if self.external_task_group_id:
            found_groups = _get_groups(found_task_ids)
            if self.external_task_group_id not in found_groups:
                raise AirflowException(
                    f"The external task group '{self.external_task_group_id}' in "
                    f"DAG {self.external_dag_id} on instance {self.external_instance_uri} "
                    "does not exist."
                )

        self._has_checked_existence = True

    def get_count(self, dttm_filter, session, states) -> int:
        """
        Get the count of records against dttm filter and states.

        :param dttm_filter: date time filter for execution date
        :param session: airflow session object
        :param states: task or dag states
        :return: count of record against the filters
        """
        warnings.warn("This method is deprecated and will be removed in future.", DeprecationWarning, stacklevel=2)

        logger.info("About to get_count with the following arguments:")
        logger.info("dttm_filter = %s", str(dttm_filter))
        logger.info("self.external_task_ids = %s", str(self.external_task_ids))
        logger.info("self.external_task_group_id = %s", self.external_task_group_id)
        logger.info("self.external_dag_id = %s", self.external_dag_id)
        logger.info("states = %s", str(states))

        return _get_count(
            self.api_config,
            dttm_filter,
            self.external_task_ids,
            self.external_task_group_id,
            self.external_dag_id,
            states,
        )

    def get_external_task_group_task_ids(self, session, dttm_filter):
        warnings.warn("This method is deprecated and will be removed in future.", DeprecationWarning, stacklevel=2)

        return _get_external_task_group_task_ids(
            self.api_config,
            dttm_filter,
            self.external_task_group_id,
            self.external_dag_id,
        )
